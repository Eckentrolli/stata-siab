setwd("C:/Documents/Computing/Stata-siab")
library(foreign)
library(Hmisc)

data <- read.dta("siab_7508_prepared.dta", convert.factors = FALSE)

data.00 <- subset(data, spellyear=2000)

#describe(data.00)


attach(data.00)
dens.00 <- density(tentgelt, na.rm=TRUE)
plot(dens.00)