set more off
capture program drop fill

use "/u/home/jd1033/siab/siab_r_7510_v1.dta"
sort persnr begepi


// Generate additional variables
gen age= (year(begepi)-gebjahr)
gen year = year(begepi)

gen bland=int(ao_region/1000)
gen ost=0 
replace ost=1 if bland>=11


//restrict analysis to west-germany
drop if ost ==1

//set wage to 0 if missing and unemployed
replace tentgelt_gr=0 if inlist(erwstat_gr, 21,22, 11,12,13,25) & tentgelt_gr >=.


//fill in missing STIB-data
replace stib=0 if erwstat_gr == 2 & stib >=.


// Find overlapping spells
gen overl = 0
by persnr: replace overl=1 if begepi[_n+1] <= endepi[_n] & persnr[_n] == persnr[_n+1]

by persnr: replace overl=1 if endepi[_n-1] >= begepi[_n] & persnr[_n-1] == persnr[_n]

//Reduce overlapping spells to one, keep the ones with higher wage
sort persnr spell

drop if overl==1 & overl[_n+1] ==1 & tentgelt_gr[_n] <= tentgelt_gr[_n+1]

drop if overl==1 & overl[_n-1] ==1 & tentgelt_gr[_n] <= tentgelt_gr[_n-1]


//Define program to fill up continuous missing values

program define fill
gsort persnr begepi
replace `1'=`1'[_n-1] if `1' >=. & `1'[_n-1] <. & persnr==persnr[_n-1] 
gsort -persnr -begepi
replace `1'=`1'[_n-1] if `1' >=. & `1'[_n-1] <. & persnr==persnr[_n-1] 
gsort persnr begepi
end

program define filltrunc
gsort persnr begepi
replace `1'=`1'[_n-1] if `1' >=. & `1'[_n-1] <. &  `1'[_n+1] <. & persnr==persnr[_n-1] & persnr==persnr[_n+1] 
end

//Apply fill program to variables

sort persnr begepi
fill ao_region
//fill schbild
fill deutsch
filltrunc beruf_gr
filltrunc stib

gen beruf_backup = beruf_gr
filltrunc beruf_gr

//generate fulltime variable
gen workft=0
replace workft=1 if inlist(stib, 1,2,3,4,7)

//Drop uneccessary variables

drop tage_alt


//Mark labor force participants 

gen LF=0
replace LF=1 if inlist(erwstat_gr, 1, 2, 3, 4, 5, 6, 7, 23)

drop if tentgelt_gr >=.

//Generate weights by share of the year spent working

gen workdays = (endepi-begepi)
gen sweight = (1/365)*(workdays)
gen ltentgelt = ln(tentgelt_gr)
gen yearwage = workdays*tentgelt_gr

//Missing discrete

replace pendler = 3 if spellyear >= 1999 & pendler >= . 
//replace bild = 50 if bild >=.
replace beruf_gr = 150 if beruf_gr >=.
replace stib = 20 if stib >=.
replace ao_region = 1 if ao_region >=.
replace deutsch = 0 if deutsch >=.


//gen education category 
recode bild (50 1 21 = 0) (2 3 22 23 =1) (4 24 25 = 2) (5 6 26 27 = 3)   , gen(bildcat)

label define bildcatl 0 "Kein" 1 "Schulabschluss" 2 "Ausbildung" 3 "Hochschulabschluss"
label values bildcat bildcatl

gen bildorig = bild

//Imputation for education variable

version 8.0
set rmsg on




replace erwstat_gr = 26 if erwstat_gr >=.

recode bildorig (21 = 1) (22 23 24 = 2) (25 26 = 5) (27=6)
 
gen AJAHR = year(begepi)

//gen int ADAT=mdy(AMONAT,ATAG,1900+AJAHR)
//drop AMONAT ATAG

*SPELL2 is inverse counter of spell
gen int SPELL2=(-1)*spell


/*data preparation*/
*generate the new education variable
generate byte IP2A=0



sort persnr spell


*one value (-9) for missing education
by persnr (spell), sort: replace IP2A= -9 if bildorig[_n]>=.

/* +++++++++++
   section 1
   acceptance of spells
   +++++++++++*/

/*special rules for young persons*/
*if age below 18 any formal education (2 to 6) is implausible
*impute "no formal education" (1)
by persnr (spell), sort: replace IP2A= 1 if age[_n]<18

*accept no vocational trainining (1,3) for young persons in vocational education
by persnr (spell), sort: replace IP2A= 1 if age[_n]<23 & bildorig[_n]==1 & stib[_n]==0
by persnr (spell), sort: replace IP2A= 3 if age[_n]>=21 & age[_n]<23 & bildorig[_n]==3 & stib[_n]==0
tabulate IP2A

/*
Accept education information which is reported three times or more
for a person, count only employment spells.
Three types of spells:
1) employment spells TYP~=6, BTYP==1
2) technical spells TYP~=6, BTYP~=1
3) UI benefit spells TYP==6
*/

*define minimum frequency per person for valid information
gen byte minimum=3
*lower minimum for persons with only a few employment spells
*count number of employment spells
by persnr: egen int NNONLED=sum(LF==1)

by persnr: replace minimum=2 if NNONLED==4
by persnr: replace minimum=1 if NNONLED==1 | NNONLED==2 | NNONLED==3

*count number of employment spells per person and specific education
by persnr bildorig (spell), sort: egen BLDN=sum(LF==1)

/*Define valid education information:
i) number of persons spells with this specific information at least minimum frequency AND
ii) employment spell AND
iii) non missing education information 1<=bild<=6
*/

gen byte VALID=(BLDN>=minimum & LF==1 & 1<=bildorig & bildorig<=6)


/* accept all VALID information */
by persnr (spell), sort: replace IP2A=bildorig[_n] if VALID==1 & age[_n]>=18

/*
For persons with education information missing at all spells:
Impute "vocational tranining degree" if the employment status (stib=2,3)
 indicates a qualified job at the respective employment spells.*/

egen NMIS=sum(bildorig >=.), by(persnr)
generate DMIS= NMIS==spell

by persnr (spell), sort: replace IP2A=2 if stib[_n]>=2 & stib[_n]<=3 & LF[_n]==1 & DMIS[_n]==1 


/* +++++++++++++++++++++
   section 2
   extrapolation
   +++++++++++++++++++++

Part 1: extrapolation to following spells

Extrapolate education information to following spells with lower
education information or missing information. Note that persons who have
vocational training (2) and high school (3) have both, which
has to be reported as (4). Hence the extrapolation is done stepwise.
1) Extrapolate "no degree" to following missings
2) Extrapolate "vocational training degree, 2" to following missings and "no degree, 1"
3) Extrapolate "high school, 3" to following missings and "no degree, 1". Do
not extrapolate 3 to 2. 3 is not higher than 2. 2 and 3 mean having both, 4.
4) Impute "high school & vocational training degree, 4" if "high school, 3" is reported
and "vocational training degree, 2" has been reported before or vice versa. Extrapolate
"high school & vocational training degree, 4" to following missings and spells with lower
education.
5) Extrapolate "technical college, 5" to following missings and lower degrees
6) Extrapolate "university, 6" to following missings and lower degrees*/

sort persnr spell

by persnr: replace IP2A=1 if _n>1 & IP2A[_n-1]==1 & IP2A[_n]<1 /*only to missing values*/

by persnr: replace IP2A=2 if _n>1 & IP2A[_n-1]==2 & IP2A[_n]<2 /*only to missing values and "1"*/

by persnr: replace IP2A=3 if _n>1 & IP2A[_n-1]==3 & IP2A[_n]<2 /*only to missing values and "1"*/

by persnr: replace IP2A=4 if _n>1 & (IP2A[_n-1]==4 | (IP2A[_n-1]==2 & IP2A[_n]==3) | (IP2A[_n-1]==3 & IP2A[_n]==2)) & IP2A[_n]<4

by persnr: replace IP2A=5 if _n>1 & IP2A[_n-1]==5 & IP2A[_n]<5

by persnr: replace IP2A=6 if _n>1 & IP2A[_n-1]==6 & IP2A[_n]<6


/*

part 2: backwards extrapolation to previous spells

The extrapolation rule above has eliminated all missing values for a person after
the first spell with education information. But possibliy the
first spell(s) still have missing values. Backwards extrapolate information
from the first spell with non missing education information for a person to
previous spells which have no information yet. Do not extrapolate below education
specific age limits:
university: 29 years
technical college: 27 years
both vocational training and high school: 23 years
only high school: 21 years
only vocational training: 20 years
no education degree: no age limit*/

by persnr (SPELL2), sort: replace IP2A=6 if _n>1 & (IP2A[_n]==-9 | IP2A[_n]==0) & IP2A[_n-1]==6 & age[_n]>=29 

by persnr (SPELL2), sort: replace IP2A=5 if _n>1 & (IP2A[_n]==-9 | IP2A[_n]==0) & IP2A[_n-1]==5 & age[_n]>=27 

by persnr (SPELL2), sort: replace IP2A=4 if _n>1 & (IP2A[_n]==-9 | IP2A[_n]==0) & IP2A[_n-1]==4 & age[_n]>=23 

by persnr (SPELL2), sort: replace IP2A=3 if _n>1 & (IP2A[_n]==-9 | IP2A[_n]==0) & IP2A[_n-1]==3 & age[_n]>=21 

by persnr (SPELL2), sort: replace IP2A=2 if _n>1 & (IP2A[_n]==-9 | IP2A[_n]==0) & IP2A[_n-1]==2 & age[_n]>=20

by persnr (SPELL2), sort: replace IP2A=1 if _n>1 & (IP2A[_n]==-9 | IP2A[_n]==0) & IP2A[_n-1]==1




/* +++++++++++
   section 3
   +++++++++++
*/


/*
Require multiple spells (parallel spells at the same time, STYP=2) to have the
same education information.
Identification through the same beginning date.
Impute highest of the parallel education information.*/


sort persnr spell

*single missing value
replace IP2A=-9 if IP2A==0
tab IP2A

version 12.1


// merge discounting values, prices of 1995

merge m:1 year using "/u/home/jd1033/siab/VPIndex.dta"

drop _merge

// merge "Geringfügigkeitsgrenze" for each year
merge m:1 year using "/u/home/jd1033/siab/geringf.dta"

drop _merge

merge m:1 











close log