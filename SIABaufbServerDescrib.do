set more off
capture program drop fill

use "/u/home/jd1033/siab/siab_r_7510_v1.dta"
sort persnr begepi



// Generate additional variables
gen age= (year(begepi)-gebjahr)
gen spellyear = year(begepi)

//set wage to 0 if missing and unemployed
replace tentgelt_gr=0 if inlist(erwstat_gr, 21,22, 11,12,13) & tentgelt_gr >=.

//fill in missing STIB-data
replace stib=0 if erwstat_gr == 2 & stib >=.


// Find overlapping spells
gen overl = 0
by persnr: replace overl=1 if begepi[_n+1] <= endepi[_n] & work==0 & persnr[_n] == persnr[_n+1]

by persnr: replace overl=1 if endepi[_n-1] >= begepi[_n] & work==0 & persnr[_n-1] == persnr[_n]

//Reduce overlapping spells to one, keep the ones with higher wage
sort persnr spell

drop if overl==1 & overl[_n+1] ==1 & tentgelt_gr[_n] <= tentgelt_gr[_n+1]

drop if overl==1 & overl[_n-1] ==1 & tentgelt_gr[_n] <= tentgelt_gr[_n-1]


//Define program to fill up continuous missing values

program define fill
gsort persnr begepi
replace `1'=`1'[_n-1] if `1' >=. & `1'[_n-1] <. & persnr==persnr[_n-1] 
gsort -persnr -begepi
replace `1'=`1'[_n-1] if `1' >=. & `1'[_n-1] <. & persnr==persnr[_n-1] 
gsort persnr begepi
end

program define filltrunc
gsort persnr begepi
replace `1'=`1'[_n-1] if `1' >=. & `1'[_n-1] <. &  `1'[_n+1] <. & persnr==persnr[_n-1] & persnr==persnr[_n+1] 
end

//Apply fill program to variables

sort persnr begepi
fill ao_region
//fill schbild
fill deutsch
filltrunc beruf_gr
filltrunc stib

gen beruf_backup = beruf_gr
filljob beruf_gr

//generate fulltime variable
gen workft=0
replace workft=1 if inlist(stib, 1,2,3,4,7)

//Drop uneccessary variables

drop tage_alt


//Mark labor force participants 

gen LF=0
replace LF=1 if inlist(erwstat_gr, 1, 2, 3, 4, 5, 6, 7, 23)

//Generate weights by share of the year spent working

gen workdays = (endepi-begepi)
gen sweight = (1/365)*(workdays)
gen ltentgelt = ln(tentgelt_gr)
gen yearwage = workdays*tentgelt_gr

//Missing discrete

replace pendler = 3 if spellyear >= 1999 & pendler >= . 
//replace bild = 50 if bild >=.
replace beruf_gr = 150 if beruf_gr >=.
replace stib = 20 if stib >=.
replace ao_region = 1 if ao_region >=.
replace deutsch = 3 if deutsch >=.



mdesc

close log