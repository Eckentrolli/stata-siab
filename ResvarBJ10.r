#Residual Variance for reweighted data

options(scipen=30)

library(foreign)
library(Hmisc)
library(parallel)

setwd("E:/Data")

# Reweighted 2010 for 1985 baseyear:

load("rw8510.RData")

years <- c(1985,2010)
justfrau <- unique(rw8510$frau)
grid <- expand.grid(years,justfrau)
colnames(grid)<-c("year","frau")

grid3 <- numeric()
for (i in 1:nrow(grid)) {
	grid3 <- c(grid3, paste(as.character(grid[i,1]), as.character(grid[i,2]),sep="@"))
}

#define functions

cellreg <- function(i, j) {
	sample1 <- subset(rw8510, frau==j & year==i)
	temp <- lm(discount ~ age+age2+age3+bildyear+age:bildcat+age2:bildcat+age3:bildcat, data=sample1,weights=rweight)
	res<-data.frame(count=sample1$count,residualsmf=temp$residuals, fittedmf=temp$fitted.values)
	rm(sample1)
	
	return(res)
}

cellregall <- function(i) {
	sample2 <- subset(rw8510, year==i)
	#test<- summary(sample1)	
	temp <-lm(discount ~ age+age2+age3+bildyear+age:bildcat+age2:bildcat+age3:bildcat, data=sample2,weights=rweight)
	resall <- data.frame(count=sample2$count, residualsall=temp$residuals, fittedall=temp$fitted.values)
	rm(sample2)
	return(resall)
}

wrapper <- function(i) {
	x <- as.numeric(unlist(strsplit(i, split="@")))
	res2<-cellreg(x[1],x[2])
	return(res2)
}

#separate m/f

results <- mclapply(grid3,function(x) wrapper(x))

bindres <- data.frame(results[1])
for (i in 2:length(grid3)) {
bindres<- rbind(bindres, data.frame(results[i]))
}

rw8510 <- rw8510[order(rw8510$count),]
bindres <- bindres[order(bindres$count),]
colnames(bindres)[2:3] <- c("res.mf.bj85", "fit.mf.bj85")

rw8510 <- cbind(rw8510, bindres)


# combined m/f

resultsall <- mclapply(years, function(x) cellregall(x))

bindresall <- data.frame(resultsall[1])

for (i in 2:length(years)) {
bindresall<- rbind(bindresall, data.frame(resultsall[i]))
}

bindresall <- bindresall[order(bindresall$count),]
colnames(bindresall)[2:3] <- c("res.all.bj85", "fit.all.bj85")

rw8510 <- cbind(rw8510, bindresall)

#Naming


#cleanup

rw1085 <- rw1085[!names(rw1085) %in% c("count") ]