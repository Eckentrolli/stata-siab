#Reweighting scheme for comparison of 

options(scipen=30)

library(np)
library(Hmisc)
library(parallel)
library(plyr)
library(speedglm)
library(boot)



#setwd("E:/DATA")
setwd("/u/home/jd1033/DFG-descriptives/Results/FT")

#load("E:/DATA/Residuals.RData")
load("/u/home/jd1033/DFG-descriptives/ResidualsSmallFT.RData")

#trim <- trim[sample(nrow(trim), 10000), ] 

#results.bak <- results
rm(results, resultsall)

trim$beruf_gr <- as.numeric(trim$beruf_gr)

berufe <- as.data.frame(read.csv("/u/home/jd1033/DFG-descriptives/Occupations_SIAB_Neu.csv", header=TRUE,sep = ";"))

occlist <- sapply(trim$beruf_gr, function(x) berufe[match(x, berufe[,2]),3])

trim$beruf.ab <- occlist
trim$beruf.ab <- as.factor(trim$beruf.ab)

trim <-trim[complete.cases(trim[,50]),]

#male and female subsamples 
trim.m <- subset(trim, frau==0 & age >= 20 & age <=60)
trim.f <- subset(trim, frau==1 & age >= 20 & age <=60)


rm(trim)


years2 <- years[years >1985]


reweight85occs <- function(i) {

	#calculation for base year 1985 to 2010

	pooled <- subset(trim.m, year==1985 | year==i )

	n <- nrow(pooled)
	nbase <- nrow(pooled[pooled$year==1985,])
	ndest <- nrow(pooled[pooled$year==i,])
	
	comp.weight <- nbase/ndest
	
	pooled$comp.weight <- 1
	pooled[pooled$year==i,]$comp.weight <- comp.weight
	
		
	#Create year dummies
	pooled$dyear <-  pooled$year
	pooled$dbase <-  pooled$year

	pooled$dyear[pooled$dyear==1985] <- 0
	pooled$dyear[pooled$dyear==i] <- 1
	pooled$dbase[pooled$dbase==1985] <- 1
	pooled$dbase[pooled$dbase==i] <- 0
	
	pooled$dyear <- as.factor(pooled$dyear)
	pooled$dbase <- as.factor(pooled$dbase)

	#Probit of likelihood of being in 2010, given charactersistics
	
	#results0 <- glm(dyear ~ beruf.ab+beruf.ab:potexp+potexp+potexp2+potexp3+bild+potexp:bild+potexp2:bild+potexp3:bild, data=pooled, family="binomial"(link=probit))
	
	data <- model.matrix( ~ beruf.ab+beruf.ab:potexp+potexp+potexp2+potexp3+bild+potexp:bild+potexp2:bild+potexp3:bild, data=pooled)
	
	#results1 <- glm.fit(data, pooled$dyear, family="binomial"(link=probit))
	results2 <- speedglm.wfit(pooled$dyear,data, family=binomial(link=probit))
	
	
	#head(inv.logit(data %*% results1$coefficients))
	#head(pnorm(data %*% results1$coefficients))
	fitted.values <- pnorm(data %*% results2$coefficients)
	
		
	#combine values & fits into dataframe
	rw.data <- cbind(pooled, fitted.values=fitted.values)

	#Calculate reweighting factor. (1-P)/P small if P large (e.g. highly educated)
	rw.data$base85occ <- (((1-rw.data$fitted.values)/rw.data$fitted.values)*(1/rw.data$comp.weight))

	#remove superfluous count columns
	rw.data <- rw.data[!names(rw.data) %in% c("dyear", "dbase","sweight.2","count.1", "count.2" ) ]
	save(rw.data, file=paste("reweighting/rw",i,".occ.RData" , sep=""))
	#res <- rw.data[!names(rw.data) %in% "fitted.values"]
	
	if (i==years2[length(years2)]) { 
		res <- rw.data 
		} 	else {
		res <- subset(rw.data, year!=1985) 
		}
	
	rm(rw.data)
	
	return(res)

}



results.85.occ  <- mclapply(years2,function(x) reweight85occs(x), mc.cores=3)

names(results.85.occ) <- years2
trim.m.rw.occ <- results.85.occ[[1]] 

for(i in 2:length(years2)) {
	temp.2 <- results.85.occ[[i]]
	trim.m.rw.occ <- rbind(trim.m.rw.occ, temp.2)
	}
	
	
#set CF weights for baseyear to 1
trim.m.rw.occ[trim.m.rw.occ$year==1985,]$base85occ <- 1


save(trim.m.rw.occ, file="reweighting/trim.m.rw85.occ.RData")


rm(results.85.occ,trim.m.rw.occ)

#FEMALES

reweight85occ.f <- function(i) {
	
	pooled <- subset(trim.f, year==1985 | year==i )

	n <- nrow(pooled)
	nbase <- nrow(pooled[pooled$year==1985,])
	ndest <- nrow(pooled[pooled$year==i,])
	
	comp.weight <- nbase/ndest
	
	pooled$comp.weight <- 1
	pooled[pooled$year==i,]$comp.weight <- comp.weight
	
		
	#Create year dummies
	pooled$dyear <-  pooled$year
	pooled$dbase <-  pooled$year

	pooled$dyear[pooled$dyear==1985] <- 0
	pooled$dyear[pooled$dyear==i] <- 1
	pooled$dbase[pooled$dbase==1985] <- 1
	pooled$dbase[pooled$dbase==i] <- 0
	
	pooled$dyear <- as.factor(pooled$dyear)
	pooled$dbase <- as.factor(pooled$dbase)

	#Probit of likelihood of being in 2010, given charactersistics
	
	#results0 <- glm(dyear ~ beruf.ab+beruf.ab:potexp+potexp+potexp2+potexp3+bild+potexp:bild+potexp2:bild+potexp3:bild, data=pooled, family="binomial"(link=probit))
	
	data <- model.matrix( ~ beruf.ab+beruf.ab:potexp+potexp+potexp2+potexp3+bild+potexp:bild+potexp2:bild+potexp3:bild, data=pooled)
	
	#results1 <- glm.fit(data, pooled$dyear, family="binomial"(link=probit))
	results2 <- speedglm.wfit(pooled$dyear,data, family=binomial(link=probit))
	
	
	#head(inv.logit(data %*% results1$coefficients))
	#head(pnorm(data %*% results1$coefficients))
	fitted.values <- pnorm(data %*% results2$coefficients)
	
		
	#combine values & fits into dataframe
	rw.data <- cbind(pooled, fitted.values=fitted.values)

	#Calculate reweighting factor. (1-P)/P small if P large (e.g. highly educated)
	rw.data$base85occ <- (((1-rw.data$fitted.values)/rw.data$fitted.values)*(1/rw.data$comp.weight))


	#cbind(rw.data$fitted.values[1:20],rw.data$base85[1:20])

	#remove superfluous count columns
	rw.data <- rw.data[!names(rw.data) %in% c("dyear", "dbase","sweight.2","count.1", "count.2" ) ]
	save(rw.data, file=paste("reweighting/rw",i,".occF.RData" , sep=""))
	#res <- rw.data[!names(rw.data) %in% "fitted.values"]
	
	if (i==years2[length(years2)]) { 
		res <- rw.data 
		} 	else {
		res <- subset(rw.data, year!=1985) 
		}
	
	rm(rw.data)
	
	return(res)

}


results.85.occ  <- mclapply(years2,function(x) reweight85occ.f(x), mc.cores=3)

names(results.85.occ) <- years2
trim.f.rw.occ <- results.85.occ[[1]] 

for(i in 2:length(years2)) {
	temp.2 <- results.85.occ[[i]]
	trim.f.rw.occ <- rbind(trim.f.rw.occ, temp.2)
	}
	
	
#set CF weights for baseyear to 1
trim.f.rw.occ[trim.f.rw.occ$year==1985,]$base85occ <- 1


save(trim.f.rw.occ, file="reweighting/trim.f.rw85.occ.RData")
