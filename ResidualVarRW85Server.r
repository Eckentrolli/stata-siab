########################################################################################
####PROBABLY OUTDATED, NO NEED FOR RECOMPUTATION OF RESIDUALS AFTER REWEIGHTING##############
#########################################################################################


options(scipen=30)

library(Hmisc)
library(parallel)
library(car)


setwd("/u/home/jd1033/DFG-descriptives/Results/FT")
load("reweighting/trim.m.rw85.RData")
load("reweighting/trim.f.rw85.RData")

#trim.m.rw <- trim.m.rw [sample(nrow(trim.m.rw ), 10000), ] 
#trim.f.rw <- trim.f.rw [sample(nrow(trim.f.rw ), 10000), ] 

#calculate reweighting

trim.m.rw$rweight <- trim.m.rw$sweight*trim.m.rw$base85
trim.f.rw$rweight <- trim.f.rw$sweight*trim.f.rw$base85

trim.m.rw$countmf <- c(1:length(trim.m.rw$discount))
trim.f.rw$countmf <- c(1:length(trim.f.rw$discount))

#reg-preparation
years <- unique(trim.m.rw$year)

years <- sort(years, decreasing=TRUE)
years<- years[order(years, decreasing=TRUE)]
justfrau <- c(0,1)
grid <- as.matrix(expand.grid(years, justfrau))
grid2 <- grid
colnames(grid)<-c("year","frau")


#define functions

cellreg <- function(i, j) {
	if(j==1) {
		sample1 <- subset(trim.f.rw , year==i)	
	}	else {
		sample1 <- subset(trim.m.rw , year==i)
	}
	temp <-lm(discount ~ potexp+potexp2+potexp3+bild+potexp:bild+potexp2:bild+potexp3:bild, data=sample1,weights=rweight)
	res<-data.frame(countmf=sample1$countmf,res.base85=temp$residuals, fitted.base85=temp$fitted.values, values.base85=sample1$discount, rweight=sample1$rweight) 
	rm(sample1)
	return(res)
}

grid3 <- numeric()

for (i in 1:nrow(grid)) {
	grid3 <- c(grid3, paste(as.character(grid[i,1]), as.character(grid[i,2]),sep="@"))
}

wrapper <- function(i) {
	x <- as.numeric(unlist(strsplit(i, split="@")))
	res2<-cellreg(x[1],x[2])
	return(res2)
}
#Mincer regression

results <- mclapply(grid3,function(x) wrapper(x), mc.cores=3)

#naming

grid[grid[,2]==0,][,2] <-"m"
grid[grid[,2]==1,][,2] <-"f"

resnames <- numeric()
for (i in 1:dim(grid)[1]) {
	resnames <- c(resnames, paste(as.character(grid[i,1]), as.character(grid[i,2]),sep=""))
}

names(results) <- resnames 

bindresm <- results[[1]]

for (i in 2:26) {
bindresm<- rbind(bindresm, results[[i]])
}

bindresf <- results[[27]]
for (i in 28:52) {
bindresf<- rbind(bindresf, results[[i]])
}


trim.m.rw <- trim.m.rw[order(trim.m.rw$countmf),]
trim.f.rw <- trim.f.rw[order(trim.f.rw$countmf),]

bindresm <- bindresm[order(bindresm$countmf),]
bindresf <- bindresf[order(bindresf$countmf),]

trim.m.rw <- cbind(trim.m.rw, bindresm)
trim.f.rw <- cbind(trim.f.rw, bindresf)


variances <- data.frame()

for (i in 1:nrow(grid)) {

variances[i,1] <- wtd.var(results[[i]]$res.base85,weights=results[[i]]$rweight, normwt=TRUE, na.rm=FALSE)
variances[i,2] <- sqrt(variances[i,1])
variances[i,3] <- wtd.var(results[[i]]$values.base85, weights=results[[i]]$rweight, normwt=TRUE, na.rm=FALSE)
variances[i,4] <- sqrt(variances[i,3])
}


rownames(variances) <- resnames
colnames(variances) <- c("ResidualV.85", "ResidualSD.85", "Variance.85", "SD.85")

trim.m.rw <- trim.m.rw[!names(trim.m.rw) %in% c("values.base85", "sweight.1 ","countmf","rweight.1","valuesall") ]
trim.f.rw <- trim.f.rw[!names(trim.f.rw) %in% c("values.base85", "sweight.1 ","countmf","rweight.1","valuesall") ]
save.image( file = "reweighting/Resid85.RData")



