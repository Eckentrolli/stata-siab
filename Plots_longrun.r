options(scipen=30)

library(foreign)
library(Hmisc)
library(WriteXLS)
library(xtable)


setwd("/u/home/jd1033/DFG-descriptives/Results/FT")

load("/u/home/jd1033/DFG-descriptives/Results/FT/CellanalysisM.RData")
load("/u/home/jd1033/DFG-descriptives/Results/FT/CellanalysisF.RData")


#######################################################################################
#PLOT OF MALE COLLEGE SHARE OVER TIME

setEPS()
postscript("Graphs/shares_over_time/College_over_time.M.eps", width=8, heigh=7)

plot.data <- subset(resultsm, resultsm$bildcat_gr==2 & resultsm$age.min==25)

par(mar=c(5.1, 5.5, 4.1, 2.1)+ 1.2)
plot(plot.data$share~plot.data$year, xlim=c(1985,2011), ylim=c(0,0.07), xaxt="n", main="Share of male college workers by age", xlab="Time", ylab="College share of male workforce", type="o", lwd=2, col="red")
axis(side=1, at=plot.data$year, labels=plot.data$year, cex.axis=0.9)

plot.data <- subset(resultsm, resultsm$bildcat_gr==2 & resultsm$age.min==35)
lines(plot.data$share~plot.data$year, type="o", col="blue", lwd=2)

plot.data <- subset(resultsm, resultsm$bildcat_gr==2 & resultsm$age.min==45)
lines(plot.data$share~plot.data$year, type="o", col="green", lwd=2)

plot.data <- subset(resultsm, resultsm$bildcat_gr==2 & resultsm$age.min==55)
lines(plot.data$share~plot.data$year, type="o", col="black", lwd=2)

legend(1985,0.07, c("Age 25-34","Age 35-44", "Age 45-54", "Age 55-64","All ages"), lty=c(1,1,1,1), lwd=c(2.5,2.5,2.5,2.5),col=c("red", "blue","green", "black","darkorchid2")) # gives the legend lines the correct color and widt


#####second y axis

plot.data <- subset(resultsm, resultsm$bildcat_gr==2 )
years <- unique(resultsm$year)
test <- by(plot.data$share, plot.data$year,sum)
totalshare <- data.frame()
for ( i in 1:length(test)) {
	totalshare[i,1] <- years[i]
	totalshare[i,2] <- test[[i]]
	}
colnames(totalshare) <- c("years", "share")

par(new = TRUE)
plot(totalshare$share~totalshare$years, ylim=c(0.0,0.145), xlab = "", ylab = "",axes=FALSE, lwd=2, type="o",col="darkorchid2")
axis(side=4, at = pretty(c(0.0,0.145)))
mtext("Total share all ages", side=4, line=2)

rm(totalshare, plot.data, test)
dev.off()



#PLOT OF VOCATIONAL & High shool SHARE OVER TIME

setEPS()
postscript("Graphs/shares_over_time/Vocational_over_time.M.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1)+ 1.2)

plot.data <- subset(resultsm, resultsm$bildcat_gr==1 & resultsm$age.min==25)
plot(plot.data$share~plot.data$year, xlim=c(1985,2011), ylim=c(0.04,0.5), xaxt="n", main="Share of male vocational & high school workers by age", xlab="Time", ylab="Vocational share of male workforce", type="o", lwd=2, col="red")

axis(side=1, at=plot.data$year, labels=plot.data$year, cex.axis=0.9)

plot.data <- subset(resultsm, resultsm$bildcat_gr==1 & resultsm$age.min==35)
lines(plot.data$share~plot.data$year, type="o", col="blue", lwd=2)

plot.data <- subset(resultsm, resultsm$bildcat_gr==1 & resultsm$age.min==45)
lines(plot.data$share~plot.data$year, type="o", col="green", lwd=2)

plot.data <- subset(resultsm, resultsm$bildcat_gr==1 & resultsm$age.min==55)
lines(plot.data$share~plot.data$year, type="o", col="black", lwd=2)

plot.data <- subset(resultsm, resultsm$bildcat_gr==1 & resultsm$age.min==18)
lines(plot.data$share~plot.data$year, type="o", col="orange", lwd=2)

legend(1985,0.5, c("Age 18-24", "Age 25-34","Age 35-44", "Age 45-54", "Age 55-64","All ages"), lty=c(1,1,1,1), lwd=c(2.5,2.5,2.5,2.5),col=c("orange","red", "blue","green", "black","darkorchid2")) # gives the legend lines the correct color and widt


#####second y axis

plot.data <- subset(resultsm, resultsm$bildcat_gr==1 )
years <- unique(resultsm$year)
test <- by(plot.data$share, plot.data$year,sum)
totalshare <- data.frame()
for ( i in 1:length(test)) {
	totalshare[i,1] <- years[i]
	totalshare[i,2] <- test[[i]]
	}
colnames(totalshare) <- c("years", "share")

par(new = TRUE)
plot(totalshare$share~totalshare$years, ylim=c(0.6,0.85), xlab = "", ylab = "",axes=FALSE, lwd=2, type="o",col="darkorchid2")
axis(side=4, at = pretty(c(0.6,0.8)))
mtext("Total share all ages", side=4, line=2)

rm(totalshare, plot.data, test)
dev.off()


#PLOT OF NON TRAINED OVER TIME

setEPS()
postscript("Graphs/shares_over_time/Nodegree_over_time.M.eps", width=8, heigh=7)

plot.data <- subset(resultsm, resultsm$bildcat_gr==0 & resultsm$age.min==25)

par(mar=c(5.1, 5.5, 4.1, 2.1)+1.2)
plot(plot.data$share~plot.data$year, xlim=c(1985,2011), ylim=c(0,0.095), xaxt="n", main="Share of male untrained workers by age", xlab="Time", ylab="Untrained share of male workforce", type="o", lwd=2, col="red")
axis(side=1, at=plot.data$year, labels=plot.data$year, cex.axis=0.9)

plot.data <- subset(resultsm, resultsm$bildcat_gr==0 & resultsm$age.min==35)
lines(plot.data$share~plot.data$year, type="o", col="blue", lwd=2)

plot.data <- subset(resultsm, resultsm$bildcat_gr==0 & resultsm$age.min==45)
lines(plot.data$share~plot.data$year, type="o", col="green", lwd=2)

plot.data <- subset(resultsm, resultsm$bildcat_gr==0 & resultsm$age.min==55)
lines(plot.data$share~plot.data$year, type="o", col="black", lwd=2)

plot.data <- subset(resultsm, resultsm$bildcat_gr==0 & resultsm$age.min==18)
plot.data$share[26]<-0.02237289
lines(plot.data$share~plot.data$year, type="o", col="orange", lwd=2)

legend(2003,0.095, c("Age 18-24","Age 25-34","Age 35-44", "Age 45-54", "Age 55-64", "All ages"), lty=c(1,1,1,1), lwd=c(2.5,2.5,2.5,2.5),col=c("orange", "red", "blue","green", "black","darkorchid2")) # gives the legend lines the correct color and widt

#####second y axis

plot.data <- subset(resultsm, resultsm$bildcat_gr==0 )
years <- unique(resultsm$year)
test <- by(plot.data$share, plot.data$year,sum)
totalshare <- data.frame()
for ( i in 1:length(test)) {
	totalshare[i,1] <- years[i]
	totalshare[i,2] <- test[[i]]
	}
colnames(totalshare) <- c("years", "share")

par(new = TRUE)
plot(totalshare$share~totalshare$years, ylim=c(0.0,0.22), xlab = "", ylab = "",axes=FALSE, lwd=2, type="o",col="darkorchid2")
axis(side=4, at = pretty(c(0.0,0.2)))
mtext("Total share all ages", side=4, line=2)

dev.off()

############################################################################################





#PLOT OF FEMALE COLLEGE SHARE OVER TIME

setEPS()
postscript("Graphs/shares_over_time/College_over_time.F.eps", width=8, heigh=7)

plot.data <- subset(resultsf, resultsf$bildcat_gr==2 & resultsf$age.min==25)

par(mar=c(5.1, 5.5, 4.1, 2.1)+1.3)
plot(plot.data$share~plot.data$year, xlim=c(1985,2011), ylim=c(0,0.085), xaxt="n", main="Share of female college workers by age", xlab="Time", ylab="College share of female workforce", type="o", lwd=2, col="red")
axis(side=1, at=plot.data$year, labels=plot.data$year, cex.axis=0.9)

plot.data <- subset(resultsf, resultsf$bildcat_gr==2 & resultsf$age.min==35)
lines(plot.data$share~plot.data$year, type="o", col="blue", lwd=2)

plot.data <- subset(resultsf, resultsf$bildcat_gr==2 & resultsf$age.min==45)
lines(plot.data$share~plot.data$year, type="o", col="green", lwd=2)

plot.data <- subset(resultsf, resultsf$bildcat_gr==2 & resultsf$age.min==55)
lines(plot.data$share~plot.data$year, type="o", col="black", lwd=2)

legend(1985,0.085, c("Age 25-34","Age 35-44", "Age 45-54", "Age 55-64","All ages"), lty=c(1,1,1,1), lwd=c(2.5,2.5,2.5,2.5),col=c("red", "blue","green", "black","darkorchid2")) # gives the legend lines the correct color and widt


#####second y axis

plot.data <- subset(resultsf, resultsf$bildcat_gr==2 )
years <- unique(resultsf$year)
test <- by(plot.data$share, plot.data$year,sum)
totalshare <- data.frame()
for ( i in 1:length(test)) {
	totalshare[i,1] <- years[i]
	totalshare[i,2] <- test[[i]]
	}
colnames(totalshare) <- c("years", "share")

par(new = TRUE)
plot(totalshare$share~totalshare$years, ylim=c(0.0,0.13), xlab = "", ylab = "",axes=FALSE, lwd=2, type="o",col="darkorchid2")
axis(side=4, at = pretty(c(0.0,0.13)))
mtext("Total share all ages", side=4, line=2)

dev.off()

#PLOT OF VOCATIONAL & high shool SHARE OVER TIME

setEPS()
postscript("Graphs/shares_over_time/Vocational_over_time.F.eps", width=8, heigh=7)

plot.data <- subset(resultsf, resultsf$bildcat_gr==1 & resultsf$age.min==25)

par(mar=c(5.1, 5.5, 4.1, 2.1)+1.3)
plot(plot.data$share~plot.data$year, xlim=c(1985,2011), ylim=c(0.03,0.55), xaxt="n", main="Share of female vocational & high school workers by age", xlab="Time", ylab="Vocational share of female workforce", type="o", lwd=2, col="red")
axis(side=1, at=plot.data$year, labels=plot.data$year, cex.axis=0.9)

plot.data <- subset(resultsf, resultsf$bildcat_gr==1 & resultsf$age.min==35)
lines(plot.data$share~plot.data$year, type="o", col="blue", lwd=2)

plot.data <- subset(resultsf, resultsf$bildcat_gr==1 & resultsf$age.min==45)
lines(plot.data$share~plot.data$year, type="o", col="green", lwd=2)

plot.data <- subset(resultsf, resultsf$bildcat_gr==1 & resultsf$age.min==55)
lines(plot.data$share~plot.data$year, type="o", col="black", lwd=2)

plot.data <- subset(resultsf, resultsf$bildcat_gr==1 & resultsf$age.min==18)
lines(plot.data$share~plot.data$year, type="o", col="orange", lwd=2)

legend(2003,0.565, c("Age 18-24", "Age 25-34","Age 35-44", "Age 45-54", "Age 55-64","All ages"), lty=c(1,1,1,1), lwd=c(2.5,2.5,2.5,2.5),col=c("orange","red", "blue","green", "black","darkorchid2")) # gives the legend lines the correct color and widt


#####second y axis

plot.data <- subset(resultsf, resultsf$bildcat_gr==1 )
years <- unique(resultsf$year)
test <- by(plot.data$share, plot.data$year,sum)
totalshare <- data.frame()
for ( i in 1:length(test)) {
	totalshare[i,1] <- years[i]
	totalshare[i,2] <- test[[i]]
	}
colnames(totalshare) <- c("years", "share")

par(new = TRUE)
plot(totalshare$share~totalshare$years, ylim=c(0.0,1.2), xlab = "", ylab = "",axes=FALSE, lwd=2, type="o",col="darkorchid2")
axis(side=4, at = pretty(c(0.0,0.8)))
mtext("Total share all ages", side=4, line=2)

dev.off()


#PLOT OF NON TRAINED OVER TIME

setEPS()
postscript("Graphs/shares_over_time/Nodegree_over_time.F.eps", width=8, heigh=7)

plot.data <- subset(resultsf, resultsf$bildcat_gr==0 & resultsf$age.min==25)

par(mar=c(5.1, 5.5, 4.1, 2.1)+1.3)
plot(plot.data$share~plot.data$year, xlim=c(1985,2011), ylim=c(0.01,0.095), xaxt="n", main="Share of female untrained workers by age", xlab="Time", ylab="Untrained share of female workforce", type="o", lwd=2, col="red")
axis(side=1, at=plot.data$year, labels=plot.data$year, cex.axis=0.9)

plot.data <- subset(resultsf, resultsf$bildcat_gr==0 & resultsf$age.min==35)
lines(plot.data$share~plot.data$year, type="o", col="blue", lwd=2)

plot.data <- subset(resultsf, resultsf$bildcat_gr==0 & resultsf$age.min==45)
lines(plot.data$share~plot.data$year, type="o", col="green", lwd=2)

plot.data <- subset(resultsf, resultsf$bildcat_gr==0 & resultsf$age.min==55)
lines(plot.data$share~plot.data$year, type="o", col="black", lwd=2)

plot.data <- subset(resultsf, resultsf$bildcat_gr==0 & resultsf$age.min==18)
plot.data$share[26]<-0.02569468
lines(plot.data$share~plot.data$year, type="o", col="orange", lwd=2)


legend(2003,0.0955, c("Age 18-24", "Age 25-34","Age 35-44", "Age 45-54", "Age 55-64","All ages"), lty=c(1,1,1,1), lwd=c(2.5,2.5,2.5,2.5),col=c("orange","red", "blue","green", "black","darkorchid2")) # gives the legend lines the correct color and widt

#####second y axis

plot.data <- subset(resultsf, resultsf$bildcat_gr==0 )
years <- unique(resultsf$year)
test <- by(plot.data$share, plot.data$year,sum)
totalshare <- data.frame()
for ( i in 1:length(test)) {
	totalshare[i,1] <- years[i]
	totalshare[i,2] <- test[[i]]
	}
colnames(totalshare) <- c("years", "share")

par(new = TRUE)
plot(totalshare$share~totalshare$years, ylim=c(0.0,0.28), xlab = "", ylab = "",axes=FALSE, lwd=2, type="o",col="darkorchid2")
axis(side=4, at = pretty(c(0.0,0.28)))
mtext("Total share all ages", side=4, line=2)

dev.off()

###############################################################################
###Within-Group Inequality over Time##

edus <- c(0,1,2)
years <- unique(resultsf$year)
agemins <- unique(resultsf$age.min)
#Males

wg.gaps.m <- data.frame()
wg.vars.m <- data.frame()

for (i in edus) {
	for (j in years) {
		
		tmp <- subset(resultsm, resultsm$year==j & resultsm$bildcat_gr==i)
		tmp$normshare <- 0
		tmpsum <- sum(tmp$share)
		for (y in agemins) {
		tmp[tmp$age.min==y,]$normshare <- tmp[tmp$age.min==y,]$share/tmpsum
		}
		
		wg.gaps.m[j-1984,1] <- j
		wg.gaps.m[j-1984,i+2] <- wtd.mean(tmp$eight15, weights= tmp$normshare)
		wg.vars.m[j-1984, i+1] <- wtd.mean(tmp$variance, weights=tmp$normshare)
		}
	}	

colnames(wg.gaps.m) <- c("year", "Gap.No", "Gap.Voc", "Gap.College")
colnames(wg.vars.m) <- c( "Var.No", "Var.Voc", "Var.College")

wg.m <- cbind(wg.gaps.m, wg.vars.m)

###########################Plot of WG-inequality by group

#MALES

setEPS()
postscript("Graphs/shares_over_time/WG-inequality_by_edu.M.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(wg.m$Gap.College~wg.m$year, xlim=c(1985,2011), ylim=c(0.45,0.95), xaxt="n", main="Male within-group inequality by education", xlab="Time", ylab="Within group 85/15 gap in log wages", type="o", lwd=2, col="deepskyblue")
axis(side=1, at=wg.m$year, labels=wg.m$year, cex.axis=0.9)

lines(wg.m$Gap.Voc~wg.m$year, type="o", col="green4", lwd=2)

lines(wg.m$Gap.No~wg.m$year, type="o", col="black", lwd=2)

legend(1985,0.95, c("College", "Vocational", "No degree"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("deepskyblue","green4", "gray15")) # gives the legend lines the correct color and widt

dev.off()


#Females

wg.gaps.f <- data.frame()
wg.vars.f <- data.frame()

for (i in edus) {
	for (j in years) {
		
		tmp <- subset(resultsf, resultsf$year==j & resultsf$bildcat_gr==i)
		tmp$normshare <- 0
		tmpsum <- sum(tmp$share)
		for (y in agemins) {
		tmp[tmp$age.min==y,]$normshare <- tmp[tmp$age.min==y,]$share/tmpsum
		}
		wg.gaps.f[j-1984,1] <- j
		wg.gaps.f[j-1984,i+2] <- wtd.mean(tmp$eight15, weights= tmp$normshare)
		wg.vars.f[j-1984, i+1] <- wtd.mean(tmp$variance, weights=tmp$normshare)
		}
	}	

colnames(wg.gaps.f) <- c("year", "Gap.No", "Gap.Voc", "Gap.College")
colnames(wg.vars.f) <- c( "Var.No", "Var.Voc", "Var.College")

wg.f <- cbind(wg.gaps.f, wg.vars.f)


setEPS()
postscript("Graphs/shares_over_time/WG-inequality_by_edu.F.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(wg.f$Gap.College~wg.f$year, xlim=c(1985,2011), ylim=c(0.45,0.95), xaxt="n", main="Female within-group inequality by education", xlab="Time", ylab="Within group 85/15 gap in log wages", type="o", lwd=2, col="deepskyblue")
axis(side=1, at=wg.f$year, labels=wg.f$year, cex.axis=0.9)

#lines(wg.f$Gap.HS~wg.f$year,, type="o", col="cyan2", lwd=2)

lines(wg.f$Gap.Voc~wg.f$year, type="o", col="green4", lwd=2)

lines(wg.f$Gap.No~wg.f$year, type="o", col="gray15", lwd=2)

legend(1985,0.95, c("College", "Vocational", "No degree"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("deepskyblue","green4", "gray15")) # gives the legend lines the correct color and widt

dev.off()

