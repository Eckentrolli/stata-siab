#Calculate Variance & gaps for age/education cells
#West germany fulltime workers only

options(scipen=30)

library(foreign)
library(Hmisc)
library(parallel)
library(car)
library(xtable)

#setwd("C:/Documents/Computing/Stata-siab")
setwd("C:/Dokumente/Uni/Wage Inequality/Results")

#load("E:/DATA/Residuals.RData")
load("I:/DATA/Residuals.RData")

#Create  bildcat variable with only 4 values (analogous to Dustman)
#Bildcat labels: Kein = 0, Schulabschluss = 1, Ausbildung = 2, Hochschule = 3

trim$bildcat <- recode(trim$IP2A, "c(-9, 1)= 0; 2=1; c(3,4)=2; c(5,6)= 3")

#Make two tables, male and female each
#Define cells with 
#Bildung 6x
#Age groups 17-30 = 1, 31-40 = 2, 41-50 =3 , 51-62 =4 4x
#Years 1985,1990,2000,2004,2010

#Create subsamples of male and female workers
males <- subset(trim, frau==0)
females <- subset(trim, frau==1)

rm(trim)

#Choose analysis years to expand on Dustman(2009):
# 1985, 1990, 2000, 2004, 2010

years <- c(1985,1990,2000,2004,2010)
bilds <- c(1,2,3,4,5,6)
ages <- cbind (c(18,31,41,51),c(30,40,50,62))

cellsm <- matrix(nrow=((length(bilds))*nrow(ages)), ncol=length(years)+1)
cellsf <- matrix(nrow=((length(bilds)+1)*nrow(ages)), ncol=length(years))


rownames(cellsm) <- c(rep("College", 4), rep("Technical", 4), rep("Highschool&Vocational", 4), rep("High School", 4), rep("Vocational", 4), rep("No degree",4) )
#rownames(cellsm) <- c("Kein1", "Ausb1", "Abi1", "Hoch1","Kein2", "Ausb2", "Abi2", "Hoch2","Kein3", "Ausbl3", "Abib3", "Hoch3","Kein4", "Ausb4", "Abi4", "Hoch4")
colnames(cellsm) <-c("Age","1985","1990","2000","2004", "2010")

# rownames(cellsf) <- c("No degree", "18-30", "32-40", "42-50", "51-62", "Vocational", "18-30", "32-40", "42-50", "51-62", "High School", "18-30", "32-40", "42-50", "51-62", "College", "18-30", "32-40", "42-50", "51-62")
# colnames(cellsf) <-c("1985","1990","2000","2006", "2010")

#create empty cells matrices
#cellsm[,1] <-c(rep(18,4),rep(31,4), rep(41,4),rep(50,4))

cellsf <- cellsm

#create input matrix with all cell criteria
input <- expand.grid(years, bilds, ages[,1])
tmp <- expand.grid(years, bilds, ages[,2])
input <- cbind(input, tmp[3])
input$Var2 <- as.factor(input$Var2)


#For each age/education cell and year, the mean, variance and quantile gaps are calculated
# for values and residuals

calculatem <- function (i) {
	sample1 <- subset(males, year==i[[1]] & bild==i[[2]] & age>=i[[3]] & age <=i[[4]])
	groupmean <- wtd.mean(sample1$values, weights=sample1$sweight, na.rm=FALSE)
	variance <- wtd.var(sample1$values, weights=sample1$sweight, na.rm=FALSE)
	SD <- sqrt(wtd.var(sample1$values, weights=sample1$sweight, na.rm=FALSE))
	resvar <- wtd.var(sample1$residualsmf, weights=sample1$sweight, na.rm=FALSE)
	resSD <- sqrt(wtd.var(sample1$residualsmf, weights=sample1$sweight, na.rm=FALSE))
	quants <- wtd.quantile(sample1$values, probs=c(0.85,0.5,0.15),, weights=sample1$sweight, na.rm=FALSE))
	resQuants <- wtd.quantile(sample1$residualsmf, probs=c(0.85,0.5,0.15), weights=sample1$sweight, na.rm=FALSE))
	nine50 <- (quants["85%"] - quants["50%"])
	nine10 <- (quants["85%"] - quants["15%"])
	five10 <- (quants["50%"] - quants["15%"])
	
	resnine50 <- (resQuants["85%"] - resQuants["50%"])
	resnine10 <- (resQuants["85%"] - resQuants["15%"])
	resfive10 <- (resQuants["50%"] - resQuants["15%"])
	obs <- nrow(sample1)
	share <- obs/nrow(males[males$year==i[[1]],])
	rm(sample1)
	return(c(obs, groupmean, share, variance, SD, nine50, nine10, five10, resvar, resSD, resnine50, resnine10, resfive10))
	}
	
resultsm <- t(apply(input, 1, calculatem))

#Attach results collumns to input collumns, new results dataframe with all relevant data
resultsm <- cbind(input, resultsm)
	
colnames(resultsm) <- c("years", "bild", "agemin", "agemax", "obs", "mean", "share", "variance","SD", "eight50", "eight15", "five15", "Res.variance", "Res.SD", "Res.nine50","Res.nine10", "Res.five10")

#Order the results as year-blocks in table res.form.m

for (i in years) {
	tmp <- subset(resultsm, years==i)
	nam <- paste("tmp", i, sep = "")
	assign(nam, tmp)
	}

res.form.m <- rbind(tmp1985, tmp1990, tmp2000, tmp2004, tmp2010)

rm(tmp1985,tmp1990, tmp2000, tmp2004, tmp2010)
	

	
#calculation females, same as above, only for dataset "females"

calculatef <- function (i) {
	sample1 <- subset(females, year==i[[1]] & bild==i[[2]] & age>=i[[3]] & age <=i[[4]])
	groupmean <- wtd.mean(sample1$values, weights=sample1$sweight, na.rm=FALSE)
	variance <- wtd.var(sample1$values,weights=sample1$sweight, na.rm=FALSE)
	SD <- sqrt(wtd.var(sample1$values,weights=sample1$sweight, na.rm=FALSE))
	resvar <- wtd.var(sample1$residualsmf,weights=sample1$sweight, na.rm=FALSE)
	resSD <- sqrt(wtd.var(sample1$residualsmf,weights=sample1$sweight, na.rm=FALSE))
	quants <- wtd.quantile(sample1$values, probs=c(0.85,0.5,0.15),weights=sample1$sweight, na.rm=FALSE)
	resQuants <- wtd.quantile(sample1$residualsmf, probs=c(0.85,0.5,0.15),weights=sample1$sweight, na.rm=FALSE)
	nine50 <- (quants["85%"] - quants["50%"])
	nine10 <- (quants["85%"] - quants["15%"])
	five10 <- (quants["50%"] - quants["15%"])
	
	resnine50 <- (resQuants["85%"] - resQuants["50%"])
	resnine10 <- (resQuants["85%"] - resQuants["15%"])
	resfive10 <- (resQuants["50%"] - resQuants["15%"])
	obs <- nrow(sample1)
	share <- obs/nrow(males[males$year==i[[1]],])
	rm(sample1)
	return(c(obs, groupmean, share, variance, SD, nine50, nine10, five10, resvar, resSD, resnine50, resnine10, resfive10))
	}
	
resultsf <- t(apply(input, 1, calculatef))
	
resultsf <- cbind(input, resultsf)

colnames(resultsf) <- c("years", "bild", "agemin", "agemax", "obs", "mean", "share", "variance","SD", "eight50", "eight15", "five15", "Res.variance", "Res.SD", "Res.nine50","Res.nine10", "Res.five10")

for (i in years) {
	tmp <- subset(resultsf, years==i)
	nam <- paste("tmp", i, sep = "")
	assign(nam, tmp)
	}

res.form.f <- rbind(tmp1985,tmp1990, tmp2000, tmp2004, tmp2010)

#Create separate Sub-Tables for each educational outcome

#MALES

#University 
edu <- 6

unim <- matrix(ncol=length(years)+2, nrow=(3*nrow(ages)))


colnames(unim)[3:(length(years)+2)] <- years

colnames(unim)[1:2] <- c("Ages", "Statistics")

unim[,1]<-c(rep(paste(ages[1,1],"-",ages[1,2]),3), rep(paste(ages[2,1],"-",ages[2,2]),3), rep(paste(ages[3,1],"-",ages[3,2]),3), rep(paste(ages[4,1],"-",ages[4,2]),3))

#rownames(unim) <- c(rep(paste(ages[1,1],"-",ages[1,2]),3), rep(paste(ages[2,1],"-",ages[2,2]),3), rep(paste(ages[3,1],"-",ages[3,2]),3), rep(paste(ages[4,1],"-",ages[4,2]),3))

unim[,2] <- rep(c("Variance","85/15-gap", "Share"), 4)

for (i in years) {
		unim[1,paste(i)] <- round(resultsm$Res.variance[resultsm$years==i & resultsm$agemin==ages[1,1] & resultsm$bild==edu],digits=3)
		unim[2,paste(i)] <- round(resultsm$eight15[resultsm$years==i & resultsm$agemin==ages[1,1] & resultsm$bild==edu],digits=3)
		unim[3,paste(i)] <- round(resultsm$share[resultsm$years==i & resultsm$agemin==ages[1,1] & resultsm$bild==edu] ,digits=3)
		unim[4,paste(i)] <- round(resultsm$Res.variance[resultsm$years==i & resultsm$agemin==ages[2,1] & resultsm$bild==edu],digits=3)
		unim[5,paste(i)] <- round(resultsm$eight15[resultsm$years==i & resultsm$agemin==ages[2,1] & resultsm$bild==edu],digits=3)
		unim[6,paste(i)] <- round(resultsm$share[resultsm$years==i & resultsm$agemin==ages[2,1] & resultsm$bild==edu],digits=3)
		unim[7,paste(i)] <- round(resultsm$Res.variance[resultsm$years==i & resultsm$agemin==ages[3,1] & resultsm$bild==edu],digits=3)
		unim[8,paste(i)] <- round(resultsm$eight15[resultsm$years==i & resultsm$agemin==ages[3,1] & resultsm$bild==edu],digits=3)
		unim[9,paste(i)] <- round(resultsm$share[resultsm$years==i & resultsm$agemin==ages[3,1] & resultsm$bild==edu],digits=3)
		unim[10,paste(i)] <- round(resultsm$Res.variance[resultsm$years==i & resultsm$agemin==ages[4,1] & resultsm$bild==edu],digits=3)
		unim[11,paste(i)] <- round(resultsm$eight15[resultsm$years==i & resultsm$agemin==ages[4,1] & resultsm$bild==edu],digits=3)
		unim[12,paste(i)] <- round(resultsm$share[resultsm$years==i & resultsm$agemin==ages[4,1] & resultsm$bild==edu],digits=3)
		}

unimtab <- xtable(unim, caption="University degree")	
print(unimtab, include.rownames=FALSE, file="Tables/unimtab.tex")

#Technical
edu <- 5

techm <- matrix(ncol=length(years)+2, nrow=(3*nrow(ages)))

colnames(techm)[3:(length(years)+2)] <- years

colnames(techm)[1:2] <- c("Ages", "Statistics")

techm[,1]<-c(rep(paste(ages[1,1],"-",ages[1,2]),3), rep(paste(ages[2,1],"-",ages[2,2]),3), rep(paste(ages[3,1],"-",ages[3,2]),3), rep(paste(ages[4,1],"-",ages[4,2]),3))

#rownames(techm) <- c(rep(paste(ages[1,1],"-",ages[1,2]),3), rep(paste(ages[2,1],"-",ages[2,2]),3), rep(paste(ages[3,1],"-",ages[3,2]),3), rep(paste(ages[4,1],"-",ages[4,2]),3))

techm[,2] <- rep(c("Variance","85/15-gap", "Share"), 4)

for (i in years) {
		techm[1,paste(i)] <- round(resultsm$Res.variance[resultsm$years==i & resultsm$agemin==ages[1,1] & resultsm$bild==edu],digits=3)
		techm[2,paste(i)] <- round(resultsm$eight15[resultsm$years==i & resultsm$agemin==ages[1,1] & resultsm$bild==edu],digits=3)
		techm[3,paste(i)] <- round(resultsm$share[resultsm$years==i & resultsm$agemin==ages[1,1] & resultsm$bild==edu] ,digits=3)
		techm[4,paste(i)] <- round(resultsm$Res.variance[resultsm$years==i & resultsm$agemin==ages[2,1] & resultsm$bild==edu],digits=3)
		techm[5,paste(i)] <- round(resultsm$eight15[resultsm$years==i & resultsm$agemin==ages[2,1] & resultsm$bild==edu],digits=3)
		techm[6,paste(i)] <- round(resultsm$share[resultsm$years==i & resultsm$agemin==ages[2,1] & resultsm$bild==edu],digits=3)
		techm[7,paste(i)] <- round(resultsm$Res.variance[resultsm$years==i & resultsm$agemin==ages[3,1] & resultsm$bild==edu],digits=3)
		techm[8,paste(i)] <- round(resultsm$eight15[resultsm$years==i & resultsm$agemin==ages[3,1] & resultsm$bild==edu],digits=3)
		techm[9,paste(i)] <- round(resultsm$share[resultsm$years==i & resultsm$agemin==ages[3,1] & resultsm$bild==edu],digits=3)
		techm[10,paste(i)] <- round(resultsm$Res.variance[resultsm$years==i & resultsm$agemin==ages[4,1] & resultsm$bild==edu],digits=3)
		techm[11,paste(i)] <- round(resultsm$eight15[resultsm$years==i & resultsm$agemin==ages[4,1] & resultsm$bild==edu],digits=3)
		techm[12,paste(i)] <- round(resultsm$share[resultsm$years==i & resultsm$agemin==ages[4,1] & resultsm$bild==edu],digits=3)
		}

techmtab <- xtable(techm, caption="Technical college degree")	
print(techmtab, include.rownames=FALSE, file="Tables/techmtab.tex")

#Vocational + Highschool
edu <- 4

vochighm <- matrix(ncol=length(years)+2, nrow=(3*nrow(ages)))

colnames(vochighm)[3:(length(years)+2)] <- years

colnames(vochighm)[1:2] <- c("Ages", "Statistics")

vochighm[,1]<-c(rep(paste(ages[1,1],"-",ages[1,2]),3), rep(paste(ages[2,1],"-",ages[2,2]),3), rep(paste(ages[3,1],"-",ages[3,2]),3), rep(paste(ages[4,1],"-",ages[4,2]),3))

#rownames(vochighm) <- c(rep(paste(ages[1,1],"-",ages[1,2]),3), rep(paste(ages[2,1],"-",ages[2,2]),3), rep(paste(ages[3,1],"-",ages[3,2]),3), rep(paste(ages[4,1],"-",ages[4,2]),3))

vochighm[,2] <- rep(c("Variance","85/15-gap", "Share"), 4)

for (i in years) {
		vochighm[1,paste(i)] <- round(resultsm$Res.variance[resultsm$years==i & resultsm$agemin==ages[1,1] & resultsm$bild==edu],digits=3)
		vochighm[2,paste(i)] <- round(resultsm$eight15[resultsm$years==i & resultsm$agemin==ages[1,1] & resultsm$bild==edu],digits=3)
		vochighm[3,paste(i)] <- round(resultsm$share[resultsm$years==i & resultsm$agemin==ages[1,1] & resultsm$bild==edu] ,digits=3)
		vochighm[4,paste(i)] <- round(resultsm$Res.variance[resultsm$years==i & resultsm$agemin==ages[2,1] & resultsm$bild==edu],digits=3)
		vochighm[5,paste(i)] <- round(resultsm$eight15[resultsm$years==i & resultsm$agemin==ages[2,1] & resultsm$bild==edu],digits=3)
		vochighm[6,paste(i)] <- round(resultsm$share[resultsm$years==i & resultsm$agemin==ages[2,1] & resultsm$bild==edu],digits=3)
		vochighm[7,paste(i)] <- round(resultsm$Res.variance[resultsm$years==i & resultsm$agemin==ages[3,1] & resultsm$bild==edu],digits=3)
		vochighm[8,paste(i)] <- round(resultsm$eight15[resultsm$years==i & resultsm$agemin==ages[3,1] & resultsm$bild==edu],digits=3)
		vochighm[9,paste(i)] <- round(resultsm$share[resultsm$years==i & resultsm$agemin==ages[3,1] & resultsm$bild==edu],digits=3)
		vochighm[10,paste(i)] <- round(resultsm$Res.variance[resultsm$years==i & resultsm$agemin==ages[4,1] & resultsm$bild==edu],digits=3)
		vochighm[11,paste(i)] <- round(resultsm$eight15[resultsm$years==i & resultsm$agemin==ages[4,1] & resultsm$bild==edu],digits=3)
		vochighm[12,paste(i)] <- round(resultsm$share[resultsm$years==i & resultsm$agemin==ages[4,1] & resultsm$bild==edu],digits=3)
		}

vochighmtab <- xtable(vochighm, caption="Vocational & High school degree")	
print(vochighmtab, include.rownames=FALSE, file="Tables/vochighmtab.tex")


#High School
edu <- 3

highm <- matrix(ncol=length(years)+2, nrow=(3*nrow(ages)))

colnames(highm)[3:(length(years)+2)] <- years

colnames(highm)[1:2] <- c("Ages", "Statistics")

highm[,1]<-c(rep(paste(ages[1,1],"-",ages[1,2]),3), rep(paste(ages[2,1],"-",ages[2,2]),3), rep(paste(ages[3,1],"-",ages[3,2]),3), rep(paste(ages[4,1],"-",ages[4,2]),3))

#rownames(highm) <- c(rep(paste(ages[1,1],"-",ages[1,2]),3), rep(paste(ages[2,1],"-",ages[2,2]),3), rep(paste(ages[3,1],"-",ages[3,2]),3), rep(paste(ages[4,1],"-",ages[4,2]),3))

highm[,2] <- rep(c("Variance","85/15-gap", "Share"), 4)

for (i in years) {
		highm[1,paste(i)] <- round(resultsm$Res.variance[resultsm$years==i & resultsm$agemin==ages[1,1] & resultsm$bild==edu],digits=3)
		highm[2,paste(i)] <- round(resultsm$eight15[resultsm$years==i & resultsm$agemin==ages[1,1] & resultsm$bild==edu],digits=3)
		highm[3,paste(i)] <- round(resultsm$share[resultsm$years==i & resultsm$agemin==ages[1,1] & resultsm$bild==edu] ,digits=3)
		highm[4,paste(i)] <- round(resultsm$Res.variance[resultsm$years==i & resultsm$agemin==ages[2,1] & resultsm$bild==edu],digits=3)
		highm[5,paste(i)] <- round(resultsm$eight15[resultsm$years==i & resultsm$agemin==ages[2,1] & resultsm$bild==edu],digits=3)
		highm[6,paste(i)] <- round(resultsm$share[resultsm$years==i & resultsm$agemin==ages[2,1] & resultsm$bild==edu],digits=3)
		highm[7,paste(i)] <- round(resultsm$Res.variance[resultsm$years==i & resultsm$agemin==ages[3,1] & resultsm$bild==edu],digits=3)
		highm[8,paste(i)] <- round(resultsm$eight15[resultsm$years==i & resultsm$agemin==ages[3,1] & resultsm$bild==edu],digits=3)
		highm[9,paste(i)] <- round(resultsm$share[resultsm$years==i & resultsm$agemin==ages[3,1] & resultsm$bild==edu],digits=3)
		highm[10,paste(i)] <- round(resultsm$Res.variance[resultsm$years==i & resultsm$agemin==ages[4,1] & resultsm$bild==edu],digits=3)
		highm[11,paste(i)] <- round(resultsm$eight15[resultsm$years==i & resultsm$agemin==ages[4,1] & resultsm$bild==edu],digits=3)
		highm[12,paste(i)] <- round(resultsm$share[resultsm$years==i & resultsm$agemin==ages[4,1] & resultsm$bild==edu],digits=3)
		}

highmtab <- xtable(highm, caption="High school degree")	
print(highmtab, include.rownames=FALSE, file="Tables/highmtab.tex")




#Vocational
edu <- 2

vocm <- matrix(ncol=length(years)+2, nrow=(3*nrow(ages)))

colnames(vocm)[3:(length(years)+2)] <- years

colnames(vocm)[1:2] <- c("Ages", "Statistics")

vocm[,1]<-c(rep(paste(ages[1,1],"-",ages[1,2]),3), rep(paste(ages[2,1],"-",ages[2,2]),3), rep(paste(ages[3,1],"-",ages[3,2]),3), rep(paste(ages[4,1],"-",ages[4,2]),3))

#rownames(vocm) <- c(rep(paste(ages[1,1],"-",ages[1,2]),3), rep(paste(ages[2,1],"-",ages[2,2]),3), rep(paste(ages[3,1],"-",ages[3,2]),3), rep(paste(ages[4,1],"-",ages[4,2]),3))

vocm[,2] <- rep(c("Variance","85/15-gap", "Share"), 4)

for (i in years) {
		vocm[1,paste(i)] <- round(resultsm$Res.variance[resultsm$years==i & resultsm$agemin==ages[1,1] & resultsm$bild==edu],digits=3)
		vocm[2,paste(i)] <- round(resultsm$eight15[resultsm$years==i & resultsm$agemin==ages[1,1] & resultsm$bild==edu],digits=3)
		vocm[3,paste(i)] <- round(resultsm$share[resultsm$years==i & resultsm$agemin==ages[1,1] & resultsm$bild==edu] ,digits=3)
		vocm[4,paste(i)] <- round(resultsm$Res.variance[resultsm$years==i & resultsm$agemin==ages[2,1] & resultsm$bild==edu],digits=3)
		vocm[5,paste(i)] <- round(resultsm$eight15[resultsm$years==i & resultsm$agemin==ages[2,1] & resultsm$bild==edu],digits=3)
		vocm[6,paste(i)] <- round(resultsm$share[resultsm$years==i & resultsm$agemin==ages[2,1] & resultsm$bild==edu],digits=3)
		vocm[7,paste(i)] <- round(resultsm$Res.variance[resultsm$years==i & resultsm$agemin==ages[3,1] & resultsm$bild==edu],digits=3)
		vocm[8,paste(i)] <- round(resultsm$eight15[resultsm$years==i & resultsm$agemin==ages[3,1] & resultsm$bild==edu],digits=3)
		vocm[9,paste(i)] <- round(resultsm$share[resultsm$years==i & resultsm$agemin==ages[3,1] & resultsm$bild==edu],digits=3)
		vocm[10,paste(i)] <- round(resultsm$Res.variance[resultsm$years==i & resultsm$agemin==ages[4,1] & resultsm$bild==edu],digits=3)
		vocm[11,paste(i)] <- round(resultsm$eight15[resultsm$years==i & resultsm$agemin==ages[4,1] & resultsm$bild==edu],digits=3)
		vocm[12,paste(i)] <- round(resultsm$share[resultsm$years==i & resultsm$agemin==ages[4,1] & resultsm$bild==edu],digits=3)
		}

vocmtab <- xtable(vocm, caption="Vocational degree")	
print(vocmtab, include.rownames=FALSE, file="Tables/vocmtab.tex")


#No Degree
edu <- 1

nodegm <- matrix(ncol=length(years)+2, nrow=(3*nrow(ages)))

colnames(nodegm)[3:(length(years)+2)] <- years

colnames(nodegm)[1:2] <- c("Ages", "Statistics")

nodegm[,1]<-c(rep(paste(ages[1,1],"-",ages[1,2]),3), rep(paste(ages[2,1],"-",ages[2,2]),3), rep(paste(ages[3,1],"-",ages[3,2]),3), rep(paste(ages[4,1],"-",ages[4,2]),3))

#rownames(nodegm) <- c(rep(paste(ages[1,1],"-",ages[1,2]),3), rep(paste(ages[2,1],"-",ages[2,2]),3), rep(paste(ages[3,1],"-",ages[3,2]),3), rep(paste(ages[4,1],"-",ages[4,2]),3))

nodegm[,2] <- rep(c("Variance","85/15-gap", "Share"), 4)

for (i in years) {
		nodegm[1,paste(i)] <- round(resultsm$Res.variance[resultsm$years==i & resultsm$agemin==ages[1,1] & resultsm$bild==edu],digits=3)
		nodegm[2,paste(i)] <- round(resultsm$eight15[resultsm$years==i & resultsm$agemin==ages[1,1] & resultsm$bild==edu],digits=3)
		nodegm[3,paste(i)] <- round(resultsm$share[resultsm$years==i & resultsm$agemin==ages[1,1] & resultsm$bild==edu] ,digits=3)
		nodegm[4,paste(i)] <- round(resultsm$Res.variance[resultsm$years==i & resultsm$agemin==ages[2,1] & resultsm$bild==edu],digits=3)
		nodegm[5,paste(i)] <- round(resultsm$eight15[resultsm$years==i & resultsm$agemin==ages[2,1] & resultsm$bild==edu],digits=3)
		nodegm[6,paste(i)] <- round(resultsm$share[resultsm$years==i & resultsm$agemin==ages[2,1] & resultsm$bild==edu],digits=3)
		nodegm[7,paste(i)] <- round(resultsm$Res.variance[resultsm$years==i & resultsm$agemin==ages[3,1] & resultsm$bild==edu],digits=3)
		nodegm[8,paste(i)] <- round(resultsm$eight15[resultsm$years==i & resultsm$agemin==ages[3,1] & resultsm$bild==edu],digits=3)
		nodegm[9,paste(i)] <- round(resultsm$share[resultsm$years==i & resultsm$agemin==ages[3,1] & resultsm$bild==edu],digits=3)
		nodegm[10,paste(i)] <- round(resultsm$Res.variance[resultsm$years==i & resultsm$agemin==ages[4,1] & resultsm$bild==edu],digits=3)
		nodegm[11,paste(i)] <- round(resultsm$eight15[resultsm$years==i & resultsm$agemin==ages[4,1] & resultsm$bild==edu],digits=3)
		nodegm[12,paste(i)] <- round(resultsm$share[resultsm$years==i & resultsm$agemin==ages[4,1] & resultsm$bild==edu],digits=3)
		}

nodegmtab <- xtable(nodegm, caption="No degree")	
print(nodegmtab, include.rownames=FALSE, file="Tables/nodegmtab.tex")

