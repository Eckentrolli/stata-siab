options("scipen"=30, "digits"=2)

library(foreign)
library(Hmisc)
library(parallel)
library(car)

setwd("E:/Data")
#setwd("I:/Data")

load(file = "E:/Data/Residuals.RData",)

years <- c(1998, 1999, 2000, 2001, 2002, 2003)

quants <-list()

for (i in years) {
	sample <- subset(trim, year==i)
	quants[[i-1997]]<- quantile(sample$discount, probs=seq(0,1,by=0.01),na.rm=FALSE)
	rm(sample)
	}
	
names(quants) <-years

sink("Quants_for_Hickup.txt", split=TRUE) 
quants
sink()