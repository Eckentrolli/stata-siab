options(scipen=30)

library(foreign)
library(Hmisc)
library(WriteXLS)
library(xtable)


setwd("/u/home/jd1033/DFG-descriptives/Results/FT")
#setwd("T:/")


load("reweighting/trim.m.rw10.occ.RData")

years <- sort(unique(trim.m.rw.occ$year),decreasing =TRUE)
trim.m.rw.occ$occweight <- trim.m.rw.occ$sweight*trim.m.rw.occ$base10occ



###############################################
#########Variance Tables#####################
##############################################

#males
# x.table <- xtable(variances[1:26,], caption ="Variance and Residuals for male workers")
# print(x.table, file="Tables/VarMalesFT1.tex")
# rm(x.table)

# xltable <- variances[1:26,]
# WriteXLS("xltable", ExcelFileName="Tables/VarMalesFT85.xls", row.names=TRUE)

#females
# x.table <- xtable(variances[27:52,], caption ="Variance and Residuals for female workers")
# print(x.table, file="Tables/VarFemFT85.tex")
# rm(x.table)

# xltable <- variances[27:52,]
# WriteXLS("xltable", ExcelFileName="Tables/VarFemFT85.xls", row.names=TRUE)


################################################
######### Gap-Plots ##########################
################################################

#Calculate Gaps for M/F separate

#MALES

gapsnorw <- data.frame()
gaps <- data.frame()
vars <- data.frame()

for (i in years) {

	tmp <- subset(trim.m.rw.occ, trim.m.rw.occ$year==i)
	quants<- wtd.quantile(tmp$values, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$occweight)
	resQuants <- wtd.quantile(tmp$residualsmf, probs=c(0.90,0.5,0.10), na.rm=FALSE, weights=tmp$occweight)
	resVar <- wtd.var(tmp$residualsmf, weights=tmp$occweight)
	
	quants.norw<- wtd.quantile(tmp$values, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$sweight)
	resQuants.norw <- wtd.quantile(tmp$residualsmf, probs=c(0.90,0.5,0.10), na.rm=FALSE, weights=tmp$sweight)
	resVar.norw <- wtd.var(tmp$residualsmf, weights=tmp$sweight)
	totalVar <- wtd.var(tmp$discount, weights=tmp$sweight)
	
	eight50 <- (quants["85%"] - quants["50%"])
	eight15 <- (quants["85%"] - quants["15%"])
	five15 <- (quants["50%"] - quants["15%"])
	
	eight50.norw <- (quants.norw["85%"] - quants.norw["50%"])
	eight15.norw <- (quants.norw["85%"] - quants.norw["15%"])
	five15.norw <- (quants.norw["50%"] - quants.norw["15%"])
		
	res9050 <- (resQuants["90%"] - resQuants["50%"])
	res9010 <- (resQuants["90%"] - resQuants["10%"])
	res5010 <- (resQuants["50%"] - resQuants["10%"])
	
	res9050.norw <- (resQuants.norw["90%"] - resQuants.norw["50%"])
	res9010.norw <- (resQuants.norw["90%"] - resQuants.norw["10%"])
	res5010.norw <- (resQuants.norw["50%"] - resQuants.norw["10%"])
	
	gaps[2011-i,1] <- eight50
	gaps[2011-i,2] <- eight15
	gaps[2011-i,3] <- five15
	gaps[2011-i,4] <- res9050
	gaps[2011-i,5] <- res9010
	gaps[2011-i,6] <- res5010
	gaps[2011-i,7] <- quants["85%"]
	gaps[2011-i,8] <- quants["50%"]
	gaps[2011-i,9] <- quants["15%"]
	gaps[2011-i,10] <-resQuants["90%"]
	gaps[2011-i,11] <-resQuants["50%"]
	gaps[2011-i,12] <-resQuants["10%"]
	
	gapsnorw[2011-i,1] <- eight50.norw
	gapsnorw[2011-i,2] <- eight15.norw
	gapsnorw[2011-i,3] <- five15.norw
	gapsnorw[2011-i,4] <- res9050.norw
	gapsnorw[2011-i,5] <- res9010.norw
	gapsnorw[2011-i,6] <- res5010.norw
	gapsnorw[2011-i,7] <- quants.norw["85%"]
	gapsnorw[2011-i,8] <- quants.norw["50%"]
	gapsnorw[2011-i,9] <- quants.norw["15%"]
	gapsnorw[2011-i,10] <- resQuants.norw["90%"]
	gapsnorw[2011-i,11] <- resQuants.norw["50%"]
	gapsnorw[2011-i,12] <- resQuants.norw["10%"]
	
	vars[2011-i,1] <- resVar
	vars[2011-i,2] <- resVar.norw
	vars[2011-i,3] <- totalVar
	
}

colnames(gaps) <- c("eight50", "eight15", "five15", "res9050",  "res9010", "res5010" ,"85% quantile", "Median", "15% quantile", "resQuant90", "resQuant50", "resQuant10")
gaps <- cbind(years, gaps)
colnames(gaps)[1]<-"years"
rownames(gaps)<-gaps$years

colnames(gapsnorw) <- c("eight50", "eight15", "five15", "res9050", "res9010", "res5010" ,"85% quantile", "Median", "15% quantile","resQuant90", "resQuant50", "resQuant10")
gapsnorw <- cbind(years, gapsnorw)
colnames(gapsnorw)[1]<-"years"
rownames(gapsnorw)<-gapsnorw$years


colnames(vars) <- c("Residual Variance Baseyear 2010", "Residual Variance", "Total Variance")
vars <- cbind (years, vars)
rownames(vars)<-vars$years

sink("OccQ-gapsRW10.txt", split=TRUE)
gaps
sink()

sink("OccQ-gaps.txt", split=TRUE)
gapsnorw
sink()

sink("OccQ-vars10.txt", split=TRUE)
vars
sink()

save(gapsnorw, gaps, vars, file="OccGaps_&_NoRW.10.M.RData")

##################################################################################
##################Explained share quantile gaps
###########################################

tab.exshare.10.m <- data.frame()

tab.exshare.10.m[1,1] <- (gapsnorw["2010","eight50"]-gapsnorw["1985","eight50"])
tab.exshare.10.m[2,1] <- (gapsnorw["2010","eight15"]-gapsnorw["1985","eight15"])
tab.exshare.10.m[3,1] <- (gapsnorw["2010","five15"]-gapsnorw["1985","five15"])

tab.exshare.10.m[1,2] <- (gaps["2010","eight50"]-gaps["1985","eight50"])
tab.exshare.10.m[2,2] <- (gaps["2010","eight15"]-gaps["1985","eight15"])
tab.exshare.10.m[3,2] <- (gaps["2010","five15"]-gaps["1985","five15"])

tab.exshare.10.m[1,3] <- 1-(tab.exshare.10.m[1,2]/(tab.exshare.10.m[1,1]))
tab.exshare.10.m[2,3] <- 1-(tab.exshare.10.m[2,2]/(tab.exshare.10.m[2,1]))
tab.exshare.10.m[3,3] <- 1-(tab.exshare.10.m[3,2]/(tab.exshare.10.m[3,1]))


colnames(tab.exshare.10.m) <- c("Quantile gap", "Quantile gap baseyear 2010", "Explained share")
rownames(tab.exshare.10.m) <- c("85/50 gap", "85/15 gap", "50/15 gap")

sink("OccExShareRW10.M.txt", split=TRUE)
tab.exshare.10.m
sink()

##################################################################################
##################Explained share residual variannce
###########################################

tab.exshare.res.10.m <- data.frame()

tab.exshare.res.10.m[1,1] <- (gapsnorw["2010","res9050"]-gapsnorw["1985","res9050"])
tab.exshare.res.10.m[2,1] <- (gapsnorw["2010","res9010"]-gapsnorw["1985","res9010"])
tab.exshare.res.10.m[3,1] <- (gapsnorw["2010","res5010"]-gapsnorw["1985","res5010"])
tab.exshare.res.10.m[4,1] <- (vars["2010","Residual Variance"]-vars["1985","Residual Variance"])

tab.exshare.res.10.m[1,2] <- (gaps["2010","res9050"]-gaps["1985","res9050"])
tab.exshare.res.10.m[2,2] <- (gaps["2010","res9010"]-gaps["1985","res9010"])
tab.exshare.res.10.m[3,2] <- (gaps["2010","res5010"]-gaps["1985","res5010"])
tab.exshare.res.10.m[4,2] <- (vars["2010","Residual Variance Baseyear 2010"]-vars["1985","Residual Variance Baseyear 2010"])

tab.exshare.res.10.m[1,3] <- 1-(tab.exshare.res.10.m[1,2]/(tab.exshare.res.10.m[1,1]))
tab.exshare.res.10.m[2,3] <- 1-(tab.exshare.res.10.m[2,2]/(tab.exshare.res.10.m[2,1]))
tab.exshare.res.10.m[3,3] <- 1-(tab.exshare.res.10.m[3,2]/(tab.exshare.res.10.m[3,1]))
tab.exshare.res.10.m[4,3] <- 1-(tab.exshare.res.10.m[4,2]/(tab.exshare.res.10.m[4,1]))

colnames(tab.exshare.res.10.m) <- c("Residuals", "Residuals baseyear 2010", "Explained share")
rownames(tab.exshare.res.10.m) <- c("90/50 gap", "90/10 gap", "50/10 gap", "Residual variance")

sink("OccResidExShareRW10.M.txt", split=TRUE)
tab.exshare.res.10.m
sink()

#####################################################
################Variance Plots###########
#################################


setEPS()
postscript("Graphs/ResidualVar10.m.eps", width=8, heigh=7)


plot(vars[,"Residual Variance"]~vars$years, xlim=c(1985,2011),ylim=c(0.05,0.25), xaxt = "n",  main="Residual Variances for male workers", xlab="Time", ylab="Residual Variance", type="o", lwd=2, col="blue")
axis(side=1, at=vars$years, labels=vars$years, cex.axis=0.9)
lines(vars[,"Residual Variance Baseyear 2010"]~vars$years, type="o", col="red", lwd=2)
lines(vars[,"Total Variance"]~vars$years, type="o", col="black", lwd=2)

legend(1985,0.25, c("Total Variance", "Residual Variance","Res. Var. baseyear 2010"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("black","blue","red")) # gives the legend lines the correct color and widt

dev.off()



##################################################################
#PLOTS
################################################################

#load("/u/home/jd1033/DFG-descriptives/Results/FT/OccGaps_and_NoRW.85.M.RData")


#Plot 85/15, 85/50, 50/15 gaps for males only 

setEPS()
postscript("Graphs/OccGapsmales10.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gaps[,"eight50"]~gaps$years, xlim=c(1985,2011),ylim=c(0.00,1.10), xaxt = "n",  main="Quantile gaps males only, baseyear 2010", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="red")
axis(side=1, at=gaps$years, labels=gaps$years, cex.axis=0.9)
lines(gaps[,"eight15"]~gaps$years, type="o", col="blue", lwd=2)
lines(gaps[,"five15"]~gaps$years, type="o", col="green", lwd=2)

legend(1985,1.1, c("85/15 gap","85/50 gap", "50/15 gap"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green")) # gives the legend lines the correct color and widt

dev.off()

#Table
temptab <- xtable(gaps, caption="Quantiles Males baseyear 2010")
print(temptab, include.rownames=FALSE, file="Tables/QuantMales10.tex")

xltable <- gaps
WriteXLS("xltable", ExcelFileName="Tables/QuantMales10.xls", row.names=TRUE)


#Non-reweighted

setEPS()
postscript("Graphs/Gapsmales.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gapsnorw[,"eight50"]~gapsnorw$years, xlim=c(1985,2011),ylim=c(0.00,1.10), xaxt = "n",  main="Quantile gaps males only", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="red")
axis(side=1, at=gapsnorw$years, labels=gapsnorw$years, cex.axis=0.9)
lines(gapsnorw[,"eight15"]~gapsnorw$years, type="o", col="blue", lwd=2)
lines(gapsnorw[,"five15"]~gapsnorw$years, type="o", col="green", lwd=2)

legend(1985,1.1, c("85/15 gap","85/50 gap", "50/15 gap"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green")) # gives the legend lines the correct color and widt

dev.off()

#Table
temptab <- xtable(gapsnorw, caption="Quantiles Males")
print(temptab, include.rownames=FALSE, file="Tables/QuantMales.tex")

xltable <- gapsnorw
WriteXLS("xltable", ExcelFileName="Tables/QuantMales.xls", row.names=TRUE)



#Combined Gaps Males 2010

setEPS()
postscript("Graphs/OccCombinedMales10.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gaps[,"eight50"]~gaps$years, xlim=c(1985,2011),ylim=c(0.00,1.20), xaxt = "n",  main="w. baseyear 2010 occupation-change", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="hotpink2")
axis(side=1, at=gaps$years, labels=gaps$years, cex.axis=0.9)
lines(gaps[,"eight15"]~gaps$years, type="o", col="cornflowerblue", lwd=2)
lines(gaps[,"five15"]~gaps$years, type="o", col="greenyellow", lwd=2)

lines(gapsnorw[,"eight50"]~gapsnorw$years, type="o", col="red", lwd=2)
lines(gapsnorw[,"eight15"]~gapsnorw$years, type="o", col="blue", lwd=2)
lines(gapsnorw[,"five15"]~gapsnorw$years, type="o", col="green", lwd=2)


legend(1985,1.2, c("85/15 gap","85/50 gap", "50/15 gap","85/15 gap baseyear 2010","85/50 gap baseyear 2010", "50/15 gap baseyear 2010"), 
lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green","cornflowerblue","hotpink2","greenyellow" )) # gives the legend lines the correct color and widt

dev.off()

###################################
#Comparison Occ and non-occ
###################################

gaps.occ <- gaps
gapsnorw.occ<-gapsnorw

rm(gaps, gapsnorw)


load("/u/home/jd1033/DFG-descriptives/Results/FT/reweighting/trim.m.rw10.RData")

trim.m.rw$rweight <- trim.m.rw$sweight*trim.m.rw$base10	

gapsnorw <- data.frame()
gaps <- data.frame()

for (i in years) {

	tmp <- subset(trim.m.rw, trim.m.rw$year==i)	#subset of data for year i
	
	#calculate weighted quantiles, reweighted with counterfactual weights
	quants<- wtd.quantile(tmp$discount, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$rweight)
	
	#calculate weighted quantiles, reweighted with unmodified sample weights
	quants.norw<- wtd.quantile(tmp$discount, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$sweight)
	
	#quantile gaps
	eight50 <- (quants["85%"] - quants["50%"])
	eight15 <- (quants["85%"] - quants["15%"])
	five15 <- (quants["50%"] - quants["15%"])
	
	eight50.norw <- (quants.norw["85%"] - quants.norw["50%"])
	eight15.norw <- (quants.norw["85%"] - quants.norw["15%"])
	five15.norw <- (quants.norw["50%"] - quants.norw["15%"])
		
	
	gaps[2011-i,1] <- eight50
	gaps[2011-i,2] <- eight15
	gaps[2011-i,3] <- five15
	gaps[2011-i,4] <- quants["85%"]
	gaps[2011-i,5] <- quants["50%"]
	gaps[2011-i,6] <- quants["15%"]
	
	gapsnorw[2011-i,1] <- eight50.norw
	gapsnorw[2011-i,2] <- eight15.norw
	gapsnorw[2011-i,3] <- five15.norw
	gapsnorw[2011-i,4] <- quants.norw["85%"]
	gapsnorw[2011-i,5] <- quants.norw["50%"]
	gapsnorw[2011-i,6] <- quants.norw["15%"]
}

gaps <- cbind(years, gaps)
colnames(gaps) <- c("years","eight50", "eight15", "five15", "85% quantile", "Median", "15% quantile")
rownames(gaps)<-gaps$years

gapsnorw <- cbind(years, gapsnorw)
colnames(gapsnorw) <- c("years","eight50", "eight15", "five15", "85% quantile", "Median", "15% quantile")
rownames(gapsnorw)<-gapsnorw$years

#Graph with and without occupations

setEPS()
postscript("Graphs/OccNonOccMales85.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gaps[,"eight50"]~gaps$years, xlim=c(1985,2011),ylim=c(0.20,1.25), xaxt = "n",  main="Quantile gaps males, overlaid w. BY 85 occupation-change", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="hotpink2")
axis(side=1, at=gaps$years, labels=gaps$years, cex.axis=0.9)
lines(gaps[,"eight15"]~gaps$years, type="o", col="cornflowerblue", lwd=2)
lines(gaps[,"five15"]~gaps$years, type="o", col="greenyellow", lwd=2)

lines(gapsnorw[,"eight50"]~gapsnorw$years, type="o", col="red", lwd=2)
lines(gapsnorw[,"eight15"]~gapsnorw$years, type="o", col="blue", lwd=2)
lines(gapsnorw[,"five15"]~gapsnorw$years, type="o", col="green", lwd=2)

lines(gaps.occ[,"eight50"]~gapsnorw$years, type="o", col="coral", lwd=2)
lines(gaps.occ[,"eight15"]~gapsnorw$years, type="o", col="cyan2", lwd=2)
lines(gaps.occ[,"five15"]~gapsnorw$years, type="o", col="olivedrab3", lwd=2)


legend(1985,1.25, c("85/15 gap","85/50 gap", "50/15 gap","85/15 gap BY 85","85/50 gap BY 85", "50/15 gap BY 85","85/15 gap occs 85","85/50 gap occs 85", "50/15 gap occs 85"), 
lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green","cornflowerblue","hotpink2","greenyellow","cyan2","coral","olivedrab3" )) # gives the legend lines the correct color and widt


dev.off()

#Table
combtab <- rbind(gaps, gaps.occ)

temptab <- xtable(gaps.occ, caption="Occupation Quantiles Males baseyear 10")
print(temptab, include.rownames=FALSE, file="Tables/OccQuantMales10.tex")

xltable <- gaps.occ
WriteXLS("xltable", ExcelFileName="Tables/OccQuantMales10.xls", row.names=TRUE)

xltable <- combtab
WriteXLS("xltable", ExcelFileName="Tables/Occ&NonQuantMales10.xls", row.names=TRUE)




########################################################
#######Females#########
##############################################

rm(gaps, gapsnorw, vars)

load("reweighting/trim.f.rw10.occ.RData")

trim.f.rw.occ$occweight <- trim.f.rw.occ$sweight*trim.f.rw.occ$base10occ
#trim.f.rw.occ$rweight <- trim.f.rw.occ$sweight*trim.f.rw.occ$base10

gapsnorw <- data.frame()
gaps <- data.frame()
vars <- data.frame()

for (i in years) {

	tmp <- subset(trim.f.rw.occ, trim.f.rw.occ$year==i)
	quants<- wtd.quantile(tmp$values, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$occweight)
	resQuants <- wtd.quantile(tmp$residualsmf, probs=c(0.9,0.5,0.1), na.rm=FALSE, weights=tmp$occweight)
	resVar <- wtd.var(tmp$residualsmf, weights=tmp$occweight)
	
	quants.norw<- wtd.quantile(tmp$values, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$sweight)
	resQuants.norw <- wtd.quantile(tmp$residualsmf, probs=c(0.9,0.5,0.1), na.rm=FALSE, weights=tmp$sweight)
	resVar.norw <- wtd.var(tmp$residualsmf, weights=tmp$sweight)
	totalVar <- wtd.var(tmp$discount, weights=tmp$sweight)
	
	eight50 <- (quants["85%"] - quants["50%"])
	eight15 <- (quants["85%"] - quants["15%"])
	five15 <- (quants["50%"] - quants["15%"])
	
	eight50.norw <- (quants.norw["85%"] - quants.norw["50%"])
	eight15.norw <- (quants.norw["85%"] - quants.norw["15%"])
	five15.norw <- (quants.norw["50%"] - quants.norw["15%"])
		
	res9050 <- (resQuants["90%"] - resQuants["50%"])
	res9010 <- (resQuants["90%"] - resQuants["10%"])
	res5010 <- (resQuants["50%"] - resQuants["10%"])
	
	res9050.norw <- (resQuants.norw["90%"] - resQuants.norw["50%"])
	res9010.norw <- (resQuants.norw["90%"] - resQuants.norw["10%"])
	res5010.norw <- (resQuants.norw["50%"] - resQuants.norw["10%"])
	
	gaps[2011-i,1] <- eight50
	gaps[2011-i,2] <- eight15
	gaps[2011-i,3] <- five15
	gaps[2011-i,4] <- res9050
	gaps[2011-i,5] <- res9010
	gaps[2011-i,6] <- res5010
	gaps[2011-i,7] <- quants["85%"]
	gaps[2011-i,8] <- quants["50%"]
	gaps[2011-i,9] <- quants["15%"]
	gaps[2011-i,10] <-resQuants["90%"]
	gaps[2011-i,11] <-resQuants["50%"]
	gaps[2011-i,12] <-resQuants["10%"]
	
	gapsnorw[2011-i,1] <- eight50.norw
	gapsnorw[2011-i,2] <- eight15.norw
	gapsnorw[2011-i,3] <- five15.norw
	gapsnorw[2011-i,4] <- res9050.norw
	gapsnorw[2011-i,5] <- res9010.norw
	gapsnorw[2011-i,6] <- res5010.norw
	gapsnorw[2011-i,7] <- quants.norw["85%"]
	gapsnorw[2011-i,8] <- quants.norw["50%"]
	gapsnorw[2011-i,9] <- quants.norw["15%"]
	gapsnorw[2011-i,10] <- resQuants.norw["90%"]
	gapsnorw[2011-i,11] <- resQuants.norw["50%"]
	gapsnorw[2011-i,12] <- resQuants.norw["10%"]
	
	vars[2011-i,1] <- resVar
	vars[2011-i,2] <- resVar.norw
	vars[2011-i,3] <- totalVar
}

colnames(gaps) <- c("eight50", "eight15", "five15", "res9050",  "res9010", "res5010" ,"85% quantile", "Median", "15% quantile", "resQuant90", "resQuant50", "resQuant10")
gaps <- cbind(years, gaps)
colnames(gaps)[1]<-"years"
rownames(gaps)<-gaps$years

colnames(gapsnorw) <- c("eight50", "eight15", "five15", "res9050", "res9010", "res5010" ,"85% quantile", "Median", "15% quantile","resQuant90", "resQuant50", "resQuant10")
gapsnorw <- cbind(years, gapsnorw)
colnames(gapsnorw)[1]<-"years"
rownames(gapsnorw)<-gapsnorw$years


colnames(vars) <- c("Residual Variance Baseyear 2010", "Residual Variance", "Total Variance")
vars <- cbind (years, vars)
rownames(vars)<-vars$years

sink("OccQ-gapsRW10.F.txt", split=TRUE)
gaps
sink()

sink("OccQ-gaps.F.txt", split=TRUE)
gapsnorw
sink()

sink("OccQ-vars10.F.txt", split=TRUE)
vars
sink()

save(gapsnorw, gaps, vars, file="OccGaps_&_NoRW.10.F.RData")


##################################################################################
##################Explained share quantile gaps
###########################################

tab.exshare.10.f <- data.frame()

tab.exshare.10.f[1,1] <- (gapsnorw["2010","eight50"]-gapsnorw["1985","eight50"])
tab.exshare.10.f[2,1] <- (gapsnorw["2010","eight15"]-gapsnorw["1985","eight15"])
tab.exshare.10.f[3,1] <- (gapsnorw["2010","five15"]-gapsnorw["1985","five15"])

tab.exshare.10.f[1,2] <- (gaps["2010","eight50"]-gaps["1985","eight50"])
tab.exshare.10.f[2,2] <- (gaps["2010","eight15"]-gaps["1985","eight15"])
tab.exshare.10.f[3,2] <- (gaps["2010","five15"]-gaps["1985","five15"])

tab.exshare.10.f[1,3] <- 1-(tab.exshare.10.f[1,2]/(tab.exshare.10.f[1,1]))
tab.exshare.10.f[2,3] <- 1-(tab.exshare.10.f[2,2]/(tab.exshare.10.f[2,1]))
tab.exshare.10.f[3,3] <- 1-(tab.exshare.10.f[3,2]/(tab.exshare.10.f[3,1]))

colnames(tab.exshare.10.f) <- c("Quantile gap", "Quantile gap baseyear 2010", "Explained share")
rownames(tab.exshare.10.f) <- c("85/50 gap", "85/15 gap", "50/15 gap")

sink("OccExShareRW10.F.txt", split=TRUE)
tab.exshare.10.f
sink()


###################################################################################
###Explained share of inequality increase residual ################################################
###################################################################################

tab.exshare.res.10.f <- data.frame()

tab.exshare.res.10.f[1,1] <- (gapsnorw["2010","res9050"]-gapsnorw["1985","res9050"])
tab.exshare.res.10.f[2,1] <- (gapsnorw["2010","res9010"]-gapsnorw["1985","res9010"])
tab.exshare.res.10.f[3,1] <- (gapsnorw["2010","res5010"]-gapsnorw["1985","res5010"])
tab.exshare.res.10.f[4,1] <- (vars["2010","Residual Variance"]-vars["1985","Residual Variance"])

tab.exshare.res.10.f[1,2] <- (gaps["2010","res9050"]-gaps["1985","res9050"])
tab.exshare.res.10.f[2,2] <- (gaps["2010","res9010"]-gaps["1985","res9010"])
tab.exshare.res.10.f[3,2] <- (gaps["2010","res5010"]-gaps["1985","res5010"])
tab.exshare.res.10.f[4,2] <- (vars["2010","Residual Variance Baseyear 2010"]-vars["1985","Residual Variance Baseyear 2010"])

tab.exshare.res.10.f[1,3] <- 1-(tab.exshare.res.10.f[1,2]/(tab.exshare.res.10.f[1,1]))
tab.exshare.res.10.f[2,3] <- 1-(tab.exshare.res.10.f[2,2]/(tab.exshare.res.10.f[2,1]))
tab.exshare.res.10.f[3,3] <- 1-(tab.exshare.res.10.f[3,2]/(tab.exshare.res.10.f[3,1]))
tab.exshare.res.10.f[4,3] <- 1-(tab.exshare.res.10.f[4,2]/(tab.exshare.res.10.f[4,1]))

colnames(tab.exshare.res.10.f) <- c("Residuals", "Residuals baseyear 2010", "Explained share")
rownames(tab.exshare.res.10.f) <- c("90/50 gap", "90/10 gap", "50/10 gap", "Residual variance")

sink("OccResidExShareRW10.F.txt", split=TRUE)
tab.exshare.res.10.f
sink()


##################################################################
##########Variance Plots###################################
##################################################

setEPS()
postscript("Graphs/ResidualVar10.f.eps", width=8, heigh=7)


plot(vars[,"Residual Variance"]~vars$years, xlim=c(1985,2011),ylim=c(0.1,0.3), xaxt = "n",  main="Residual Variances for female workers with baseyear 2010", xlab="Time", ylab="Residual Variance", type="o", lwd=2, col="blue")
axis(side=1, at=vars$years, labels=vars$years, cex.axis=0.9)
lines(vars[,"Residual Variance Baseyear 2010"]~vars$years, type="o", col="red", lwd=2)
lines(vars[,"Total Variance"]~vars$years, type="o", col="black", lwd=2)

legend(1985,0.3, c("Total Variance", "Residual Variance","Res. Var. baseyear 2010"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("black","blue","red")) # gives the legend lines the correct color and widt

dev.off()


################################################
#########Gap Plots##################
###########################


#Plot 85/15, 85/50, 50/15 gaps for females only 

setEPS()
postscript("Graphs/OccGapsfemales10.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gaps[,"eight50"]~gaps$years, xlim=c(1985,2011),ylim=c(0.25,1.15), xaxt = "n",  main="Quantile gaps females only, baseyear 2010", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="red")
axis(side=1, at=gaps$years, labels=gaps$years, cex.axis=0.9)
lines(gaps[,"eight15"]~gaps$years, type="o", col="blue", lwd=2)
lines(gaps[,"five15"]~gaps$years, type="o", col="green", lwd=2)

legend(1985,1.15, c("85/15 gap","85/50 gap", "50/15 gap"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green")) # gives the legend lines the correct color and widt

dev.off()

#Table
temptab <- xtable(gaps, caption="Quantiles Females baseyear 2010")
print(temptab, include.rownames=FALSE, file="Tables/OccQuantFemales10.tex")

xltable <- gaps
WriteXLS("xltable", ExcelFileName="Tables/OccQuantFemales10.xls", row.names=TRUE)


#Non-reweighted

setEPS()
postscript("Graphs/Gapsfemales.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gapsnorw[,"eight50"]~gapsnorw$years, xlim=c(1985,2011),ylim=c(0.2,1.10), xaxt = "n",  main="Quantile gaps females only", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="red")
axis(side=1, at=gapsnorw$years, labels=gapsnorw$years, cex.axis=0.9)
lines(gapsnorw[,"eight15"]~gapsnorw$years, type="o", col="blue", lwd=2)
lines(gapsnorw[,"five15"]~gapsnorw$years, type="o", col="green", lwd=2)

legend(1985,1.1, c("85/15 gap","85/50 gap", "50/15 gap"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green")) # gives the legend lines the correct color and widt

dev.off()

#Table
temptab <- xtable(gapsnorw, caption="Quantiles Females baseyear")
print(temptab, include.rownames=FALSE, file="Tables/OccQuantFemales.tex")

xltable <- gapsnorw
WriteXLS("xltable", ExcelFileName="Tables/OccQuantFemales.xls", row.names=TRUE)



#Combined Gaps Females 2010

setEPS()
postscript("Graphs/OccCombinedFemales10.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gaps[,"eight50"]~gaps$years, xlim=c(1985,2011),ylim=c(0.2,1.30), xaxt = "n",  main="Quantile gaps females, overlaid w. baseyear 2010 occupation-change", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="hotpink2")
axis(side=1, at=gaps$years, labels=gaps$years, cex.axis=0.9)
lines(gaps[,"eight15"]~gaps$years, type="o", col="cornflowerblue", lwd=2)
lines(gaps[,"five15"]~gaps$years, type="o", col="greenyellow", lwd=2)

lines(gapsnorw[,"eight50"]~gapsnorw$years, type="o", col="red", lwd=2)
lines(gapsnorw[,"eight15"]~gapsnorw$years, type="o", col="blue", lwd=2)
lines(gapsnorw[,"five15"]~gapsnorw$years, type="o", col="green", lwd=2)

legend(1985,1.3, c("85/15 gap","85/50 gap", "50/15 gap","85/15 gap baseyear 2010","85/50 gap baseyear 2010", "50/15 gap baseyear 2010"), 
lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green","cornflowerblue","hotpink2","greenyellow" )) # gives the legend lines the correct color and widt

dev.off()


###################################
#Comparison females
###################################

gaps.occ <- gaps
gapsnorw.occ<-gapsnorw

rm(gaps, gapsnorw)


load("/u/home/jd1033/DFG-descriptives/Results/FT/reweighting/trim.f.rw85.RData")

trim.f.rw$rweight <- trim.f.rw$sweight*trim.f.rw$base85	

gapsnorw <- data.frame()
gaps <- data.frame()

for (i in years) {

	tmp <- subset(trim.f.rw, trim.f.rw$year==i)	#subset of data for year i
	
	#calculate weighted quantiles, reweighted with counterfactual weights
	quants<- wtd.quantile(tmp$discount, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$rweight)
	
	#calculate weighted quantiles, reweighted with unmodified sample weights
	quants.norw<- wtd.quantile(tmp$discount, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$sweight)
	
	#quantile gaps
	eight50 <- (quants["85%"] - quants["50%"])
	eight15 <- (quants["85%"] - quants["15%"])
	five15 <- (quants["50%"] - quants["15%"])
	
	eight50.norw <- (quants.norw["85%"] - quants.norw["50%"])
	eight15.norw <- (quants.norw["85%"] - quants.norw["15%"])
	five15.norw <- (quants.norw["50%"] - quants.norw["15%"])
		
	
	gaps[2011-i,1] <- eight50
	gaps[2011-i,2] <- eight15
	gaps[2011-i,3] <- five15
	gaps[2011-i,4] <- quants["85%"]
	gaps[2011-i,5] <- quants["50%"]
	gaps[2011-i,6] <- quants["15%"]
	
	gapsnorw[2011-i,1] <- eight50.norw
	gapsnorw[2011-i,2] <- eight15.norw
	gapsnorw[2011-i,3] <- five15.norw
	gapsnorw[2011-i,4] <- quants.norw["85%"]
	gapsnorw[2011-i,5] <- quants.norw["50%"]
	gapsnorw[2011-i,6] <- quants.norw["15%"]
}

gaps <- cbind(years, gaps)
colnames(gaps) <- c("years","eight50", "eight15", "five15", "85% quantile", "Median", "15% quantile")
rownames(gaps)<-gaps$years

gapsnorw <- cbind(years, gapsnorw)
colnames(gapsnorw) <- c("years","eight50", "eight15", "five15", "85% quantile", "Median", "15% quantile")
rownames(gapsnorw)<-gapsnorw$years

#Graph with and without occupations

setEPS()
postscript("Graphs/OccNonOccFemales85.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gaps[,"eight50"]~gaps$years, xlim=c(1985,2011),ylim=c(0.20,1.3), xaxt = "n",  main="Quantile gaps females, overlaid w. BY 85 occupation-change", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="hotpink2")
axis(side=1, at=gaps$years, labels=gaps$years, cex.axis=0.9)
lines(gaps[,"eight15"]~gaps$years, type="o", col="cornflowerblue", lwd=2)
lines(gaps[,"five15"]~gaps$years, type="o", col="greenyellow", lwd=2)

lines(gapsnorw[,"eight50"]~gapsnorw$years, type="o", col="red", lwd=2)
lines(gapsnorw[,"eight15"]~gapsnorw$years, type="o", col="blue", lwd=2)
lines(gapsnorw[,"five15"]~gapsnorw$years, type="o", col="green", lwd=2)

lines(gaps.occ[,"eight50"]~gapsnorw$years, type="o", col="coral", lwd=2)
lines(gaps.occ[,"eight15"]~gapsnorw$years, type="o", col="cyan2", lwd=2)
lines(gaps.occ[,"five15"]~gapsnorw$years, type="o", col="olivedrab3", lwd=2)


legend(1985,1.3, c("85/15 gap","85/50 gap", "50/15 gap","85/15 gap BY 85","85/50 gap BY 85", "50/15 gap BY 85","85/15 gap occs 85","85/50 gap occs 85", "50/15 gap occs 85"), 
lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green","cornflowerblue","hotpink2","greenyellow","cyan2","coral","olivedrab3" )) # gives the legend lines the correct color and widt


dev.off()