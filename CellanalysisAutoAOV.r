#Calculate Variance & gaps for age/education cells
#West germany fulltime workers only

options(scipen=30)

library(foreign)
library(Hmisc)
library(parallel)
library(car)
library(xtable)

setwd("E:/DATA/Results")
#setwd("C:/Dokumente/Uni/Wage Inequality/Results")

load("E:/DATA/Residuals.RData")
#load("I:/DATA/Residuals.RData")

#Create  bildcat variable with only 4 values (analogous to Dustman)
#Bildcat labels: Kein = 0, Schulabschluss = 1, Ausbildung = 2, Hochschule = 3

trim$bildcat <- recode(trim$IP2A, "c(-9, 1)= 0; 2=1; c(3,4)=2; c(5,6)= 3")
trim$agecat <- recode(trim$age, "17:30=18; 31:40=31; 41:50=41; 51:62=51")
trim$cell <- paste(trim$agecat, trim$IP2A, sep="") 
trim$cell <- as.factor(trim$cell)

test <- aov(trim$discount~trim$cell+Error(trim$cell), data=trim)
summary(test)

#Make two tables, male and female each
#Define cells with 
#Bildung 6x
#Age groups 17-30 = 1, 31-40 = 2, 41-50 =3 , 51-62 =4 4x
#Years 1985,1990,2000,2004,2010

#Create subsamples of male and female workers
males <- subset(trim, frau==0)
females <- subset(trim, frau==1)



for(i in years) {
	tmp <- subset(males, males$year==i)
	attach(tmp)
	test <- aov(discount~cell+Error(cell), data=tmp)
	print(i)
	print(summary(test))
	detach(tmp)
#by(males$discount, males$cell, mean)
	
}
rm(trim)

#Choose analysis years to expand on Dustman(2009):
# 1985, 1990, 2000, 2004, 2010

years <- c(1985,1990,2000,2004,2010)
bilds <- c(1,2,3,4,5,6)
ages <- cbind (c(18,31,41,51),c(30,40,50,62))

cellsm <- matrix(nrow=((length(bilds))*nrow(ages)), ncol=length(years)+1)
cellsf <- matrix(nrow=((length(bilds)+1)*nrow(ages)), ncol=length(years))


rownames(cellsm) <- c(rep("College", 4), rep("Technical", 4), rep("Highschool&Vocational", 4), rep("High School", 4), rep("Vocational", 4), rep("No degree",4) )
#rownames(cellsm) <- c("Kein1", "Ausb1", "Abi1", "Hoch1","Kein2", "Ausb2", "Abi2", "Hoch2","Kein3", "Ausbl3", "Abib3", "Hoch3","Kein4", "Ausb4", "Abi4", "Hoch4")
colnames(cellsm) <-c("Age","1985","1990","2000","2004", "2010")

# rownames(cellsf) <- c("No degree", "18-30", "32-40", "42-50", "51-62", "Vocational", "18-30", "32-40", "42-50", "51-62", "High School", "18-30", "32-40", "42-50", "51-62", "College", "18-30", "32-40", "42-50", "51-62")
# colnames(cellsf) <-c("1985","1990","2000","2006", "2010")

#create empty cells matrices
#cellsm[,1] <-c(rep(18,4),rep(31,4), rep(41,4),rep(50,4))

cellsf <- cellsm

#create input matrix with all cell criteria
input <- expand.grid(years, bilds, ages[,1])
tmp <- expand.grid(years, bilds, ages[,2])
input <- cbind(input, tmp[3])
input$Var2 <- as.factor(input$Var2)


#For each age/education cell and year, the mean, variance and quantile gaps are calculated
# for values and residuals

calculatem <- function (i) {
	sample1 <- subset(males, year==i[[1]] & bild==i[[2]] & age>=i[[3]] & age <=i[[4]])
	groupmean <- wtd.mean(sample1$discount, weights=sample1$sweight, na.rm=FALSE)
	variance <- wtd.var(sample1$discount, weights=sample1$sweight, na.rm=FALSE)
	SD <- sqrt(wtd.var(sample1$discount, weights=sample1$sweight, na.rm=FALSE))
	resvar <- wtd.var(sample1$residualsmf, weights=sample1$sweight, na.rm=FALSE)
	resSD <- sqrt(wtd.var(sample1$residualsmf, weights=sample1$sweight, na.rm=FALSE))
	quants <- wtd.quantile(sample1$discount, probs=c(0.85,0.5,0.15),weights=sample1$sweight, na.rm=FALSE)
	resQuants <- wtd.quantile(sample1$residualsmf, probs=c(0.85,0.5,0.15), weights=sample1$sweight, na.rm=FALSE)
	nine50 <- (quants["85%"] - quants["50%"])
	nine10 <- (quants["85%"] - quants["15%"])
	five10 <- (quants["50%"] - quants["15%"])
	
	resnine50 <- (resQuants["85%"] - resQuants["50%"])
	resnine10 <- (resQuants["85%"] - resQuants["15%"])
	resfive10 <- (resQuants["50%"] - resQuants["15%"])
	obs <- nrow(sample1)
	share <- obs/nrow(males[males$year==i[[1]],])
	rm(sample1)
	return(c(obs, groupmean, share, variance, SD, nine50, nine10, five10, resvar, resSD, resnine50, resnine10, resfive10))
	}
	
resultsm <- t(apply(input, 1, calculatem))

#Attach results collumns to input collumns, new results dataframe with all relevant data
resultsm <- cbind(input, resultsm)
	
colnames(resultsm) <- c("years", "bild", "agemin", "agemax", "obs", "mean", "share", "variance","SD", "eight50", "eight15", "five15", "Res.variance", "Res.SD", "Res.nine50","Res.nine10", "Res.five10")

#Order the results as year-blocks in table res.form.m

for (i in years) {
	tmp <- subset(resultsm, years==i)
	nam <- paste("tmp", i, sep = "")
	assign(nam, tmp)
	}

res.form.m <- rbind(tmp1985, tmp1990, tmp2000, tmp2004, tmp2010)

rm(tmp1985,tmp1990, tmp2000, tmp2004, tmp2010)
	

	
#calculation females, same as above, only for dataset "females"

calculatef <- function (i) {
	sample1 <- subset(females, year==i[[1]] & bild==i[[2]] & age>=i[[3]] & age <=i[[4]])
	groupmean <- wtd.mean(sample1$discount, weights=sample1$sweight, na.rm=FALSE)
	variance <- wtd.var(sample1$discount,weights=sample1$sweight, na.rm=FALSE)
	SD <- sqrt(wtd.var(sample1$discount,weights=sample1$sweight, na.rm=FALSE))
	resvar <- wtd.var(sample1$residualsmf,weights=sample1$sweight, na.rm=FALSE)
	resSD <- sqrt(wtd.var(sample1$residualsmf,weights=sample1$sweight, na.rm=FALSE))
	quants <- wtd.quantile(sample1$discount, probs=c(0.85,0.5,0.15),weights=sample1$sweight, na.rm=FALSE)
	resQuants <- wtd.quantile(sample1$residualsmf, probs=c(0.85,0.5,0.15),weights=sample1$sweight, na.rm=FALSE)
	nine50 <- (quants["85%"] - quants["50%"])
	nine10 <- (quants["85%"] - quants["15%"])
	five10 <- (quants["50%"] - quants["15%"])
	
	resnine50 <- (resQuants["85%"] - resQuants["50%"])
	resnine10 <- (resQuants["85%"] - resQuants["15%"])
	resfive10 <- (resQuants["50%"] - resQuants["15%"])
	obs <- nrow(sample1)
	share <- obs/nrow(males[males$year==i[[1]],])
	rm(sample1)
	return(c(obs, groupmean, share, variance, SD, nine50, nine10, five10, resvar, resSD, resnine50, resnine10, resfive10))
	}
	
resultsf <- t(apply(input, 1, calculatef))
	
resultsf <- cbind(input, resultsf)

colnames(resultsf) <- c("years", "bild", "agemin", "agemax", "obs", "mean", "share", "variance","SD", "eight50", "eight15", "five15", "Res.variance", "Res.SD", "Res.nine50","Res.nine10", "Res.five10")

for (i in years) {
	tmp <- subset(resultsf, years==i)
	nam <- paste("tmp", i, sep = "")
	assign(nam, tmp)
	}

res.form.f <- rbind(tmp1985,tmp1990, tmp2000, tmp2004, tmp2010)

#Create separate Sub-Tables for each educational outcome

#MALES
edu <- c(6,5,4,3,2,1)
edunames <- c( "No","Vocational","High School","High School & Vocational", "Technical College", "University" )

subtable <- function(x) {
	
	temp <- matrix(ncol=length(years)+2, nrow=(3*nrow(ages)))

	colnames(temp)[3:(length(years)+2)] <- years

	colnames(temp)[1:2] <- c("Ages", "Statistics")

	temp[,1]<-c(rep(paste(ages[1,1],"-",ages[1,2]),3), rep(paste(ages[2,1],"-",ages[2,2]),3), rep(paste(ages[3,1],"-",ages[3,2]),3), rep(paste(ages[4,1],"-",ages[4,2]),3))

	#rownames(temp) <- c(rep(paste(ages[1,1],"-",ages[1,2]),3), rep(paste(ages[2,1],"-",ages[2,2]),3), rep(paste(ages[3,1],"-",ages[3,2]),3), rep(paste(ages[4,1],"-",ages[4,2]),3))

	temp[,2] <- rep(c("Variance","85/15-gap", "Share"), 4)

	for (i in years) {
		temp[1,paste(i)] <- round(resultsm$Res.variance[resultsm$years==i & resultsm$agemin==ages[1,1] & resultsm$bild==x],digits=3)
		temp[2,paste(i)] <- round(resultsm$eight15[resultsm$years==i & resultsm$agemin==ages[1,1] & resultsm$bild==x],digits=3)
		temp[3,paste(i)] <- round(resultsm$share[resultsm$years==i & resultsm$agemin==ages[1,1] & resultsm$bild==x] ,digits=3)
		temp[4,paste(i)] <- round(resultsm$Res.variance[resultsm$years==i & resultsm$agemin==ages[2,1] & resultsm$bild==x],digits=3)
		temp[5,paste(i)] <- round(resultsm$eight15[resultsm$years==i & resultsm$agemin==ages[2,1] & resultsm$bild==x],digits=3)
		temp[6,paste(i)] <- round(resultsm$share[resultsm$years==i & resultsm$agemin==ages[2,1] & resultsm$bild==x],digits=3)
		temp[7,paste(i)] <- round(resultsm$Res.variance[resultsm$years==i & resultsm$agemin==ages[3,1] & resultsm$bild==x],digits=3)
		temp[8,paste(i)] <- round(resultsm$eight15[resultsm$years==i & resultsm$agemin==ages[3,1] & resultsm$bild==x],digits=3)
		temp[9,paste(i)] <- round(resultsm$share[resultsm$years==i & resultsm$agemin==ages[3,1] & resultsm$bild==x],digits=3)
		temp[10,paste(i)] <- round(resultsm$Res.variance[resultsm$years==i & resultsm$agemin==ages[4,1] & resultsm$bild==x],digits=3)
		temp[11,paste(i)] <- round(resultsm$eight15[resultsm$years==i & resultsm$agemin==ages[4,1] & resultsm$bild==x],digits=3)
		temp[12,paste(i)] <- round(resultsm$share[resultsm$years==i & resultsm$agemin==ages[4,1] & resultsm$bild==x],digits=3)
		}

		nam <- paste("males.edu.",x,sep="")
	assign(nam, temp )
	
	temptab <- xtable(temp, caption=paste(edunames[x],"degree"))
	print(temptab, include.rownames=FALSE, file=paste("Tables/",edunames[x],"M.tex", sep=""))
	
	return(temp)
}

subtable.results.m <- lapply(edu, function(x) subtable(x))

names(subtable.results.m) <- edu