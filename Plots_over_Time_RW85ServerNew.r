options(scipen=30)

library(foreign)
library(Hmisc)
library(WriteXLS)
library(xtable)


setwd("/u/home/jd1033/DFG-descriptives/Results/FT")
#setwd("T:/")


load("reweighting/Resid85.RData")

rm(bindresf, bindresm)

#trim.m.rw <- trim.m.rw[!names(trim.m.rw) %in% c("values.base85", "sweight.1 ","countmf", "rweight.1") ]

setwd("/u/home/jd1033/DFG-descriptives/Results/FT")


###############################################
#########Variance Tables#####################
##############################################

#males
x.table <- xtable(variances[1:26,], caption ="Variance and Residuals for male workers")
print(x.table, file="Tables/VarMalesFT85.tex")
rm(x.table)

xltable <- variances[1:26,]
WriteXLS("xltable", ExcelFileName="Tables/VarMalesFT85.xls", row.names=TRUE)

#females
x.table <- xtable(variances[27:52,], caption ="Variance and Residuals for female workers")
print(x.table, file="Tables/VarFemFT85.tex")
rm(x.table)

xltable <- variances[27:52,]
WriteXLS("xltable", ExcelFileName="Tables/VarFemFT85.xls", row.names=TRUE)


################################################
######### Variance Plots #######################
################################################


#Plot variance for males only 

setEPS()
postscript("Graphs/MalesVar&Res85.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(variances[1:26,1]~years, xlim=c(1985,2011),ylim=c(0.00,0.25), xaxt = "n",  main="Residual variance males, baseyear 1985", xlab="Time", ylab="Variance / Residual variance", type="o", lwd=2, col="red")
axis(side=1, at=years, labels=years, cex.axis=0.9)
lines(variances[1:26,3]~years, type="o", col="blue", lwd=2)
legend(1985,0.25, c("Total Variance", "Residual Variance"), lty=c(1,1), lwd=c(2.5,2.5),col=c("blue","red")) # gives the legend lines the correct color and widt
dev.off()


#Plot variance for females only
setEPS()
postscript("Graphs/FemVar&Res85.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(variances[27:52,1]~years, xlim=c(1985,2011),ylim=c(0.0,0.3), xaxt = "n",  main="Residual variance females, baseyear 85", xlab="Time", ylab="Variance / Residual variance", type="o", lwd=2, col="red")
axis(side=1, at=years, labels=years, cex.axis=0.9)
lines(variances[27:52,3]~years, type="o", col="blue", lwd=2)
legend(1985,0.3, c("Total Variance", "Residual Variance"), lty=c(1,1), lwd=c(2.5,2.5),col=c("blue","red")) # gives the legend lines the correct color and widt

dev.off()


################################################
######### Gap-Plots ##########################
################################################

#Calculate Gaps for M/F separate

#MALES

gapsnorw <- data.frame()
gaps <- data.frame()

for (i in years) {

	tmp <- subset(trim.m.rw, trim.m.rw$year==i)
	quants<- wtd.quantile(tmp$values, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$rweight)
	resQuants <- wtd.quantile(tmp$res.base85, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$rweight)

	quants.norw<- wtd.quantile(tmp$values, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$sweight)
	resQuants.norw <- wtd.quantile(tmp$res.base85, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$sweight)
	
	eight50 <- (quants["85%"] - quants["50%"])
	eight15 <- (quants["85%"] - quants["15%"])
	five15 <- (quants["50%"] - quants["15%"])
	
	eight50.norw <- (quants.norw["85%"] - quants.norw["50%"])
	eight15.norw <- (quants.norw["85%"] - quants.norw["15%"])
	five15.norw <- (quants.norw["50%"] - quants.norw["15%"])
		
	reseight50 <- (resQuants["85%"] - resQuants["50%"])
	reseight15 <- (resQuants["85%"] - resQuants["15%"])
	resfive15 <- (resQuants["50%"] - resQuants["15%"])
	
	reseight50.norw <- (resQuants.norw["85%"] - resQuants.norw["50%"])
	reseight15.norw <- (resQuants.norw["85%"] - resQuants.norw["15%"])
	resfive15.norw <- (resQuants.norw["50%"] - resQuants.norw["15%"])
	
	gaps[2011-i,1] <- eight50
	gaps[2011-i,2] <- eight15
	gaps[2011-i,3] <- five15
	gaps[2011-i,4] <- reseight50
	gaps[2011-i,5] <- reseight15
	gaps[2011-i,6] <- resfive15
	gaps[2011-i,7] <- quants["85%"]
	gaps[2011-i,8] <- quants["50%"]
	gaps[2011-i,9] <- quants["15%"]
	
	gapsnorw[2011-i,1] <- eight50.norw
	gapsnorw[2011-i,2] <- eight15.norw
	gapsnorw[2011-i,3] <- five15.norw
	gapsnorw[2011-i,4] <- reseight50.norw
	gapsnorw[2011-i,5] <- reseight15.norw
	gapsnorw[2011-i,6] <- resfive15.norw
	gapsnorw[2011-i,7] <- quants.norw["85%"]
	gapsnorw[2011-i,8] <- quants.norw["50%"]
	gapsnorw[2011-i,9] <- quants.norw["15%"]
}
colnames(gaps) <- c("eight50", "eight15", "five15", "reseight50", "reseight15", "resfive15" ,"85% quantile", "Median", "15% quantile")
gaps <- cbind(years, gaps)
colnames(gaps)[1]<-"years"
rownames(gaps)<-gaps$years

colnames(gapsnorw) <- c("eight50", "eight15", "five15", "reseight50", "reseight15", "resfive15" ,"85% quantile", "Median", "15% quantile")
gapsnorw <- cbind(years, gapsnorw)
colnames(gapsnorw)[1]<-"years"
rownames(gapsnorw)<-gapsnorw$years

sink("Q-gapsRW85.txt", split=TRUE)
gaps
sink()

sink("Q-gaps.txt", split=TRUE)
gapsnorw
sink()

save(gapsnorw, gaps, file="Gaps_and_NoRW.85.M.RData")


#FEMALES

gapsnorw.f <- data.frame()
gaps.f <- data.frame()

for (i in years) {

	tmp <- subset(trim.f.rw, trim.f.rw$year==i)
	quants<- wtd.quantile(tmp$values, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$rweight)
	resQuants <- wtd.quantile(tmp$res.base85, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$rweight)

	quants.norw<- wtd.quantile(tmp$values, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$sweight)
	resQuants.norw <- wtd.quantile(tmp$res.base85, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$sweight)
	
	eight50 <- (quants["85%"] - quants["50%"])
	eight15 <- (quants["85%"] - quants["15%"])
	five15 <- (quants["50%"] - quants["15%"])
	
	eight50.norw <- (quants.norw["85%"] - quants.norw["50%"])
	eight15.norw <- (quants.norw["85%"] - quants.norw["15%"])
	five15.norw <- (quants.norw["50%"] - quants.norw["15%"])
		
	reseight50 <- (resQuants["85%"] - resQuants["50%"])
	reseight15 <- (resQuants["85%"] - resQuants["15%"])
	resfive15 <- (resQuants["50%"] - resQuants["15%"])
	
	reseight50.norw <- (resQuants.norw["85%"] - resQuants.norw["50%"])
	reseight15.norw <- (resQuants.norw["85%"] - resQuants.norw["15%"])
	resfive15.norw <- (resQuants.norw["50%"] - resQuants.norw["15%"])
	
	gaps.f[2011-i,1] <- eight50
	gaps.f[2011-i,2] <- eight15
	gaps.f[2011-i,3] <- five15
	gaps.f[2011-i,4] <- reseight50
	gaps.f[2011-i,5] <- reseight15
	gaps.f[2011-i,6] <- resfive15
	gaps.f[2011-i,7] <- quants["85%"]
	gaps.f[2011-i,8] <- quants["50%"]
	gaps.f[2011-i,9] <- quants["15%"]
	
	gapsnorw.f[2011-i,1] <- eight50.norw
	gapsnorw.f[2011-i,2] <- eight15.norw
	gapsnorw.f[2011-i,3] <- five15.norw
	gapsnorw.f[2011-i,4] <- reseight50.norw
	gapsnorw.f[2011-i,5] <- reseight15.norw
	gapsnorw.f[2011-i,6] <- resfive15.norw
	gapsnorw.f[2011-i,7] <- quants.norw["85%"]
	gapsnorw.f[2011-i,8] <- quants.norw["50%"]
	gapsnorw.f[2011-i,9] <- quants.norw["15%"]
}

colnames(gaps.f) <- c("eight50", "eight15", "five15", "reseight50", "reseight15", "resfive15" ,"85% quantile", "Median", "15% quantile")
gaps.f <- cbind(years, gaps.f)
colnames(gaps.f)[1]<-"years"
rownames(gaps.f)<-gaps.f$years

colnames(gapsnorw.f) <- c("eight50", "eight15", "five15", "reseight50", "reseight15", "resfive15" ,"85% quantile", "Median", "15% quantile")
gapsnorw.f <- cbind(years, gapsnorw.f)
colnames(gapsnorw.f)[1]<-"years"
rownames(gapsnorw.f)<-gapsnorw.f$years


sink("Q-gapsRW85.F.txt", split=TRUE)
gaps.f
sink()

sink("Q-gaps.F.txt", split=TRUE)
gapsnorw.f
sink()

save(gapsnorw.f, gaps.f, file="Gaps_and_NoRW.85.F.RData")


##################################################################
#PLOTS
################################################################

load("/u/home/jd1033/DFG-descriptives/Results/FT/Gaps_and_NoRW.85.M.RData")
load("/u/home/jd1033/DFG-descriptives/Results/FT/Gaps_and_NoRW.85.F.RData")

#Plot 85/15, 85/50, 50/15 gaps for males only 

setEPS()
postscript("Graphs/Gapsmales85.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gaps[,"eight50"]~gaps$years, xlim=c(1985,2011),ylim=c(0.00,1.10), xaxt = "n",  main="Quantile gaps males only, baseyear 85", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="red")
axis(side=1, at=gaps$years, labels=gaps$years, cex.axis=0.9)
lines(gaps[,"eight15"]~gaps$years, type="o", col="blue", lwd=2)
lines(gaps[,"five15"]~gaps$years, type="o", col="green", lwd=2)

legend(1985,1.1, c("85/15 gap","85/50 gap", "50/15 gap"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green")) # gives the legend lines the correct color and widt

dev.off()

#Table
temptab <- xtable(gaps, caption="Quantiles Males baseyear 85")
print(temptab, include.rownames=FALSE, file="Tables/QuantMales85.tex")

xltable <- gaps
WriteXLS("xltable", ExcelFileName="Tables/QuantMales85.xls", row.names=TRUE)


#Non-reweighted

setEPS()
postscript("Graphs/Gapsmales.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gapsnorw[,"eight50"]~gapsnorw$years, xlim=c(1985,2011),ylim=c(0.00,1.10), xaxt = "n",  main="Quantile gaps males only", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="red")
axis(side=1, at=gapsnorw$years, labels=gapsnorw$years, cex.axis=0.9)
lines(gapsnorw[,"eight15"]~gapsnorw$years, type="o", col="blue", lwd=2)
lines(gapsnorw[,"five15"]~gapsnorw$years, type="o", col="green", lwd=2)

legend(1985,1.1, c("85/15 gap","85/50 gap", "50/15 gap"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green")) # gives the legend lines the correct color and widt

dev.off()

#Table
temptab <- xtable(gapsnorw, caption="Quantiles Males baseyear 85")
print(temptab, include.rownames=FALSE, file="Tables/QuantMales.tex")

xltable <- gapsnorw
WriteXLS("xltable", ExcelFileName="Tables/QuantMales.xls", row.names=TRUE)


#Residual Gaps
setEPS()
postscript("Graphs/ResGapsmales85.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gaps[,"reseight50"]~gaps$years, xlim=c(1985,2011),ylim=c(0.00,1.10), xaxt = "n",  main="Residual quantile gaps males only, baseyear 85", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="red")
axis(side=1, at=years, labels=gaps$years, cex.axis=0.9)
lines(gaps[,"reseight15"]~gaps$years, type="o", col="blue", lwd=2)
lines(gaps[,"resfive15"]~gaps$years, type="o", col="green", lwd=2)

legend(1985,1.1, c("85/15 gap","85/50 gap", "50/15 gap"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green")) # gives the legend lines the correct color and widt

dev.off()


#Combined Gaps Males 85

setEPS()
postscript("Graphs/CombinedMales85.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gaps[,"eight50"]~gaps$years, xlim=c(1985,2011),ylim=c(0.00,1.10), xaxt = "n",  main="Quantile gaps males, overlaid with baseyear 85", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="hotpink2")
axis(side=1, at=gaps$years, labels=gaps$years, cex.axis=0.9)
lines(gaps[,"eight15"]~gaps$years, type="o", col="cornflowerblue", lwd=2)
lines(gaps[,"five15"]~gaps$years, type="o", col="greenyellow", lwd=2)

lines(gapsnorw[,"eight50"]~gapsnorw$years, type="o", col="red", lwd=2)
lines(gapsnorw[,"eight15"]~gapsnorw$years, type="o", col="blue", lwd=2)
lines(gapsnorw[,"five15"]~gapsnorw$years, type="o", col="green", lwd=2)


legend(1985,1.1, c("85/15 gap","85/50 gap", "50/15 gap","85/15 gap baseyear 85","85/50 gap baseyear 85", "50/15 gap baseyear 85"), 
lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green","cornflowerblue","hotpink2","greenyellow" )) # gives the legend lines the correct color and widt

dev.off()




#FEMALES

#Plot 85/15, 85/50, 50/15 gaps for females only 

setEPS()
postscript("Graphs/Gapsfemales85.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gaps.f[,"eight50"]~gaps.f$years, xlim=c(1985,2011),ylim=c(0.1,1.0), xaxt = "n",  main="Quantile gaps females only, baseyear 1985", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="red")
axis(side=1, at=years, labels=years, cex.axis=0.9)
lines(gaps.f[,"eight15"]~gaps.f$years, type="o", col="blue", lwd=2)
lines(gaps.f[,"five15"]~gaps.f$years, type="o", col="green", lwd=2)

legend(1986,1.0, c("85/15 gap","85/50 gap", "50/15 gap"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green")) # gives the legend lines the correct color and widt
dev.off()

temptab <- xtable(gaps.f, caption="Quantiles Females baseyear 85")
print(temptab, include.rownames=FALSE, file="Tables/QuantFemales85.tex")

xltable <- gaps.f
WriteXLS("xltable", ExcelFileName="Tables/QuantFemales85.xls", row.names=TRUE)

#Residual Gaps

setEPS()
postscript("Graphs/Res.Gapsfemales85.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gaps.f[,"reseight50"]~gaps.f$years, xlim=c(1985,2011),ylim=c(0.1,1.0), xaxt = "n",  main="Residual quantile gaps females only, baseyear 1985", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="red")
axis(side=1, at=years, labels=years, cex.axis=0.9)
lines(gaps.f[,"reseight15"]~gaps.f$years, type="o", col="blue", lwd=2)
lines(gaps.f[,"resfive15"]~gaps.f$years, type="o", col="green", lwd=2)

legend(1985,1.0, c("85/15 gap","85/50 gap", "50/15 gap"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green")) # gives the legend lines the correct color and widt

dev.off()

#Non-reweighted

setEPS()
postscript("Graphs/Gapsfemales.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gapsnorw.f[,"eight50"]~gapsnorw.f$years, xlim=c(1985,2011),ylim=c(0.00,1.10), xaxt = "n",  main="Quantile gaps females only", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="red")
axis(side=1, at=gapsnorw.f$years, labels=gapsnorw.f$years, cex.axis=0.9)
lines(gapsnorw.f[,"eight15"]~gapsnorw.f$years, type="o", col="blue", lwd=2)
lines(gapsnorw.f[,"five15"]~gapsnorw.f$years, type="o", col="green", lwd=2)

legend(1985,1.1, c("85/15 gap","85/50 gap", "50/15 gap"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green")) # gives the legend lines the correct color and widt

dev.off()

#Table
temptab <- xtable(gapsnorw.f[,], caption="Quantiles Females baseyear 85")
print(temptab, include.rownames=FALSE, file="Tables/QuantFemales.tex")

xltable <- gapsnorw.f
WriteXLS("xltable", ExcelFileName="Tables/QuantFemales.xls", row.names=TRUE)


#Combined Gaps Females 85

setEPS()
postscript("Graphs/CombinedFemales85.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gaps.f[,"eight50"]~gaps.f$years, xlim=c(1985,2011),ylim=c(0.2,1.2), xaxt = "n",  main="Quantile gaps females, overlaid with baseyear 85", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="hotpink2")
axis(side=1, at=gaps.f$years, labels=gaps.f$years, cex.axis=0.9)
lines(gaps.f[,"eight15"]~gaps.f$years, type="o", col="cornflowerblue", lwd=2)
lines(gaps.f[,"five15"]~gaps.f$years, type="o", col="greenyellow", lwd=2)

lines(gapsnorw.f[,"eight50"]~gapsnorw.f$years, type="o", col="red", lwd=2)
lines(gapsnorw.f[,"eight15"]~gapsnorw.f$years, type="o", col="blue", lwd=2)
lines(gapsnorw.f[,"five15"]~gapsnorw.f$years, type="o", col="green", lwd=2)


legend(1985,1.2, c("85/15 gap","85/50 gap", "50/15 gap","85/15 gap baseyear 85","85/50 gap baseyear 85", "50/15 gap baseyear 85"), 
lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green","cornflowerblue","hotpink2","greenyellow" )) # gives the legend lines the correct color and widt

dev.off()


