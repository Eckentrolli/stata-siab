#Calculate Variance & gaps for age/education cells
#West germany fulltime workers only

options(scipen=30)

library(Hmisc)
library(parallel)
library(car)
library(xtable)
library(WriteXLS)


load("/u/home/jd1033/DFG-descriptives/ResidualsSmallFT.RData")

setwd("/u/home/jd1033/DFG-descriptives")


#trim <- trim[sample(nrow(trim), 50000), ] 

#Create  bildcat variable with only 4 values (analogous to Dustman)
#Bildcat labels: Kein = 0, Schulabschluss = 1, Ausbildung = 2, Hochschule = 3

trim$bildcat <- recode(trim$IP2A, "c(-9, 1)= 0; 2=1; c(3,4)=2; c(5,6)= 3")
trim$bildcat_gr <- recode(trim$IP2A, "c(-9, 1)= 0; c(2,3,4)=1; c(5,6)= 2")
#Bildcat_gr: Uni/FH: 2, Abi/Lehre: 1, Other: 0

#Make two tables, male and female each
#Define cells with 
#Bildung 6x
#Age groups 17-30 = 1, 31-40 = 2, 41-50 =3 , 51-62 =4 4x
#Years 1985,1990,2000,2004,2010

#Create subsamples of male and female workers
males <- subset(trim, frau==0 )
females <- subset(trim, frau==1 )

rm(trim)

#Choose analysis years to expand on Dustman(2009):
# 1985, 1990, 2000, 2004, 2010

#years <- c(1985,1990,2000,2004,2007, 2010)
years <- sort(subset(unique(males$year),unique(males$year)>=1985))
bilds <- c(0,1,2)
ages <- cbind (c(18,25,35,45,55),c(24,34,44,54,63))

#cellsm <- matrix(nrow=((length(bilds))*nrow(ages)), ncol=length(years)+1)
#cellsf <- matrix(nrow=((length(bilds)+1)*nrow(ages)), ncol=length(years))

#rownames(cellsm) <- c(rep("College", 4), rep("High School", 4), rep("Vocational", 4), rep("No degree",4) )

#colnames(cellsm) <-c("Age","1985","1990","2000","2004", "2007","2010")


#create empty cells matrices

#cellsf <- cellsm

#create input matrix with all cell criteria
input <- expand.grid(years, bilds, ages[,1])
tmp <- expand.grid(years, bilds, ages[,2])
input <- cbind(input, tmp[3])
input$Var2 <- as.factor(input$Var2)
colnames(input) <- c("year", "bildcat_gr", "age.min", "age.max")

nr_year.m <-data.frame()
for (i in 1:length(years)) {
nr_year.m[i,1]<-years[i]
nr_year.m[i,2] <- nrow(males[males$year ==years[i],])
}

nr_year.f <-data.frame()
for (i in 1:length(years)) {
nr_year.f[i,1]<-years[i]
nr_year.f[i,2] <- nrow(females[females$year ==years[i],])
}

#For each age/education cell and year, the mean, variance and quantile gaps are calculated
# for values and residuals

calculatem <- function (i) {
	sample1 <- subset(males, year==i[[1]] & bildcat_gr==i[[2]] & age>=i[[3]] & age <=i[[4]])
	groupmean <- wtd.mean(sample1$discount, weights=sample1$sweight, na.rm=FALSE)
	variance <- wtd.var(sample1$discount, weights=sample1$sweight, na.rm=FALSE)
	SD <- sqrt(wtd.var(sample1$discount, weights=sample1$sweight, na.rm=FALSE))
	resvar <- wtd.var(sample1$residualsmf, weights=sample1$sweight, na.rm=FALSE)
	resSD <- sqrt(wtd.var(sample1$residualsmf, weights=sample1$sweight, na.rm=FALSE))
	quants <- wtd.quantile(sample1$discount, probs=c(0.85,0.5,0.15),weights=sample1$sweight, na.rm=FALSE)
	resQuants <- wtd.quantile(sample1$residualsmf, probs=c(0.85,0.5,0.15), weights=sample1$sweight, na.rm=FALSE)
	nine50 <- (quants["85%"] - quants["50%"])
	nine10 <- (quants["85%"] - quants["15%"])
	five10 <- (quants["50%"] - quants["15%"])
	
	resnine50 <- (resQuants["85%"] - resQuants["50%"])
	resnine10 <- (resQuants["85%"] - resQuants["15%"])
	resfive10 <- (resQuants["50%"] - resQuants["15%"])
	obs <- nrow(sample1)
	share <- obs/nr_year.m[nr_year.m[,1]==i[[1]] ,2]
	rm(sample1)
	return(c(obs, groupmean, share, variance, SD, nine50, nine10, five10, resvar, resSD, resnine50, resnine10, resfive10))
	}
	
#resultsm <- apply(input, 1, calculatem)

resultsm <- t(apply(input, 1, calculatem))
colnames(resultsm) <- c( "obs", "mean", "share", "variance","SD", "eight50", "eight15", "five15", "Res.variance", "Res.SD", "Res.nine50","Res.nine10", "Res.five10")

#Attach results collumns to input collumns, new results dataframe with all relevant data
resultsm <- cbind(input, resultsm)

save(resultsm, file="Results/FT/CellanalysisM.RData")	


#Order the results as year-blocks in table res.form.m

# for (i in years) {
	# tmp <- subset(resultsm, years==i)
	# nam <- paste("tmp", i, sep = "")
	# assign(nam, tmp)
	# }

# res.form.m <- rbind(tmp1985, tmp1990, tmp2000, tmp2004, tmp2007, tmp2010)

# rm(tmp1985,tmp1990, tmp2000, tmp2004, tmp2007, tmp2010)
	

	
#calculation females, same as above, only for dataset "females"




calculatef <- function (i) {
	sample1 <- subset(females, year==i[[1]] & bildcat_gr==i[[2]] & age>=i[[3]] & age <=i[[4]])
	groupmean <- wtd.mean(sample1$discount, weights=sample1$sweight, na.rm=FALSE)
	variance <- wtd.var(sample1$discount,weights=sample1$sweight, na.rm=FALSE)
	SD <- sqrt(wtd.var(sample1$discount,weights=sample1$sweight, na.rm=FALSE))
	resvar <- wtd.var(sample1$residualsmf,weights=sample1$sweight, na.rm=FALSE)
	resSD <- sqrt(wtd.var(sample1$residualsmf,weights=sample1$sweight, na.rm=FALSE))
	quants <- wtd.quantile(sample1$discount, probs=c(0.85,0.5,0.15),weights=sample1$sweight, na.rm=FALSE)
	resQuants <- wtd.quantile(sample1$residualsmf, probs=c(0.85,0.5,0.15),weights=sample1$sweight, na.rm=FALSE)
	nine50 <- (quants["85%"] - quants["50%"])
	nine10 <- (quants["85%"] - quants["15%"])
	five10 <- (quants["50%"] - quants["15%"])
	
	resnine50 <- (resQuants["85%"] - resQuants["50%"])
	resnine10 <- (resQuants["85%"] - resQuants["15%"])
	resfive10 <- (resQuants["50%"] - resQuants["15%"])
	obs <- nrow(sample1)
	share <- obs/nr_year.f[nr_year.f[,1]==i[[1]] ,2]
	rm(sample1)
	return(c(obs, groupmean, share, variance, SD, nine50, nine10, five10, resvar, resSD, resnine50, resnine10, resfive10))
	}
	
resultsf <- t(apply(input, 1, calculatef))
	
colnames(resultsf) <- c( "obs", "mean", "share", "variance","SD", "eight50", "eight15", "five15", "Res.variance", "Res.SD", "Res.nine50","Res.nine10", "Res.five10")

resultsf <- cbind(input, resultsf)
save(resultsf, file="Results/FT/CellanalysisF.RData")
#colnames(resultsf) <- c("years", "bildcat", "age.min", "age.max", "obs", "mean", "share", "variance","SD", "eight50", "eight15", "five15", "Res.variance", "Res.SD", "Res.nine50","Res.nine10", "Res.five10")

# for (i in years) {
	# tmp <- subset(resultsf, years==i)
	# nam <- paste("tmp", i, sep = "")
	# assign(nam, tmp)
	# }

# res.form.f <- rbind(tmp1985,tmp1990, tmp2000, tmp2004, tmp2007,tmp2010)

rm(males, females)

#Create separate Sub-Tables for each educational outcome

#MALES
edu <- c(0,1,2)

edunames <- c("No degree","Vocational&High School", "College")

years.backup<-years

years <- c(1985, 1991, 1998, 2004, 2010)
 
subtable <- function(x) {
	
	temp <- matrix(ncol=length(years)+2, nrow=(3*nrow(ages)))

	colnames(temp)[3:(length(years)+2)] <- years

	colnames(temp)[1:2] <- c("Ages", "Statistics")

	temp[,1]<-c(rep(paste(ages[1,1],"-",ages[1,2]),3), rep(paste(ages[2,1],"-",ages[2,2]),3), rep(paste(ages[3,1],"-",ages[3,2]),3), rep(paste(ages[4,1],"-",ages[4,2]),3), rep(paste(ages[5,1],"-",ages[5,2]),3))

	#rownames(temp) <- c(rep(paste(ages[1,1],"-",ages[1,2]),3), rep(paste(ages[2,1],"-",ages[2,2]),3), rep(paste(ages[3,1],"-",ages[3,2]),3), rep(paste(ages[4,1],"-",ages[4,2]),3))

	temp[,2] <- rep(c("Variance","85/15-gap", "Share"), 5)

	for (i in years) {
		temp[1,paste(i)] <- round(resultsm$Res.variance[resultsm$year==i & resultsm$age.min==ages[1,1] & resultsm$bildcat_gr==x],digits=3)
		temp[2,paste(i)] <- round(resultsm$eight15[resultsm$year==i & resultsm$age.min==ages[1,1] & resultsm$bildcat_gr==x],digits=3)
		temp[3,paste(i)] <- round(resultsm$share[resultsm$year==i & resultsm$age.min==ages[1,1] & resultsm$bildcat_gr==x] ,digits=3)
		temp[4,paste(i)] <- round(resultsm$Res.variance[resultsm$year==i & resultsm$age.min==ages[2,1] & resultsm$bildcat_gr==x],digits=3)
		temp[5,paste(i)] <- round(resultsm$eight15[resultsm$year==i & resultsm$age.min==ages[2,1] & resultsm$bildcat_gr==x],digits=3)
		temp[6,paste(i)] <- round(resultsm$share[resultsm$year==i & resultsm$age.min==ages[2,1] & resultsm$bildcat_gr==x],digits=3)
		temp[7,paste(i)] <- round(resultsm$Res.variance[resultsm$year==i & resultsm$age.min==ages[3,1] & resultsm$bildcat_gr==x],digits=3)
		temp[8,paste(i)] <- round(resultsm$eight15[resultsm$year==i & resultsm$age.min==ages[3,1] & resultsm$bildcat_gr==x],digits=3)
		temp[9,paste(i)] <- round(resultsm$share[resultsm$year==i & resultsm$age.min==ages[3,1] & resultsm$bildcat_gr==x],digits=3)
		temp[10,paste(i)] <- round(resultsm$Res.variance[resultsm$year==i & resultsm$age.min==ages[4,1] & resultsm$bildcat_gr==x],digits=3)
		temp[11,paste(i)] <- round(resultsm$eight15[resultsm$year==i & resultsm$age.min==ages[4,1] & resultsm$bildcat_gr==x],digits=3)
		temp[12,paste(i)] <- round(resultsm$share[resultsm$year==i & resultsm$age.min==ages[4,1] & resultsm$bildcat_gr==x],digits=3)
		temp[13,paste(i)] <- round(resultsm$Res.variance[resultsm$year==i & resultsm$age.min==ages[5,1] & resultsm$bildcat_gr==x],digits=3)
		temp[14,paste(i)] <- round(resultsm$eight15[resultsm$year==i & resultsm$age.min==ages[5,1] & resultsm$bildcat_gr==x],digits=3)
		temp[15,paste(i)] <- round(resultsm$share[resultsm$year==i & resultsm$age.min==ages[5,1] & resultsm$bildcat_gr==x],digits=3)
		}

		nam <- paste("males.edu.",x,sep="")
	assign(nam, temp )
	
	# if (x==2) {
		# temp[1,1] <- "College Test"
	# }
	
	temptab <- xtable(temp, caption=paste(edunames[x+1],"degree"))
	print(temptab, include.rownames=FALSE, file=paste("Results/FT/Tables/",edunames[x+1],"M.tex", sep=""))
	
	xltable <- data.frame(temp)
	colnames(xltable)[3:(length(years)+2)] <- years
	WriteXLS("xltable", ExcelFileName=paste("Results/FT/Tables/",edunames[x+1],"M.xls",sep=""), row.names=TRUE,BoldHeaderRow = TRUE)
	
	return(temp)
}

#subtable.results.m <- mclapply(edu, function(x) subtable(x), mc.cores=3)
subtable.results.m <- lapply(edu, function(x) subtable(x))


#edunames.backward <- c( "College","High School&Vocational","No degree")
names(subtable.results.m) <- edunames  #.backward


save(subtable.results.m, file="Results/FT/SubtablesM.RData")





#Females
subtable.f <- function(x) {
	
	temp <- matrix(ncol=length(years)+2, nrow=(3*nrow(ages)))

	colnames(temp)[3:(length(years)+2)] <- years

	colnames(temp)[1:2] <- c("Ages", "Statistics")

	temp[,1]<-c(rep(paste(ages[1,1],"-",ages[1,2]),3), rep(paste(ages[2,1],"-",ages[2,2]),3), rep(paste(ages[3,1],"-",ages[3,2]),3), rep(paste(ages[4,1],"-",ages[4,2]),3), rep(paste(ages[5,1],"-",ages[5,2]),3))

	temp[,2] <- rep(c("Variance","85/15-gap", "Share"), 5)

	for (i in years) {
		temp[1,paste(i)] <- round(resultsf$Res.variance[resultsf$year==i & resultsf$age.min==ages[1,1] & resultsf$bildcat_gr==x],digits=3)
		temp[2,paste(i)] <- round(resultsf$eight15[resultsf$year==i & resultsf$age.min==ages[1,1] & resultsf$bildcat_gr==x],digits=3)
		temp[3,paste(i)] <- round(resultsf$share[resultsf$year==i & resultsf$age.min==ages[1,1] & resultsf$bildcat_gr==x] ,digits=3)
		temp[4,paste(i)] <- round(resultsf$Res.variance[resultsf$year==i & resultsf$age.min==ages[2,1] & resultsf$bildcat_gr==x],digits=3)
		temp[5,paste(i)] <- round(resultsf$eight15[resultsf$year==i & resultsf$age.min==ages[2,1] & resultsf$bildcat_gr==x],digits=3)
		temp[6,paste(i)] <- round(resultsf$share[resultsf$year==i & resultsf$age.min==ages[2,1] & resultsf$bildcat_gr==x],digits=3)
		temp[7,paste(i)] <- round(resultsf$Res.variance[resultsf$year==i & resultsf$age.min==ages[3,1] & resultsf$bildcat_gr==x],digits=3)
		temp[8,paste(i)] <- round(resultsf$eight15[resultsf$year==i & resultsf$age.min==ages[3,1] & resultsf$bildcat_gr==x],digits=3)
		temp[9,paste(i)] <- round(resultsf$share[resultsf$year==i & resultsf$age.min==ages[3,1] & resultsf$bildcat_gr==x],digits=3)
		temp[10,paste(i)] <- round(resultsf$Res.variance[resultsf$year==i & resultsf$age.min==ages[4,1] & resultsf$bildcat_gr==x],digits=3)
		temp[11,paste(i)] <- round(resultsf$eight15[resultsf$year==i & resultsf$age.min==ages[4,1] & resultsf$bildcat_gr==x],digits=3)
		temp[12,paste(i)] <- round(resultsf$share[resultsf$year==i & resultsf$age.min==ages[4,1] & resultsf$bildcat_gr==x],digits=3)
		temp[13,paste(i)] <- round(resultsf$Res.variance[resultsf$year==i & resultsf$age.min==ages[5,1] & resultsf$bildcat_gr==x],digits=3)
		temp[14,paste(i)] <- round(resultsf$eight15[resultsf$year==i & resultsf$age.min==ages[5,1] & resultsf$bildcat_gr==x],digits=3)
		temp[15,paste(i)] <- round(resultsf$share[resultsf$year==i & resultsf$age.min==ages[5,1] & resultsf$bildcat_gr==x],digits=3)
		}

	nam <- paste("females.edu.",x,sep="")
	assign(nam, temp )
	
	temptab <- xtable(temp, caption=paste(edunames[x+1],"degree"))
	print(temptab, include.rownames=FALSE, file=paste("Results/FT/Tables/",edunames[x+1],"F.tex", sep=""))
	
	xltable <- data.frame(temp)
	colnames(xltable)[3:(length(years)+2)] <- years
	WriteXLS("xltable", ExcelFileName=paste("Results/FT/Tables/",edunames[x+1],"F.xls",sep=""), row.names=TRUE,BoldHeaderRow = TRUE)
	
	return(temp)
}

subtable.results.f <- lapply(edu, function(x) subtable.f(x))

names(subtable.results.f) <- edunames

save(subtable.results.f, file="Results/FT/SubtablesF.RData")









