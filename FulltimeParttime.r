options(scipen=30)

library(foreign)
library(Hmisc)
library(parallel)
library(car)

#setwd("E:/Data")
setwd("I:/DATA/")
#data <- read.dta("E:/Data/siab_r_7510_v1_testdata.dta", convert.factors=FALSE)
data <- read.dta("siab_r_7510_v1_testdata.dta", convert.factors=FALSE)

setwd("I:/DATA/Results")
#preparation
attach(data)


data$bild <- as.factor(IP2A)
data$deutsch <- as.factor(deutsch)

data$potexp2 <- potexp*potexp
data$potexp3 <- potexp*potexp*potexp

detach(data)

#Create subsample for west german full time workers with non-missing education

trim <- subset(data,  discount> 1 & LF==1 & ost==0 )
rm(data)

sharecalc <- function(x) {
	ft.obs <- nrow(trim[which(trim$year==x & trim$workft ==1 & trim$geringf==0),] )
	pt.obs <- nrow(trim[which(trim$year==x & trim$workft ==0 & trim$geringf==0 ),])
	mpt.obs <- nrow(trim[which(trim$year==x & trim$workft ==0 ),])
	obs <- nrow(trim[which(trim$year==x & trim$geringf==0),])
	obspt <- nrow(trim[which(trim$year==x ),])
	ft.share <- ft.obs/obs
	pt.share <- pt.obs/obs
	mpt.share <- mpt.obs/obspt
	ft.share.pt <- ft.obs/obspt
	pt.share.pt <- pt.obs/obspt
	return(c(x,ft.share, pt.share, mpt.share,ft.share.pt, pt.share.pt))
}

allyears <- unique(trim$year)
allyears <- allyears[order(allyears)]
#allyears <- allyears[which(allyears>=1990)]
results <- lapply(allyears, function(x) sharecalc(x))
res.tab <- results[[1]]
for (i in 2:length(allyears)) {
	res.tab <- rbind( res.tab, results[[i]])
}
colnames(res.tab) <- c("Year", "Fulltime share", "Part-time share", "PT-share incl. marginals", "Fulltime share w. marginals", "PT-Share w. marginals")



trim$bildcat <- recode(trim$IP2A, "c(-9, 1)= 0; 2=1; c(3,4)=2; c(5,6)= 3")
trim$agecat <- recode(trim$age, "17:30=18; 31:40=31; 41:50=41; 51:62=51")
trim$cell <- paste(trim$agecat, trim$IP2A, sep="") 
trim$cell <- as.factor(trim$cell)


#Create subsamples of male and female workers
males <- subset(trim, frau==0)
females <- subset(trim, frau==1)

#rm(trim)

#males
sharecalc.m <- function(x) {
	ft.obs <- nrow(trim[which(trim$year==x & trim$workft ==1 & trim$frau==0 & trim$geringf==0),] )
	pt.obs <- nrow(trim[which(trim$year==x & trim$workft ==0 & trim$frau==0 & trim$geringf==0),])
	mpt.obs <- nrow(trim[which(trim$year==x & trim$workft ==0 & trim$frau==0 ),])
	obs <- nrow(trim[which(trim$year==x & trim$frau==0 & trim$geringf==0),])
	obspt <- nrow(trim[which(trim$year==x & trim$frau==0),])
	ft.share <- ft.obs/obs
	pt.share <- pt.obs/obs
	mpt.share <- mpt.obs/obspt
	ft.share.pt <- ft.obs/obspt
	pt.share.pt <- pt.obs/obspt
	return(c(x,ft.share, pt.share,mpt.share, ft.share.pt, pt.share.pt))
}

results.m <- lapply(allyears, function(x) sharecalc.m(x))
res.tab.m <- results.m[[1]]
for (i in 2:length(allyears)) {
	res.tab.m <- rbind( res.tab.m, results.m[[i]])
}
colnames(res.tab.m) <- c("Year", "Fulltime share", "Part-time share", "PT-share incl. marginals", "Fulltime share w. marginals", "PT-Share w. marginals")

#females

sharecalc.f <- function(x) {
	ft.obs <- nrow(trim[which(trim$year==x & trim$workft ==1 & trim$frau==1 & trim$geringf==0),] )
	pt.obs <- nrow(trim[which(trim$year==x & trim$workft ==0 & trim$frau==1 & trim$geringf==0),])
	obs <- nrow(trim[which(trim$year==x & trim$frau==1 & trim$geringf==0),])
	obspt <- nrow(trim[which(trim$year==x & trim$frau==1),])
	mpt.obs <- nrow(trim[which(trim$year==x & trim$frau==1 & trim$workft ==0 ),])
	ft.share <- ft.obs/obs
	pt.share <- pt.obs/obs
	mpt.share <- mpt.obs/obspt
	ft.share.pt <- ft.obs/obspt
	pt.share.pt <- pt.obs/obspt
	return(c(x,ft.share, pt.share,mpt.share,ft.share.pt, pt.share.pt))
}

results.f <- lapply(allyears, function(x) sharecalc.f(x))
res.tab.f <- results.f[[1]]
for (i in 2:length(allyears)) {
	res.tab.f <- rbind( res.tab.f, results.f[[i]])
}
colnames(res.tab.f) <- c("Year", "Fulltime share", "Part-time share", "PT-share incl. marginals", "Fulltime share w. marginals", "PT-Share w. marginals")

#Plots All

#setEPS()
#postscript("Graphs/FTPT.all.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(res.tab[,2]~res.tab[,1], xlim=c(1980,2011),ylim=c(0,1), xaxt = "n", xlab="",   ylab="Share of FT/PT workers", type="o", lwd=2, col="red")
title(main="Share of FT/PT workers", sub="Not including marginal employment" )
axis(side=1, at=res.tab[,1], labels=res.tab[,1], cex.axis=0.9)
lines(res.tab[,3]~res.tab[,1], type="o", col="blue", lwd=2)
legend(2000,0.6, c("Working fulltime", "Working Part time"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("red","blue")) # gives the legend lines the correct color and widt

#dev.off()

#setEPS()
#postscript("Graphs/FTPTmarg.all.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(res.tab[,2]~res.tab[,1], xlim=c(1999,2011),ylim=c(0,1), xaxt = "n", xlab="", ylab="Share of FT/PT workers", type="o", lwd=2, col="red")
title(main="Share of FT/PT workers", sub="Including marginal employment" )
axis(side=1, at=res.tab[,1], labels=res.tab[,1], cex.axis=0.9)
lines(res.tab[,3]~res.tab[,1], type="o", col="blue", lwd=2)
lines(res.tab[,4]~res.tab[,1], type="o", col="green", lwd=2)
legend(1999,0.7, c("Working fulltime", "Working Part time", "PT with marginals"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("red","blue","green")) # gives the legend lines the correct color and widt

#dev.off()

#Plots Males

#setEPS()
#postscript("Graphs/FTPT.m.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(res.tab.m[,2]~res.tab.m[,1], xlim=c(1980,2011),ylim=c(0,1), xaxt = "n", xlab="",   ylab="Share of male FT/PT workers", type="o", lwd=2, col="red")
title(main="Share of male FT/PT workers", sub="Not including marginal employment" )
axis(side=1, at=res.tab.m[,1], labels=res.tab.m[,1], cex.axis=0.9)
lines(res.tab.m[,3]~res.tab.m[,1], type="o", col="blue", lwd=2)
legend(2000,0.8, c("Working fulltime", "Working Part time"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("red","blue")) # gives the legend lines the correct color and widt

#dev.off()

#setEPS()
#postscript("Graphs/FTPTmarg.m.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(res.tab.m[,2]~res.tab.m[,1], xlim=c(1999,2011),ylim=c(0,1), xaxt = "n", xlab="", ylab="Share of male FT/PT workers", type="o", lwd=2, col="red")
title(main="Share of male FT/PT workers", sub="Including marginal employment" )
axis(side=1, at=res.tab.m[,1], labels=res.tab.m[,1], cex.axis=0.9)
lines(res.tab.m[,3]~res.tab.m[,1], type="o", col="blue", lwd=2)
lines(res.tab.m[,4]~res.tab.m[,1], type="o", col="green", lwd=2)
legend(2000,0.8, c("Working fulltime", "Working Part time", "PT with marginals"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("red","blue","green")) # gives the legend lines the correct color and widt

#dev.off()



#Plots females
#setEPS()
#postscript("Graphs/FTPT.fem.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(res.tab.f[,2]~res.tab.f[,1], xlim=c(1980,2011),ylim=c(0.2,0.8), xaxt = "n", xlab="",   ylab="Share of female FT/PT workers", type="o", lwd=2, col="red")
title(main="Share of female FT/PT workers", sub="Not including marginal employment" )
axis(side=1, at=res.tab.f[,1], labels=res.tab.f[,1], cex.axis=0.9)
lines(res.tab.f[,3]~res.tab.f[,1], type="o", col="blue", lwd=2)
legend(2000,0.8, c("Working fulltime", "Working Part time"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("red","blue")) # gives the legend lines the correct color and widt

#dev.off()

#setEPS()
#postscript("Graphs/FTPTmarg.fem.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(res.tab.f[,2]~res.tab.f[,1], xlim=c(1999,2011),ylim=c(0.2,0.8), xaxt = "n", xlab="", ylab="Share of FT/PT workers", type="o", lwd=2, col="red")
title(main="Share of female FT/PT workers", sub="Including marginal employment" )
axis(side=1, at=res.tab.f[,1], labels=res.tab.f[,1], cex.axis=0.9)
lines(res.tab.f[,3]~res.tab.f[,1], type="o", col="blue", lwd=2)
lines(res.tab.f[,4]~res.tab.f[,1], type="o", col="green", lwd=2)
legend(2000,0.8, c("Working fulltime", "Working Part time", "PT with marginals"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("red","blue","green")) # gives the legend lines the correct color and widt

#dev.off()


