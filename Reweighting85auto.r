#Reweighting scheme for comparison of 

options(scipen=30)

library(foreign)
library(Hmisc)
library(parallel)

setwd("E:/DATA")
#setwd("I:/DATA")

load("E:/DATA/Residuals.RData")
#load("I:/DATA/Residuals.RData")

rm(varsall, variances, results, resultsall)


#Males only 
trim.m <- subset(trim, frau==0 & age >= 20 & age <=60)

years <- years[years!=1985]

rm(trim)



reweight85 <- function(i) {

	#calculation for base year 1985 to 2010

	pooled <- subset(trim.m, year==1985 | year==i )

	#Create year dummies
	pooled$dyear <-  pooled$year
	
	pooled$dyear[pooled$dyear==1985] <- 0
	pooled$dyear[pooled$dyear==i] <- 1
	


	#Probit of likelihood of being in 2010, given charactersistics
	results <- glm(dyear ~ potexp+potexp2+potexp3+bild+potexp:bild+potexp2:bild+potexp3:bild, data=pooled, family="binomial")
	summary(results)

	rm(pooled)
	#combine values & fits ainto dataframe
	rw.data <- cbind(results$data, fitted.values=results$fitted.values )

	rm(results)

	#Calculate reweighting factor. (1-P)/P small if P large (e.g. highly educated)
	rw.data$base85 <- (1-rw.data$fitted.values)/rw.data$fitted.values
	
	print(summary(rw.data[which(rw.data$year==1985),]$base85)

	#remove superfluous count columns
	rw.data <- rw.data[!names(rw.data) %in% c("count.1", "count.2","dyear") ]

	rw.data$base85[rw.data$year==1985] <- 1 
	#res <- rw.data[!names(rw.data) %in% "fitted.values"]

	res <- rw.data
	save(rw.data, file=paste("reweighting/rw",i,".RData" , sep=""))
if (i==years[1]) {
		res <- res
	}
	else {
		res <- subset(res, year!=1985)
	}
	

	rm(rw.data)
	
	return(res)

}


results.85  <- lapply(years,function(x) reweight85(x))

trim.m.rw <- results.85[[1]] 

for(i in 2:length(years)) {
	temp.2 <- results.85[[i]]
	temp.2 <- subset(temp.2, year!= 1985)
	trim.m.rw <- rbind(trim.m.rw, temp.2)
	}
	
save(trim.m.rw, file="reweighting/trim.m.rw.RData")