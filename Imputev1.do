set more off

*new imputation CHK
gen maxwest=(censwestNEW-1)

forvalues i=1985/2010 {
	imputw4 ltentgelt age age2 i.bildcat i.bildcat*age i.deutsch*age i.deutsch i.bildcat*age2 i.deutsch*age2 if year==`i' , cens(censored) grenze(maxwest) outvar(imput)
}


[pweight=1/sweight]

forvalues i=1980/2010 {
	cnreg ltentgelt age age2 bildyear bildyear2 if year==`i', cens(censwest) 
	predict xb, xb
	gen alpha=(ln(dailywest)-xb)/_b[_se] if censwest==1 & year==`i'
	gen imputtest=ltentgelt if year==`i'
	replace imputtest = invnorm(uniform()*(1-norm(alpha))+norm(alpha)) * _b[_se] if year==`i'
	xb if cen==1 & year==`i'
	drop xb alpha 
	
	}



gen maxwest=dailywest *0.98
gen censwest=1
replace censwest =1 if tentgelt_gr>=maxwest & ost==0

gen maxost=dailyost *0.98
gen censost=0
replace censost=1 if tentgelt_gr>=maxost & ost==1


forvalues i=1985/2010 {
	imputw ltentgelt age age2 bildyear bildyear2 frau deutsch if year==`i' & ltentgelt <. & ost==0 , cens(censwest) grenze(maxwest) outvar(imputwest)
}

replace imputwest = ltentgelt if imputwest >=. & censwest ==0

forvalues i=1980/2010 {
	imputw ltentgelt age age2 bildyear bildyear2 frau deutsch if year==`i' & tentgelt_gr <. & ost==1, cens(censost) grenze(maxost) outvar(imputost)
}
replace imputost = ltentgelt if imputost >=. & censost ==0 & ost==1

gen imput=imputwest if ost==0
replace imput=imputost if ost==1

replace imput=0 if imput>=.
