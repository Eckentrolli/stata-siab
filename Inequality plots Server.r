options(scipen=30)

library(foreign)
library(xtable)
library(Hmisc)
library(parallel)
library(car)

setwd("/u/home/jd1033/DFG-descriptives")
load("siab_r_7510_v1_forResiduals.RData")

#preparation
attach(data)

data$bildcat <- as.factor(bildcat)
data$deutsch <- as.factor(deutsch)

data$age3 <- age*age*age

detach(data)

trim <- subset(data,  discount> 1 & workft==1 & ost==0 & geringf == 0 )

rm(data)

#Remove marginal jobs!!!!!
describe(trim$year)

years <- unique(trim$year)
years <-sort(years,decreasing=TRUE)

variances <- unlist(mclapply(years, function(x) var(trim[ which(trim$year==x),]$discount),mc.cores=3))

quantiles <- mclapply(years, function(x) quantile(trim[ which(trim$year==x),]$discount, prob=c(0.1, 0.5, 0.9)),mc.cores=3)

quant2 <- matrix(nrow=length(years), ncol=3)
colnames(quant2) <- c("ten","fifty","ninety")
for(i in 1:length(years)) {
quant2[i,] <- unlist(quantiles[i])
}
 
descript1 <- cbind(years,variances, quant2)
descript1 <- as.data.frame(descript1)
attach(descript1)
descript1$fifty10 <- fifty/ten
descript1$ninety50 <- ninety/fifty
descript1$ninety10 <- ninety/ten
head(descript1)
detach(descript1)

#plot(descript1$variances~years)
#plot(descript1$fifty~years)


save.image(file = "InequalityPlots.RData", compress=TRUE)


