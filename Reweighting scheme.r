#Reweighting scheme for comparison of 

options(scipen=30)

library(foreign)
library(Hmisc)
library(parallel)

#setwd("E:/DATA")
setwd("I:/DATA")

#load("E:/DATA/Residuals.RData")
load("I:/DATA/Residuals.RData")

rm(varsall, variances, results, resultsall)

#Males only 
trim.m <- subset(trim, frau==0)

rm(trim)

#calculation for base year 1985 to 2010

pooled <- subset(trim.m, year==1985 | year==2010 )

#Create year dummies
pooled$dyear <-  pooled$year
pooled$dbase <-  pooled$year

pooled$dyear[pooled$dyear==1985] <- 0
pooled$dyear[pooled$dyear==2010] <- 1
pooled$dbase[pooled$dbase==1985] <- 1
pooled$dbase[pooled$dbase==2010] <- 0


#Probit of likelihood of being in 2010, given charactersistics
results <- glm(dyear ~ potexp+potexp2+potexp3+bild+potexp:bild+potexp2:bild+potexp3:bild, data=pooled, weights=sweight, family="binomial")
summary(results)

rm(pooled)
#combine values & fits ainto dataframe
rw.data <- cbind(results$data, fitted.values=results$fitted.values )

rm(results)

#Calculate reweighting factor. (1-P)/P small if P large (e.g. highly educated)
rw.data$base85 <- (1-rw.data$fitted.values)/rw.data$fitted.values


cbind(rw.data$fitted.values[1:20],rw.data$base85[1:20])

#remove superfluous count columns
rw.data <- rw.data[!names(rw.data) %in% c("count.1", "count.2","dyear", "dbase") ]

trim.m.rw <- rw.data[!names(rw.data) %in% "fitted.values"]

save(rw.data, file="reweighting/rw8510.RData" )

rm(rw.data)


#calculation for base year 1985 to 2009
pooled <- subset(trim.m, year==1985 | year==2009 )
#Create year dummies
pooled$dyear <-  pooled$year
pooled$dbase <-  pooled$year
pooled$dyear[pooled$dyear==1985] <- 0
pooled$dyear[pooled$dyear==2009] <- 1
pooled$dbase[pooled$dbase==1985] <- 1
pooled$dbase[pooled$dbase==2009] <- 0

#Probit of likelihood of being in 2010, given charactersistics
results <- glm(dyear ~ potexp+potexp2+potexp3+bild+potexp:bild+potexp2:bild+potexp3:bild, data=pooled, weights=sweight, family="binomial")
summary(results)
rm(pooled)
#combine values & fits ainto dataframe
rw.data <- cbind(results$data, fitted.values=results$fitted.values )
rm(results)
#Calculate reweighting factor. (1-P)/P small if P large (e.g. highly educated)
rw.data$base85 <- (1-rw.data$fitted.values)/rw.data$fitted.values
cbind(rw.data$fitted.values[1:20],rw.data$base85[1:20])
#remove superfluous count columns
rw.data <- rw.data[!names(rw.data) %in% c("count.1", "count.2","dyear", "dbase") ]
temp <- rw.data[!names(rw.data) %in% "fitted.values"]
temp <- subset(temp, year!= 1985)
trim.m.rw <- rbind(trim.m.rw, temp)
save(rw.data, file="reweighting/rw8509.RData" )
rm(rw.data)


#calculation for base year 1985 to 2008

pooled <- subset(trim.m, year==1985 | year==2008 )
#Create year dummies
pooled$dyear <-  pooled$year
pooled$dbase <-  pooled$year
pooled$dyear[pooled$dyear==1985] <- 0
pooled$dyear[pooled$dyear==2008] <- 1
pooled$dbase[pooled$dbase==1985] <- 1
pooled$dbase[pooled$dbase==2008] <- 0

#Probit of likelihood of being in 2010, given charactersistics
results <- glm(dyear ~ potexp+potexp2+potexp3+bild+potexp:bild+potexp2:bild+potexp3:bild, data=pooled, weights=sweight, family="binomial")
summary(results)
rm(pooled)
#combine values & fits ainto dataframe
rw.data <- cbind(results$data, fitted.values=results$fitted.values )
rm(results)
#Calculate reweighting factor. (1-P)/P small if P large (e.g. highly educated)
rw.data$base85 <- (1-rw.data$fitted.values)/rw.data$fitted.values
cbind(rw.data$fitted.values[1:20],rw.data$base85[1:20])
#remove superfluous count columns
rw.data <- rw.data[!names(rw.data) %in% c("count.1", "count.2","dyear", "dbase") ]
temp <- rw.data[!names(rw.data) %in% "fitted.values"]
temp <- subset(temp, year!= 1985)
trim.m.rw <- rbind(trim.m.rw, temp)
save(rw.data, file="reweighting/rw8508.RData" )
rm(rw.data)


#calculation for base year 1985 to 2007

pooled <- subset(trim.m, year==1985 | year==2007 )
#Create year dummies
pooled$dyear <-  pooled$year
pooled$dbase <-  pooled$year
pooled$dyear[pooled$dyear==1985] <- 0
pooled$dyear[pooled$dyear==2007] <- 1
pooled$dbase[pooled$dbase==1985] <- 1
pooled$dbase[pooled$dbase==2007] <- 0

#Probit of likelihood of being in 2010, given charactersistics
results <- glm(dyear ~ potexp+potexp2+potexp3+bild+potexp:bild+potexp2:bild+potexp3:bild, data=pooled, weights=sweight, family="binomial")
summary(results)
rm(pooled)
#combine values & fits ainto dataframe
rw.data <- cbind(results$data, fitted.values=results$fitted.values )
rm(results)
#Calculate reweighting factor. (1-P)/P small if P large (e.g. highly educated)
rw.data$base85 <- (1-rw.data$fitted.values)/rw.data$fitted.values
cbind(rw.data$fitted.values[1:20],rw.data$base85[1:20])
#remove superfluous count columns
rw.data <- rw.data[!names(rw.data) %in% c("count.1", "count.2","dyear", "dbase") ]
temp <- rw.data[!names(rw.data) %in% "fitted.values"]
temp <- subset(temp, year!= 1985)
trim.m.rw <- rbind(trim.m.rw, temp)
save(rw.data, file="reweighting/rw8507.RData" )
rm(rw.data)


#calculation for base year 1985 to 2006

pooled <- subset(trim.m, year==1985 | year==2006 )
#Create year dummies
pooled$dyear <-  pooled$year
pooled$dbase <-  pooled$year
pooled$dyear[pooled$dyear==1985] <- 0
pooled$dyear[pooled$dyear==2006] <- 1
pooled$dbase[pooled$dbase==1985] <- 1
pooled$dbase[pooled$dbase==2006] <- 0

#Probit of likelihood of being in 2010, given charactersistics
results <- glm(dyear ~ potexp+potexp2+potexp3+bild+potexp:bild+potexp2:bild+potexp3:bild, data=pooled, weights=sweight, family="binomial")
summary(results)
rm(pooled)
#combine values & fits ainto dataframe
rw.data <- cbind(results$data, fitted.values=results$fitted.values )
rm(results)
#Calculate reweighting factor. (1-P)/P small if P large (e.g. highly educated)
rw.data$base85 <- (1-rw.data$fitted.values)/rw.data$fitted.values
cbind(rw.data$fitted.values[1:20],rw.data$base85[1:20])
#remove superfluous count columns
rw.data <- rw.data[!names(rw.data) %in% c("count.1", "count.2","dyear", "dbase") ]
temp <- rw.data[!names(rw.data) %in% "fitted.values"]
temp <- subset(temp, year!= 1985)
trim.m.rw <- rbind(trim.m.rw, temp)
save(rw.data, file="reweighting/rw8506.RData" )
rm(rw.data)


#calculation for base year 1985 to 2005

pooled <- subset(trim.m, year==1985 | year==2005 )
#Create year dummies
pooled$dyear <-  pooled$year
pooled$dbase <-  pooled$year
pooled$dyear[pooled$dyear==1985] <- 0
pooled$dyear[pooled$dyear==2005] <- 1
pooled$dbase[pooled$dbase==1985] <- 1
pooled$dbase[pooled$dbase==2005] <- 0

#Probit of likelihood of being in 2010, given charactersistics
results <- glm(dyear ~ potexp+potexp2+potexp3+bild+potexp:bild+potexp2:bild+potexp3:bild, data=pooled, weights=sweight, family="binomial")
summary(results)
rm(pooled)
#combine values & fits ainto dataframe
rw.data <- cbind(results$data, fitted.values=results$fitted.values )
rm(results)
#Calculate reweighting factor. (1-P)/P small if P large (e.g. highly educated)
rw.data$base85 <- (1-rw.data$fitted.values)/rw.data$fitted.values
cbind(rw.data$fitted.values[1:20],rw.data$base85[1:20])
#remove superfluous count columns
rw.data <- rw.data[!names(rw.data) %in% c("count.1", "count.2","dyear", "dbase") ]
temp <- rw.data[!names(rw.data) %in% "fitted.values"]
temp <- subset(temp, year!= 1985)
trim.m.rw <- rbind(trim.m.rw, temp)
save(rw.data, file="reweighting/rw8505.RData" )
rm(rw.data)


#calculation for base year 1985 to 2004

pooled <- subset(trim.m, year==1985 | year==2004 )
#Create year dummies
pooled$dyear <-  pooled$year
pooled$dbase <-  pooled$year
pooled$dyear[pooled$dyear==1985] <- 0
pooled$dyear[pooled$dyear==2004] <- 1
pooled$dbase[pooled$dbase==1985] <- 1
pooled$dbase[pooled$dbase==2004] <- 0

#Probit of likelihood of being in 2010, given charactersistics
results <- glm(dyear ~ potexp+potexp2+potexp3+bild+potexp:bild+potexp2:bild+potexp3:bild, data=pooled, weights=sweight, family="binomial")
summary(results)
rm(pooled)
#combine values & fits ainto dataframe
rw.data <- cbind(results$data, fitted.values=results$fitted.values )
rm(results)
#Calculate reweighting factor. (1-P)/P small if P large (e.g. highly educated)
rw.data$base85 <- (1-rw.data$fitted.values)/rw.data$fitted.values
cbind(rw.data$fitted.values[1:20],rw.data$base85[1:20])
#remove superfluous count columns
rw.data <- rw.data[!names(rw.data) %in% c("count.1", "count.2","dyear", "dbase") ]
temp <- rw.data[!names(rw.data) %in% "fitted.values"]
temp <- subset(temp, year!= 1985)
trim.m.rw <- rbind(trim.m.rw, temp)
save(rw.data, file="reweighting/rw8504.RData" )
rm(rw.data)


#calculation for base year 1985 to 2003

pooled <- subset(trim.m, year==1985 | year==2003 )
#Create year dummies
pooled$dyear <-  pooled$year
pooled$dbase <-  pooled$year
pooled$dyear[pooled$dyear==1985] <- 0
pooled$dyear[pooled$dyear==2003] <- 1
pooled$dbase[pooled$dbase==1985] <- 1
pooled$dbase[pooled$dbase==2003] <- 0

#Probit of likelihood of being in 2010, given charactersistics
results <- glm(dyear ~ potexp+potexp2+potexp3+bild+potexp:bild+potexp2:bild+potexp3:bild, data=pooled, weights=sweight, family="binomial")
summary(results)
rm(pooled)
#combine values & fits ainto dataframe
rw.data <- cbind(results$data, fitted.values=results$fitted.values )
rm(results)
#Calculate reweighting factor. (1-P)/P small if P large (e.g. highly educated)
rw.data$base85 <- (1-rw.data$fitted.values)/rw.data$fitted.values
cbind(rw.data$fitted.values[1:20],rw.data$base85[1:20])
#remove superfluous count columns
rw.data <- rw.data[!names(rw.data) %in% c("count.1", "count.2","dyear", "dbase") ]
temp <- rw.data[!names(rw.data) %in% "fitted.values"]
temp <- subset(temp, year!= 1985)
trim.m.rw <- rbind(trim.m.rw, temp)
save(rw.data, file="reweighting/rw8503.RData" )
rm(rw.data)













save(trim.m.rw, file="reweighting/trim.m.rw85.RData")