options(scipen=30)

library(foreign)
library(Hmisc)
library(WriteXLS)
library(xtable)


setwd("/u/home/jd1033/DFG-descriptives/Results/FT")
#setwd("T:/")


load("reweighting/trim.m.rw85.RData")
load("reweighting/trim.f.rw85.RData")

setwd("/u/home/jd1033/DFG-descriptives/Results/Age2555")

years <- unique(trim.m.rw$year)
years <- sort(years, decreasing=TRUE)

trim.m.rw <-subset(trim.m.rw, age >=25 & age <=55)
trim.f.rw <-subset(trim.f.rw, age >=25 & age <=55)

#calculate counterfactual weights as product of h(x|t=0)/h(x|t=1) and sample weights "sweight"
trim.m.rw$rweight <- trim.m.rw$sweight*trim.m.rw$base85	
trim.f.rw$rweight <- trim.f.rw$sweight*trim.f.rw$base85

trim.m.rw$countmf <- c(1:length(trim.m.rw$discount))
trim.f.rw$countmf <- c(1:length(trim.f.rw$discount))


################################################
######### Gap-Plots ##########################
################################################

#Calculate Quantile Gaps for Males and Females separate

#MALES

gapsnorw <- data.frame()
gaps <- data.frame()

#define program

for (i in years) {

	tmp <- subset(trim.m.rw, trim.m.rw$year==i)	#subset of data for year i
	
	#calculate weighted quantiles, reweighted with counterfactual weights
	quants<- wtd.quantile(tmp$discount, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$rweight)
	
	#calculate weighted quantiles, reweighted with unmodified sample weights
	quants.norw<- wtd.quantile(tmp$discount, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$sweight)
	
	#quantile gaps
	eight50 <- (quants["85%"] - quants["50%"])
	eight15 <- (quants["85%"] - quants["15%"])
	five15 <- (quants["50%"] - quants["15%"])
	
	eight50.norw <- (quants.norw["85%"] - quants.norw["50%"])
	eight15.norw <- (quants.norw["85%"] - quants.norw["15%"])
	five15.norw <- (quants.norw["50%"] - quants.norw["15%"])
		
	
	gaps[2011-i,1] <- eight50
	gaps[2011-i,2] <- eight15
	gaps[2011-i,3] <- five15
	gaps[2011-i,4] <- quants["85%"]
	gaps[2011-i,5] <- quants["50%"]
	gaps[2011-i,6] <- quants["15%"]
	
	gapsnorw[2011-i,1] <- eight50.norw
	gapsnorw[2011-i,2] <- eight15.norw
	gapsnorw[2011-i,3] <- five15.norw
	gapsnorw[2011-i,4] <- quants.norw["85%"]
	gapsnorw[2011-i,5] <- quants.norw["50%"]
	gapsnorw[2011-i,6] <- quants.norw["15%"]
}


#create tables + labels
colnames(gaps) <- c("eight50", "eight15", "five15", "85% quantile", "Median", "15% quantile")
gaps <- cbind(years, gaps)
colnames(gaps)[1]<-"years"
rownames(gaps)<-gaps$years

colnames(gapsnorw) <- c("eight50", "eight15", "five15", "85% quantile", "Median", "15% quantile")
gapsnorw <- cbind(years, gapsnorw)
colnames(gapsnorw)[1]<-"years"
rownames(gapsnorw)<-gapsnorw$years

sink("Q-gapsRW85.txt", split=TRUE)
gaps
sink()

sink("Q-gaps.txt", split=TRUE)
gapsnorw
sink()

save(gapsnorw, gaps, file="Gaps_and_NoRW.85.M.RData")


#FEMALES (same procedure as for males)

gapsnorw.f <- data.frame()
gaps.f <- data.frame()

for (i in years) {

	tmp <- subset(trim.f.rw, trim.f.rw$year==i)
	quants<- wtd.quantile(tmp$values, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$rweight)
	#resQuants <- wtd.quantile(tmp$res.base85, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$rweight)

	quants.norw<- wtd.quantile(tmp$values, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$sweight)
	#resQuants.norw <- wtd.quantile(tmp$res.base85, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$sweight)
	
	eight50 <- (quants["85%"] - quants["50%"])
	eight15 <- (quants["85%"] - quants["15%"])
	five15 <- (quants["50%"] - quants["15%"])
	
	eight50.norw <- (quants.norw["85%"] - quants.norw["50%"])
	eight15.norw <- (quants.norw["85%"] - quants.norw["15%"])
	five15.norw <- (quants.norw["50%"] - quants.norw["15%"])
		
	gaps.f[2011-i,1] <- eight50
	gaps.f[2011-i,2] <- eight15
	gaps.f[2011-i,3] <- five15
	gaps.f[2011-i,4] <- quants["85%"]
	gaps.f[2011-i,5] <- quants["50%"]
	gaps.f[2011-i,6] <- quants["15%"]
	
	gapsnorw.f[2011-i,1] <- eight50.norw
	gapsnorw.f[2011-i,2] <- eight15.norw
	gapsnorw.f[2011-i,3] <- five15.norw
	gapsnorw.f[2011-i,4] <- quants.norw["85%"]
	gapsnorw.f[2011-i,5] <- quants.norw["50%"]
	gapsnorw.f[2011-i,6] <- quants.norw["15%"]
}

colnames(gaps.f) <- c("eight50", "eight15", "five15", "85% quantile", "Median", "15% quantile")
gaps.f <- cbind(years, gaps.f)
colnames(gaps.f)[1]<-"years"
rownames(gaps.f)<-gaps.f$years

colnames(gapsnorw.f) <- c("eight50", "eight15", "five15", "85% quantile", "Median", "15% quantile")
gapsnorw.f <- cbind(years, gapsnorw.f)
colnames(gapsnorw.f)[1]<-"years"
rownames(gapsnorw.f)<-gapsnorw.f$years


sink("Q-gapsRW85.F.txt", split=TRUE)
gaps.f
sink()

sink("Q-gaps.F.txt", split=TRUE)
gapsnorw.f
sink()

save(gapsnorw.f, gaps.f, file="Gaps_and_NoRW.85.F.RData")


##################################################################
#PLOTS
################################################################

#load("Gaps_and_NoRW.85.M.RData")
#load("Gaps_and_NoRW.85.F.RData")

#Plot 85/15, 85/50, 50/15 counterfactual quantile gaps for males only 

setEPS()
postscript("Graphs/Gapsmales85age2555.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gaps[,"eight50"]~gaps$years, xlim=c(1985,2011),ylim=c(0.00,1.10), xaxt = "n",  main="Quantile gaps males age 25-55 only, baseyear 85", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="red")
axis(side=1, at=gaps$years, labels=gaps$years, cex.axis=0.9)
lines(gaps[,"eight15"]~gaps$years, type="o", col="blue", lwd=2)
lines(gaps[,"five15"]~gaps$years, type="o", col="green", lwd=2)

legend(1985,1.1, c("85/15 gap","85/50 gap", "50/15 gap"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green")) # gives the legend lines the correct color and widt

dev.off()

#Table
temptab <- xtable(gaps, caption="Quantiles Males age 25-55 baseyear 85")
print(temptab, include.rownames=FALSE, file="Tables/QuantMales85age2555.tex")

xltable <- gaps
WriteXLS("xltable", ExcelFileName="Tables/QuantMales85.xls", row.names=TRUE)


#Plot 85/15, 85/50, 50/15 non-reweighted quantile gaps for males only 

setEPS()
postscript("Graphs/Gapsmales.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gapsnorw[,"eight50"]~gapsnorw$years, xlim=c(1985,2011),ylim=c(0.00,1.10), xaxt = "n",  main="Quantile gaps males age 25-55 only", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="red")
axis(side=1, at=gapsnorw$years, labels=gapsnorw$years, cex.axis=0.9)
lines(gapsnorw[,"eight15"]~gapsnorw$years, type="o", col="blue", lwd=2)
lines(gapsnorw[,"five15"]~gapsnorw$years, type="o", col="green", lwd=2)

legend(1985,1.1, c("85/15 gap","85/50 gap", "50/15 gap"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green")) # gives the legend lines the correct color and widt

dev.off()

#Table
temptab <- xtable(gapsnorw, caption="Quantiles Males age 25-55 baseyear 85")
print(temptab, include.rownames=FALSE, file="Tables/QuantMalesage2555.tex")

xltable <- gapsnorw
WriteXLS("xltable", ExcelFileName="Tables/QuantMalesage2555.xls", row.names=TRUE)



#Plot both counterfactual and real wage gaps over time (males only)

setEPS()
postscript("Graphs/CombinedMales85age2555.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gaps[,"eight50"]~gaps$years, xlim=c(1985,2011),ylim=c(0.00,1.10), xaxt = "n",  main="Quantile gaps males age 25-55, overlaid with baseyear 85", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="hotpink2")
axis(side=1, at=gaps$years, labels=gaps$years, cex.axis=0.9)
lines(gaps[,"eight15"]~gaps$years, type="o", col="cornflowerblue", lwd=2)
lines(gaps[,"five15"]~gaps$years, type="o", col="greenyellow", lwd=2)

lines(gapsnorw[,"eight50"]~gapsnorw$years, type="o", col="red", lwd=2)
lines(gapsnorw[,"eight15"]~gapsnorw$years, type="o", col="blue", lwd=2)
lines(gapsnorw[,"five15"]~gapsnorw$years, type="o", col="green", lwd=2)


legend(1985,1.1, c("85/15 gap","85/50 gap", "50/15 gap","85/15 gap baseyear 85","85/50 gap baseyear 85", "50/15 gap baseyear 85"), 
lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green","cornflowerblue","hotpink2","greenyellow" )) # gives the legend lines the correct color and widt

dev.off()



#FEMALES

#Plot 85/15, 85/50, 50/15 counterfactual quantile gaps for females only 

setEPS()
postscript("Graphs/Gapsfemales85age2555.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gaps.f[,"eight50"]~gaps.f$years, xlim=c(1985,2011),ylim=c(0.1,1.0), xaxt = "n",  main="Quantile gaps females age 25-55 only, baseyear 1985", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="red")
axis(side=1, at=years, labels=years, cex.axis=0.9)
lines(gaps.f[,"eight15"]~gaps.f$years, type="o", col="blue", lwd=2)
lines(gaps.f[,"five15"]~gaps.f$years, type="o", col="green", lwd=2)

legend(1986,1.0, c("85/15 gap","85/50 gap", "50/15 gap"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green")) # gives the legend lines the correct color and widt
dev.off()

temptab <- xtable(gaps.f, caption="Quantiles Females age 25-55 baseyear 85")
print(temptab, include.rownames=FALSE, file="Tables/QuantFemales85age2555.tex")

xltable <- gaps.f
WriteXLS("xltable", ExcelFileName="Tables/QuantFemales85age2555.xls", row.names=TRUE)


#Plot 85/15, 85/50, 50/15 non-reweighted quantile gaps for females only 

setEPS()
postscript("Graphs/Gapsfemalesage2555.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gapsnorw.f[,"eight50"]~gapsnorw.f$years, xlim=c(1985,2011),ylim=c(0.00,1.10), xaxt = "n",  main="Quantile gaps females age 25-55 only", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="red")
axis(side=1, at=gapsnorw.f$years, labels=gapsnorw.f$years, cex.axis=0.9)
lines(gapsnorw.f[,"eight15"]~gapsnorw.f$years, type="o", col="blue", lwd=2)
lines(gapsnorw.f[,"five15"]~gapsnorw.f$years, type="o", col="green", lwd=2)

legend(1985,1.1, c("85/15 gap","85/50 gap", "50/15 gap"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green")) # gives the legend lines the correct color and widt

dev.off()

#Table
temptab <- xtable(gapsnorw.f[,], caption="Quantiles Females age 25-55 baseyear 85")
print(temptab, include.rownames=FALSE, file="Tables/QuantFemalesage2555.tex")

xltable <- gapsnorw.f
WriteXLS("xltable", ExcelFileName="Tables/QuantFemalesage2555.xls", row.names=TRUE)


#lot both counterfactual and real wage gaps over time (females only)

setEPS()
postscript("Graphs/CombinedFemales85age2555.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gaps.f[,"eight50"]~gaps.f$years, xlim=c(1985,2011),ylim=c(0.2,1.2), xaxt = "n",  main="Quantile gaps females age 25-55, overlaid with baseyear 85", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="hotpink2")
axis(side=1, at=gaps.f$years, labels=gaps.f$years, cex.axis=0.9)
lines(gaps.f[,"eight15"]~gaps.f$years, type="o", col="cornflowerblue", lwd=2)
lines(gaps.f[,"five15"]~gaps.f$years, type="o", col="greenyellow", lwd=2)

lines(gapsnorw.f[,"eight50"]~gapsnorw.f$years, type="o", col="red", lwd=2)
lines(gapsnorw.f[,"eight15"]~gapsnorw.f$years, type="o", col="blue", lwd=2)
lines(gapsnorw.f[,"five15"]~gapsnorw.f$years, type="o", col="green", lwd=2)


legend(1985,1.2, c("85/15 gap","85/50 gap", "50/15 gap","85/15 gap baseyear 85","85/50 gap baseyear 85", "50/15 gap baseyear 85"), 
lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green","cornflowerblue","hotpink2","greenyellow" )) # gives the legend lines the correct color and widt

dev.off()


