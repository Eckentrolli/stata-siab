options(scipen=30)

library(foreign)
library(Hmisc)
library(WriteXLS)
library(xtable)
library(plotrix)


setwd("/u/home/jd1033/DFG-descriptives/Results/FT")
#setwd("T:/")


load("reweighting/Resid85.RData")

rm(bindresf, bindresm)

#trim.m.rw <- trim.m.rw[!names(trim.m.rw) %in% c("values.base85", "sweight.1 ","countmf", "rweight.1") ]

setwd("/u/home/jd1033/DFG-descriptives/Results/FT")

#Histogram year 2010 real and counterfactual

hist.norw <- weighted.hist(trim.m.rw[trim.m.rw$year == 2010,]$discount, trim.m.rw[trim.m.rw$year == 2010,]$sweight, breaks=100, plot=FALSE)
#weighted.hist(trim.m.rw[trim.m.rw$year == 1985,]$discount, trim.m.rw[trim.m.rw$year == 1985,]$sweight, breaks=100)

lo <- loess(hist.norw$density ~ hist.norw$mids,surface="direct", span=0.1)
pred <- predict(lo, data.frame(hist.norw$mids))

hist.85 <- weighted.hist(trim.m.rw[trim.m.rw$year == 2010,]$discount, trim.m.rw[trim.m.rw$year == 2010,]$rweight, breaks=100, plot=FALSE)

lo2 <- loess(hist.85$density ~ hist.85$mids,surface="direct", span=0.1)
pred2 <- predict(lo2, data.frame(hist.85$mids))


setEPS()
postscript("Graphs/Counterfac_density2010.eps", width=8, heigh=7)

plot(hist.norw$mids, pred, type="l", col="black", lwd=2, ylim=c(0,0.055),xlab='log real wage', ylab="Density", main="Histogram 2010 (black), Counterfactual (grey)")
polygon(hist.norw$mids, pred, density=10, angle=45, border="black")

polygon(hist.85$mids, pred2, density=10, angle=135,col="grey", border="darkgrey", lwd=2) 

dev.off()


#Histogram year 2000 real and counterfactual

hist.norw <- weighted.hist(trim.m.rw[trim.m.rw$year == 2000,]$discount, trim.m.rw[trim.m.rw$year == 2000,]$sweight, breaks=100, plot=FALSE)


lo <- loess(hist.norw$density ~ hist.norw$mids,surface="direct", span=0.1)
pred <- predict(lo, data.frame(hist.norw$mids))

hist.85 <- weighted.hist(trim.m.rw[trim.m.rw$year == 2000,]$discount, trim.m.rw[trim.m.rw$year == 2000,]$rweight, breaks=100, plot=FALSE)

lo2 <- loess(hist.85$density ~ hist.85$mids,surface="direct", span=0.1)
pred2 <- predict(lo2, data.frame(hist.85$mids))


setEPS()
postscript("Graphs/Counterfac_density2000.eps", width=8, heigh=7)

plot(hist.norw$mids, pred, type="l", col="black", lwd=2, ylim=c(0,0.06),xlab='log real wage', ylab="Density", main="Histogram 2000 (black), Counterfactual (grey)")
polygon(hist.norw$mids, pred, density=10, angle=45, border="black")

polygon(hist.85$mids, pred2, density=10, angle=135,col="grey", border="darkgrey", lwd=2) 

dev.off()