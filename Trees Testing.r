###############################
######Trees###################
#############################

library(foreign)
library(Hmisc)
library(WriteXLS)
library(xtable)
library(party)
library(rpart)
library(plyr)

#setwd("E:/DATA")
setwd("/u/home/jd1033/DFG-descriptives/Results/FT")

#load("E:/DATA/Residuals.RData")
load("/u/home/jd1033/DFG-descriptives/ResidualsSmallFT.RData")

#trim <- trim[sample(nrow(trim), 10000), ] 

#results.bak <- results
rm(results, resultsall)

trim$bland <- as.factor(trim$bland)
trim$bland <- C(trim$bland, contr.treatment, base=10)

#male and female subsamples 
trim.m <- subset(trim, frau==0 & age >= 18 & age <=62)
trim.f <- subset(trim, frau==1 & age >= 18 & age <=62)

#rm(trim)


