options(scipen=30)

library(foreign)
library(Hmisc)
library(parallel)
library(xtable)
library(WriteXLS)
library(car)

#setwd("/u/home/jd1033/DFG-descriptives")
#load("/u/home/jd1033/DFG-descriptives/siab_r_7510_v1_forResiduals.RData")
setwd("E:/Data")
data <- read.dta("siab_r_7510_v1_testdataCrisis.dta", convert.factors=FALSE)

data <- subset(data, data$year>=2005)

attach(data)

data$bild <- as.factor(IP2A)
data$deutsch <- as.factor(deutsch)
data$beruf_gr <- as.factor(beruf_gr)

data$potexp2 <- potexp*potexp
data$potexp3 <- potexp*potexp*potexp

data$bildcat <- recode(data$IP2A, "c(-9, 1)= 0; 2=1; c(3,4)=2; c(5,6)= 3")
detach(data)

trim <- subset(data, data$age >=18 & age <=62 )
rm(data)

males <- subset(trim, frau==0 & age >= 18 & age <=62)
females <- subset(trim, frau==1 & age >= 18 & age <=62)

rm(trim)