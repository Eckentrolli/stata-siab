capture log close
log using IP2A.log, replace
*insert directory if necessary

/***********************************************************************************
This program creates the improved education variable IP2A used in the paper
"Imputation Rules to Improve the Education Variable in the IAB Employment Supsample"
 by Bernd Fitzenberger, Aderonke Osikominu and Robert Voelter. The data is the IABS
 regional file 1975-1997.
(c) 2005 Bernd Fitzenberger, Aderonke Osikominu and Robert Voelter
Goethe-Universitaet Frankfurt
************************************************************************************/

/*
This procedure generates the improved education variable IP2A. The basic
rule is to impute a spell with the highest degree RELIABLY reported for this
person before this spell if information at this spell is missing or a lower
degree is reported. Usually educational information is seen as reliably reported
if it is reported at least three times for this person.

program structure

Section 1
Only employment spells containing reliable education information are accepted as
basis for the extrapolation rule. Spells are reliable if they contain education
information which is reported at least three times for this person. Exceptions are
made for persons with 4 or fewer employment spells. Information from not accepted
spells will be treated as missing. At a later stage information will be extrapolated
to these spells. Special acceptance rules for young persons.

Only for persons with all education information missing the program imputes a
vocational training degree if certain employment statuses are reported.

Section 2
The extrapolation rule extrapolates degrees from the accepted spells to later spells if
there is missing information or a lower degree until there is an accepted spell with a
higher degree.

Only if a person's first spells have missing information the program extrapolates
backwards educational information from the persons first accepted spell with non
missing information to the previous spells with missing information.

Section 3
Ensure the same education information at parallel spells.
*/

version 8.0
set more off
set rmsg on
clear




use "E:\Data\siab_r_7510_v1_testdata.dta"



/* note:
 persnr is variable PERSNR
  */


replace erwstat_gr = 26 if erwstat_gr >=.

recode bildorig (21 = 1) (22 23 24 = 2) (25 26 = 5) (27=6)
 
gen AJAHR = year(begepi)

//gen int ADAT=mdy(AMONAT,ATAG,1900+AJAHR)
//drop AMONAT ATAG

*SPELL2 is inverse counter of spell
gen int SPELL2=(-1)*spell


/*data preparation*/
*generate the new education variable
generate byte IP2A=0



sort persnr spell


*one value (-9) for missing education
by persnr (spell), sort: replace IP2A= -9 if bildorig[_n]>=.

/* +++++++++++
   section 1
   acceptance of spells
   +++++++++++*/

/*special rules for young persons*/
*if age below 18 any formal education (2 to 6) is implausible
*impute "no formal education" (1)
by persnr (spell), sort: replace IP2A= 1 if age[_n]<18

*accept no vocational trainining (1,3) for young persons in vocational education
by persnr (spell), sort: replace IP2A= 1 if age[_n]<23 & bildorig[_n]==1 & stib[_n]==0
by persnr (spell), sort: replace IP2A= 3 if age[_n]>=21 & age[_n]<23 & bildorig[_n]==3 & stib[_n]==0
tabulate IP2A

/*
Accept education information which is reported three times or more
for a person, count only employment spells.
Three types of spells:
1) employment spells TYP~=6, BTYP==1
2) technical spells TYP~=6, BTYP~=1
3) UI benefit spells TYP==6
*/

*define minimum frequency per person for valid information
gen byte minimum=3
*lower minimum for persons with only a few employment spells
*count number of employment spells
by persnr: egen int NNONLED=sum(LF==1)

by persnr: replace minimum=2 if NNONLED==4
by persnr: replace minimum=1 if NNONLED==1 | NNONLED==2 | NNONLED==3

*count number of employment spells per person and specific education
by persnr bildorig (spell), sort: egen BLDN=sum(LF==1)

/*Define valid education information:
i) number of persons spells with this specific information at least minimum frequency AND
ii) employment spell AND
iii) non missing education information 1<=bild<=6
*/

gen byte VALID=(BLDN>=minimum & LF==1 & 1<=bildorig & bildorig<=6)


/* accept all VALID information */
by persnr (spell), sort: replace IP2A=bildorig[_n] if VALID==1 & age[_n]>=18

/*
For persons with education information missing at all spells:
Impute "vocational tranining degree" if the employment status (stib=2,3)
 indicates a qualified job at the respective employment spells.*/

egen NMIS=sum(bildorig >=.), by(persnr)
generate DMIS= NMIS==spell

by persnr (spell), sort: replace IP2A=2 if stib[_n]>=2 & stib[_n]<=3 & LF[_n]==1 & DMIS[_n]==1 


/* +++++++++++++++++++++
   section 2
   extrapolation
   +++++++++++++++++++++

Part 1: extrapolation to following spells

Extrapolate education information to following spells with lower
education information or missing information. Note that persons who have
vocational training (2) and high school (3) have both, which
has to be reported as (4). Hence the extrapolation is done stepwise.
1) Extrapolate "no degree" to following missings
2) Extrapolate "vocational training degree, 2" to following missings and "no degree, 1"
3) Extrapolate "high school, 3" to following missings and "no degree, 1". Do
not extrapolate 3 to 2. 3 is not higher than 2. 2 and 3 mean having both, 4.
4) Impute "high school & vocational training degree, 4" if "high school, 3" is reported
and "vocational training degree, 2" has been reported before or vice versa. Extrapolate
"high school & vocational training degree, 4" to following missings and spells with lower
education.
5) Extrapolate "technical college, 5" to following missings and lower degrees
6) Extrapolate "university, 6" to following missings and lower degrees*/

sort persnr spell

by persnr: replace IP2A=1 if _n>1 & IP2A[_n-1]==1 & IP2A[_n]<1 /*only to missing values*/

by persnr: replace IP2A=2 if _n>1 & IP2A[_n-1]==2 & IP2A[_n]<2 /*only to missing values and "1"*/

by persnr: replace IP2A=3 if _n>1 & IP2A[_n-1]==3 & IP2A[_n]<2 /*only to missing values and "1"*/

by persnr: replace IP2A=4 if _n>1 & (IP2A[_n-1]==4 | (IP2A[_n-1]==2 & IP2A[_n]==3) | (IP2A[_n-1]==3 & IP2A[_n]==2)) & IP2A[_n]<4

by persnr: replace IP2A=5 if _n>1 & IP2A[_n-1]==5 & IP2A[_n]<5

by persnr: replace IP2A=6 if _n>1 & IP2A[_n-1]==6 & IP2A[_n]<6


/*

part 2: backwards extrapolation to previous spells

The extrapolation rule above has eliminated all missing values for a person after
the first spell with education information. But possibliy the
first spell(s) still have missing values. Backwards extrapolate information
from the first spell with non missing education information for a person to
previous spells which have no information yet. Do not extrapolate below education
specific age limits:
university: 29 years
technical college: 27 years
both vocational training and high school: 23 years
only high school: 21 years
only vocational training: 20 years
no education degree: no age limit*/

by persnr (SPELL2), sort: replace IP2A=6 if _n>1 & (IP2A[_n]==-9 | IP2A[_n]==0) & IP2A[_n-1]==6 & age[_n]>=29 

by persnr (SPELL2), sort: replace IP2A=5 if _n>1 & (IP2A[_n]==-9 | IP2A[_n]==0) & IP2A[_n-1]==5 & age[_n]>=27 

by persnr (SPELL2), sort: replace IP2A=4 if _n>1 & (IP2A[_n]==-9 | IP2A[_n]==0) & IP2A[_n-1]==4 & age[_n]>=23 

by persnr (SPELL2), sort: replace IP2A=3 if _n>1 & (IP2A[_n]==-9 | IP2A[_n]==0) & IP2A[_n-1]==3 & age[_n]>=21 

by persnr (SPELL2), sort: replace IP2A=2 if _n>1 & (IP2A[_n]==-9 | IP2A[_n]==0) & IP2A[_n-1]==2 & age[_n]>=20

by persnr (SPELL2), sort: replace IP2A=1 if _n>1 & (IP2A[_n]==-9 | IP2A[_n]==0) & IP2A[_n-1]==1




/* +++++++++++
   section 3
   +++++++++++
*/


/*
Require multiple spells (parallel spells at the same time, STYP=2) to have the
same education information.
Identification through the same beginning date.
Impute highest of the parallel education information.*/


sort persnr spell

*single missing value
replace IP2A=-9 if IP2A==0
tab IP2A

//save IP2A.dta, replace

log close
exit
