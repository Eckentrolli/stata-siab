capture log close
log using IP2B.log, replace
*insert directory if necessary

/***********************************************************************************
This program creates the imputed education variable IP2B used in the paper
"Imputation Rules to Improve the Education Variable in the IAB Employment Supsample"
 by Bernd Fitzenberger, Aderonke Osikominu and Robert Voelter. The data is the IABS
 regional file 1975-1997.
(c) 2005 Bernd Fitzenberger, Aderonke Osikominu and Robert Voelter
Goethe-Universitaet Frankfurt
************************************************************************************/

/*

This procedure generates the corrected education variable IP2B. The basic
rule is to impute a spell with the highest degree RELIABLY reported for this
person before this spell if information at this spell is missing or a lower
degree is reported. Usually educational information is seen as reliably reported
if it is reported at least three times for this person. Additionally all of a
person's education information is seen as reliable if there are no inconsistencies
in the person's sequence of education information. A person's sequence is inconsistent
if once a degree has been reported as the highest degree and later a lower degree has
been reported as the highest degree.


program structure

section 0
This section checks if a person's sequence of education information
is plausible. It is not if the sequence is falling (an educational
degree reported once is not reported later). It is also not plausible
if education information is increasing from 2 to 3 since the vocational
tranining degree is not consistently reported. The program allows for
missing values in the education information sequence. The section
results in a variable personok=1 if consistent sequence, =0 otherwise. The
variable personok is uniformly defined and constant for each person.

Section 1
Only employment spells containing reliable education information are accepted as
basis for the extrapolation rule. Spells are reliable if they contain education
information which is reported at least three times for this person. Exceptions are
made for persons with 4 or fewer employment spells. Additionally all employment spells
are reliable if the information sequence is consistent. Personok=1 indicates this.
Information from not accepted spells will be treated as missing. At a later stage
information will be extrapolated to these spells. Special acceptance rules for
young persons.

Only for persons with all education information missing the program imputes a
vocational training degree if certain employment statuses are reported.

Section 2
The extrapolation rule extrapolates degrees from the accepted spells to later spells if
there is missing information or a lower degree until there is an accepted spell with a
higher degree.

Only if a person's first spells have missing information the program extrapolates
backwards educational information from the persons first accepted spell with non
missing information to the previous spells with missing information.

Section 3
Ensure the same education information at parallel spells.
*/

version 8.0
set more off
set rmsg on
clear
set mem 1000m

/*******************
section 0
********************/


/* The definiton of the variable "personok" is done stepwise for
subsamples of the data to increase processing speed. The data is
split according to the total number of spells each person has, "NSPELL".
Subsamples:
i) 0<NSPELL<=10
ii) 10<NSPELL<=20
iii) ...
...
x) 90<NSPELL<=1170 which is the maximum of NSPELL in the data
*/

*initialize tempfile plausibel, which will contain PNR SPELL and the variable "personok"
tempfile plausibel
gen int PNR=0
save `plausibel'

forval i=0(10)90{
    if `i'<90{
        local i_upper=`i'+10
        }
    if `i'==90{
        local i_upper=1170
        /*this is the maximum of NSPELL in the sample IABS regional file 75-97*/
        }
    #delimit ;
    use PNR SPELL NSPELL BILD AMONAT ATAG AJAHR using
    reg7597 if NSPELL>`i' & NSPELL<=`i_upper';
    *reads in relevant subsample;
    *insert directory if necessary;
    #delimit cr

    tab NSPELL

    *ADAT is mdy(AMONAT,ATAG,1900+AJAHR), first day of a spell in stata format
    gen int ADAT=mdy(AMONAT,ATAG,1900+AJAHR)
    drop AMONAT ATAG AJAHR

    sort PNR SPELL
    replace BILD=-9 if BILD==7 | BILD==9

    /* Generate indicator variable "plausibel", with
    plausibel=0 if (a spell for the same person follows with a lower education
    or 3 follows after 2 for the same person)
    plausibel=1 otherwise.
    Thus  a case of plausibel=0 indicates an inconsistency for this person.

    The following loop checks this. The loop goes from 1 to the maximum of NSPELL
    in the subsample. This makes sure that we would even detect if the last SPELL
    of a person has lower education information than the first spell of this person.*/

    gen byte plausibel=1

    local j=1

    while `j' <=`i_upper' {

        #delimit ;
        *check if there is decreasing education;
        quietly replace plausibel=0 if BILD[_n+`j']<BILD[_n] & BILD[_n+`j']>0
                            & PNR[_n]==PNR[_n+`j'] & ADAT[_n+`j'] > ADAT[_n];
        *check if education 3 is reported after education 2 has been reported;
        quietly replace plausibel=0 if BILD[_n+`j']==3 & BILD[_n]==2
                    & PNR[_n]==PNR[_n+`j'] & ADAT[_n+`j'] > ADAT[_n];
        #delimit cr

        local j=`j'+1

        }

    /*Generate indicator variable "personok" uniformly defined for each person
    personok=1 all spells ok, eduation sequence consistent
    personok=0 at least one spell not ok, inconsistent sequence*/

    bys PNR: egen byte personok=min(plausibel)
    keep PNR SPELL plausibel personok

    *combine results for subsample with previous results and save
    append using `plausibel'
    sort PNR SPELL
    save `plausibel', replace
    }



/*******************
section 1
********************/


#delimit ;
use PNR SPELL NSPELL GEBJAHR AMONAT ATAG AJAHR TYP BTYP STIB BILD STYP
 using reg7597.dta , clear;
*insert directory if necessary;
#delimit cr

/* note:
 PNR is variable PERSNR
  */

*ADAT is mdy(AMONAT,ATAG,1900+AJAHR), first day of a spell in stata format
gen int ADAT=mdy(AMONAT,ATAG,1900+AJAHR)
drop AMONAT ATAG

*SPELL2 is inverse counter of SPELL
gen int SPELL2=(-1)*SPELL


/*data preparation*/
*merge with variable personok generated above
sort PNR SPELL
merge PNR SPELL using `plausibel'

*generate the new education variable
generate byte IP2B=0

*generate age for uncensored years of birth
gen ALTER=AJAHR-GEBJAHR if GEBJAHR<89
sort PNR SPELL
*impute likely age for censored years of birth
by PNR: replace ALTER=15+AJAHR[_n]-AJAHR[1] if GEBJAHR==90
by PNR: replace ALTER=63+AJAHR[_n]-AJAHR[_N] if GEBJAHR==95

*one value (-9) for missing education
by PNR (SPELL), sort: replace IP2B= -9 if BILD[_n]==7 | BILD[_n]==9

/* +++++++++++
   section 1
   acceptance of spells
   +++++++++++*/

/*special rules for young persons*/
*if age below 18 any formal education (2 to 6) is implausible
*impute "no formal education" (1)
by PNR (SPELL), sort: replace IP2B= 1 if ALTER[_n]<18

*accept no vocational trainining (1,3) for young persons in vocational education
by PNR (SPELL), sort: replace IP2B= 1 if ALTER[_n]<23 & BILD[_n]==1 & STIB[_n]==0
by PNR (SPELL), sort: replace IP2B= 3 if ALTER[_n]>=21 & ALTER[_n]<23 & BILD[_n]==3 & STIB[_n]==0
tabulate IP2B

/*
Accept education information which is
either 1) reported three times or more for a person
or 2) from persons with plausible education information sequences.

Count only employment spells.
Three types of spells:
1) employment spells TYP~=6, BTYP==1
2) technical spells TYP~=6, BTYP~=1
3) UI benefit spells TYP==6
*/

*Define minimum frequency per person for valid information
gen byte minimum=3
*lower minimum for persons with only a few employment spells
*count number of employment spells
by PNR: egen int NNONLED=sum(TYP~=6 & BTYP==1)

by PNR: replace minimum=2 if NNONLED==4
by PNR: replace minimum=1 if NNONLED==1 | NNONLED==2 | NNONLED==3

*count number of employment spells per person and specific education
by PNR BILD (SPELL), sort: egen BLDN=sum(TYP~=6 & BTYP==1)

/*Define valid education information for rule 1):
i) number of persons spells with this specific information at least minimum frequency AND
ii) employment spell AND
iii) non missing education information 1<=BILD<=6
*/

gen byte valid1=(BLDN>=minimum & TYP ~=6 & BTYP==1 & 1<=BILD & BILD<=6)

/*Define valid education information for rule 2):
i) person's education information sequence is plausible AND
ii) employment spell AND
iii) non missing education information 1<=BILD<=6
*/

gen byte valid2=(personok==1 & TYP ~=6 & BTYP==1 & 1<=BILD & BILD<=6)

#delimit ;
/* accept all valid information */
by PNR (SPELL), sort: replace IP2B=BILD[_n] if (valid1==1 | valid2==1)
                                            & ALTER[_n]>=18;

/*
For persons with education information missing at all spells:
Impute "vocational tranining degree" if the employment status (STIB=2,3)
 indicates a qualified job at the respective employment spells.*/

egen NMIS=sum(BILD==7 | BILD==9), by(PNR);
generate DMIS= NMIS==NSPELL;


by PNR (SPELL), sort: replace IP2B=2 if
                        2<=STIB[_n] & STIB[_n]<=3
                        & 1<=TYP[_n] & TYP[_n]<=5
                        & BTYP==1
                        & DMIS[_n]==1 ;

/* +++++++++++++++++++++
   section 2
   extrapolation
   +++++++++++++++++++++

Part 1: extrapolation to following spells

Extrapolate education information to following spells with lower
education information or missing information. Note that persons who have
vocational training (2) and high school (3) have both, which
has to be reported as (4). Hence the extrapolation is done stepwise.
1) Extrapolate "no degree" to following missings
2) Extrapolate "vocational training degree, 2" to following missings and "no degree, 1"
3) Extrapolate "high school, 3" to following missings and "no degree, 1". Do
not extrapolate 3 to 2. 3 is not higher than 2. 2 and 3 mean having both, 4.
4) Impute "high school & vocational training degree, 4" if "high school, 3" is reported
and "vocational training degree, 2" has been reported before or vice versa. Extrapolate
"high school & vocational training degree, 4" to following missings and spells with lower
education.
5) Extrapolate "technical college, 5" to following missings and lower degrees
6) Extrapolate "university, 6" to following missings and lower degrees*/

sort PNR SPELL;

by PNR: replace IP2B=1 if _n>1
                             & IP2B[_n-1]==1
                             & IP2B[_n]<1; /*only to missing values*/

by PNR: replace IP2B=2 if _n>1
                             & IP2B[_n-1]==2
                             & IP2B[_n]<2; /*only to missing values and "1"*/

by PNR: replace IP2B=3 if _n>1
                             & IP2B[_n-1]==3
                             & IP2B[_n]<2; /*only to missing values and "1"*/

by PNR: replace IP2B=4 if _n>1
                             & (IP2B[_n-1]==4 | (IP2B[_n-1]==2 & IP2B[_n]==3) | (IP2B[_n-1]==3 & IP2B[_n]==2))
                             & IP2B[_n]<4;

by PNR: replace IP2B=5 if _n>1
                             & IP2B[_n-1]==5
                             & IP2B[_n]<5;

by PNR: replace IP2B=6 if _n>1
                             & IP2B[_n-1]==6
                             & IP2B[_n]<6;


/*

part 2: backwards extrapolation to previous spells

The extrapolation rule above has eliminated all missing values for a person after
the first spell with education information. But possibliy the
first spell(s) still have missing values. Backwards extrapolate information
from the first spell with non missing education information for a person to
previous spells which have no information yet. Do not extrapolate below education
specific age limits:
university: 29 years
technical college: 27 years
both vocational training and high school: 23 years
only high school: 21 years
only vocational training: 20 years
no education degree: no age limit*/

by PNR (SPELL2), sort: replace IP2B=6 if
                         _n>1
                        & (IP2B[_n]==-9 | IP2B[_n]==0)
                        & IP2B[_n-1]==6
                        & ALTER[_n]>=29 ;

by PNR (SPELL2), sort: replace IP2B=5 if
                         _n>1
                        & (IP2B[_n]==-9 | IP2B[_n]==0)
                        & IP2B[_n-1]==5
                        & ALTER[_n]>=27 ;

by PNR (SPELL2), sort: replace IP2B=4 if
                        _n>1
                        & (IP2B[_n]==-9 | IP2B[_n]==0)
                        & IP2B[_n-1]==4
                        & ALTER[_n]>=23 ;

by PNR (SPELL2), sort: replace IP2B=3 if
                        _n>1
                        & (IP2B[_n]==-9 | IP2B[_n]==0)
                        & IP2B[_n-1]==3
                        & ALTER[_n]>=21 ;

by PNR (SPELL2), sort: replace IP2B=2 if
                        _n>1
                        & (IP2B[_n]==-9 | IP2B[_n]==0)
                        & IP2B[_n-1]==2
                        & ALTER[_n]>=20;

by PNR (SPELL2), sort: replace IP2B=1 if
                        _n>1
                        & (IP2B[_n]==-9 | IP2B[_n]==0)
                        & IP2B[_n-1]==1;


#delimit cr

/* +++++++++++
   section 3
   +++++++++++
*/

/*
Require multiple spells (parallel spells at the same time, STYP=2) to have the
same education information.
Identification through the same beginning date.
Impute highest of the parallel education information.*/

sort PNR ADAT SPELL
by PNR ADAT (SPELL): egen maxbild=max(IP2B)
by PNR ADAT (SPELL): replace IP2B=maxbild if IP2B~=maxbild & maxbild>0.5


sort PNR SPELL

*single missing value
replace IP2B=-9 if IP2B==0
tab IP2B
keep PNR SPELL IP2B
compress
save IP2B.dta, replace
*insert directory if necessary

log close
exit
