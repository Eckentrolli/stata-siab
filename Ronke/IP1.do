capture log close
log using IP1.log, replace
*insert directory if necessary

/***********************************************************************************
This program creates the improved education variable IP1 used in the paper
"Imputation Rules to Improve the Education Variable in the IAB Employment Supsample"
 by Bernd Fitzenberger, Aderonke Osikominu and Robert Voelter. The data is the IABS
 regional file 1975-1997.
(c) 2005 Bernd Fitzenberger, Aderonke Osikominu and Robert Voelter
Goethe-Universitaet Frankfurt
************************************************************************************/

/*

This procedure generates the improved education variable IP1. The basic
rule is to impute a spell with the highest degree reported for this person
before this spell if information at this spell is missing or a lower degree is reported.

program structure

Section 1
All employment spells are accepted as basis for the extrapolation rule. Information
from not accepted non employment spells will be treated as missing. At a later stage
information will be extrapolated to these spells. Young persons by assumption have no degree.

Only for persons with all education information missing the program imputes a
vocational training degree if certain employment statuses are reported.

Section 2
The extrapolation rule extrapolates degrees from the accepted spells to later spells if
there is missing information or a lower degree until there is an accepted spell with a
higher degree.

Only if a person's first spells have missing information the program extrapolates
backwards educational information from the persons first accepted spell with non
missing information to the previous spells with missing information.

Section 3
Ensure the same education information at parallel spells.
*/

version 8.0
set more off
set rmsg on
clear
set mem 1000m


#delimit ;
use PNR SPELL NSPELL GEBJAHR AMONAT ATAG AJAHR TYP BTYP STIB BILD STYP
 using reg7597.dta , clear;
*insert directory if necessary;
#delimit cr

/* note:
 PNR is variable PERSNR
  */

*ADAT is mdy(AMONAT,ATAG,1900+AJAHR), first day of a spell in stata format
gen int ADAT=mdy(AMONAT,ATAG,1900+AJAHR)
drop AMONAT ATAG

*SPELL2 is inverse counter of SPELL
gen int SPELL2=(-1)*SPELL

/*data preparation*/
*generate the new education variable
generate byte IP1=0

*generate age for uncensored years of birth
gen ALTER=AJAHR-GEBJAHR if GEBJAHR<89
sort PNR SPELL
*impute likely age for censored years of birth
by PNR: replace ALTER=15+AJAHR[_n]-AJAHR[1] if GEBJAHR==90
by PNR: replace ALTER=63+AJAHR[_n]-AJAHR[_N] if GEBJAHR==95

*one value (-9) for missing education
by PNR (SPELL), sort: replace IP1= -9 if BILD[_n]==7 | BILD[_n]==9

/* +++++++++++
   section 1
   acceptance of spells
   +++++++++++

special rules for young persons*/
*if age below 18 any formal education (2 to 6) is implausible
*impute "no formal education" (1)
by PNR (SPELL), sort: replace IP1= 1 if ALTER[_n]<18

tabulate IP1

/*
Accept only education information from employment spells.
Three types of spells:
1) employment spells TYP~=6, BTYP==1
2) technical spells TYP~=6, BTYP~=1
3) UI benefit spells TYP==6

Define valid education information:
i) employment spell AND
ii) non missing education information 1<=BILD<=6
*/

gen byte valid=(TYP ~=6 & BTYP==1 & 1<=BILD & BILD<=6)

#delimit ;
/* accept all valid information */
by PNR (SPELL), sort: replace IP1=BILD[_n] if valid==1
                                            & ALTER[_n]>=18;

/*
For persons with education information missing at all spells:
Impute "vocational tranining degree" if the employment status (STIB=2,3)
 indicates a qualified job at the respective employment spells.*/

egen NMIS=sum(BILD==7 | BILD==9), by(PNR);
generate DMIS= NMIS==NSPELL;


by PNR (SPELL), sort: replace IP1=2 if
                        2<=STIB[_n] & STIB[_n]<=3
                        & 1<=TYP[_n] & TYP[_n]<=5
                        & BTYP==1
                        & DMIS[_n]==1 ;


/* +++++++++++++++++++++
   section 2
   extrapolation
   +++++++++++++++++++++

Part 1: extrapolation to following spells

Extrapolate education information to following spells with lower
education information or missing information. Note that persons who have
vocational training (2) and high school (3) have both, which
has to be reported as (4). Hence the extrapolation is done stepwise.
1) Extrapolate "no degree" to following missings
2) Extrapolate "vocational training degree, 2" to following missings and "no degree, 1"
3) Extrapolate "high school, 3" to following missings and "no degree, 1". Do
not extrapolate 3 to 2. 3 is not higher than 2. 2 and 3 mean having both, 4.
4) Impute "high school & vocational training degree, 4" if "high school, 3" is reported
and "vocational training degree, 2" has been reported before or vice versa. Extrapolate
"high school & vocational training degree, 4" to following missings and spells with lower
education.
5) Extrapolate "technical college, 5" to following missings and lower degrees
6) Extrapolate "university, 6" to following missings and lower degrees*/

sort PNR SPELL;

by PNR: replace IP1=1 if _n>1
                             & IP1[_n-1]==1
                             & IP1[_n]<1; /*only to missing values*/

by PNR: replace IP1=2 if _n>1
                             & IP1[_n-1]==2
                             & IP1[_n]<2; /*only to missing values and "1"*/

by PNR: replace IP1=3 if _n>1
                             & IP1[_n-1]==3
                             & IP1[_n]<2; /*only to missing values and "1"*/

by PNR: replace IP1=4 if _n>1
                             & (IP1[_n-1]==4 | (IP1[_n-1]==2 & IP1[_n]==3) | (IP1[_n-1]==3 & IP1[_n]==2))
                             & IP1[_n]<4;

by PNR: replace IP1=5 if _n>1
                             & IP1[_n-1]==5
                             & IP1[_n]<5;

by PNR: replace IP1=6 if _n>1
                             & IP1[_n-1]==6
                             & IP1[_n]<6;


/*

part 2: backwards extrapolation to previous spells

The extrapolation rule above has eliminated all missing values for a person after
the first spell with education information. But possibliy the
first spell(s) still have missing values. Backwards extrapolate information
from the first spell with non missing education information for a person to
previous spells which have no information yet. Do not extrapolate below education
specific age limits:
university: 29 years
technical college: 27 years
both vocational training and high school: 23 years
only high school: 21 years
only vocational training: 20 years
no education degree: no age limit*/

by PNR (SPELL2), sort: replace IP1=6 if
                         _n>1
                        & (IP1[_n]==-9 | IP1[_n]==0)
                        & IP1[_n-1]==6
                        & ALTER[_n]>=29 ;

by PNR (SPELL2), sort: replace IP1=5 if
                         _n>1
                        & (IP1[_n]==-9 | IP1[_n]==0)
                        & IP1[_n-1]==5
                        & ALTER[_n]>=27 ;

by PNR (SPELL2), sort: replace IP1=4 if
                        _n>1
                        & (IP1[_n]==-9 | IP1[_n]==0)
                        & IP1[_n-1]==4
                        & ALTER[_n]>=23 ;

by PNR (SPELL2), sort: replace IP1=3 if
                        _n>1
                        & (IP1[_n]==-9 | IP1[_n]==0)
                        & IP1[_n-1]==3
                        & ALTER[_n]>=21 ;

by PNR (SPELL2), sort: replace IP1=2 if
                        _n>1
                        & (IP1[_n]==-9 | IP1[_n]==0)
                        & IP1[_n-1]==2
                        & ALTER[_n]>=20;

by PNR (SPELL2), sort: replace IP1=1 if
                        _n>1
                        & (IP1[_n]==-9 | IP1[_n]==0)
                        & IP1[_n-1]==1;


#delimit cr

/* +++++++++++
   section 3
   +++++++++++
*/

/*
Require multiple spells (parallel spells at the same time, STYP=2) to have the
same education information.
Identification through the same beginning date.
Impute highest of the parallel education information.*/

sort PNR ADAT SPELL
by PNR ADAT (SPELL): egen maxbild=max(IP1)
by PNR ADAT (SPELL): replace IP1=maxbild if IP1~=maxbild & maxbild>0.5


sort PNR SPELL

*single missing value
replace IP1=-9 if IP1==0
tab IP1
keep PNR SPELL IP1
compress
save IP1.dta, replace
*insert directory if necessary
log close
exit
