capture log close
log using IP3.log, replace
*insert directory if necessary

/***********************************************************************************
This program creates the improved education variable IP3 used in the paper
"Imputation Rules to Improve the Education Variable in the IAB Employment Supsample"
 by Bernd Fitzenberger, Aderonke Osikominu and Robert Voelter. The data is the IABS
 regional file 1975-1997.
(c) 2005 Bernd Fitzenberger, Aderonke Osikominu and Robert Voelter
Goethe-Universitaet Frankfurt
************************************************************************************/

/*
This procedure generates the improved education variable IP3. The basic
rule is to impute a spell with the highest degree RELIABLY reported for this person
before this spell if information at this spell is missing or a lower degree is reported.
A spell is seen as reliable reported if it is reported by an employer who changed the
information he reported about an employee not more often than once. We
allow for employer to correct the education information they report about an amployee.
If an employer reports a specific education information about an employee only once
and immediately afterwards reports the same information as before we assume this is
a self correction. We don't count it towards the number of changes. If a reliable
employer first reports a higher degree for an employee and later a lower one we assume
this is a self correction. We assume the employer wanted to report the lower education
all the time.

program structure

Section 1
In contrast to the other IPx programs this program works only with the first education
information an employer reports about his employee and all reports where the employer
changes the education information he reports.
Preliminary accept the employment spells which are the first employment spell an employer
reports about an employee and the spells where the employer reports a different education
information than he reported before. Correct one time exceptions in education reports.
Correct the reported education if a reliable employer first reported a certain education
information and later reported a lower one. Information from not accepted spells will be
treated as missing. At a later stage
information will be extrapolated to these spells. Young persons by assumption have no degree.

Only for persons with all education information missing the program imputes a
vocational training degree if certain employment statuses are reported.

Section 2
The extrapolation rule extrapolates degrees from the accepted spells to later spells if
there is missing information or a lower degree until there is an accepted spell with a
higher degree.

Only if a person's first spells have missing information the program extrapolates
backwards educational information from the persons first accepted spell with non
missing information to the previous spells with missing information.

Section 3
Ensure the same education information at parallel spells.
*/

version 8.0
set more off
set rmsg on
clear
set mem 1000m


#delimit ;
use PNR SPELL NSPELL GEBJAHR AMONAT ATAG AJAHR TYP BTYP STIB BILD STYP BNN
 using reg7597.dta , clear;
*insert directory if necessary;
#delimit cr

/* note:
 PNR is variable PERSNR
  */

*ADAT is mdy(AMONAT,ATAG,1900+AJAHR), first day of a spell in stata format
gen int ADAT=mdy(AMONAT,ATAG,1900+AJAHR)
drop AMONAT ATAG

*SPELL2 is inverse counter of SPELL
gen int SPELL2=(-1)*SPELL

/*data preparation*/
*generate the new education variable
generate byte IP3=0

*generate age for uncensored years of birth
gen ALTER=AJAHR-GEBJAHR if GEBJAHR<89
sort PNR SPELL
*impute likely age for censored years of birth
by PNR: replace ALTER=15+AJAHR[_n]-AJAHR[1] if GEBJAHR==90
by PNR: replace ALTER=63+AJAHR[_n]-AJAHR[_N] if GEBJAHR==95

*one value (-9) for missing education
by PNR (SPELL), sort: replace IP3= -9 if BILD[_n]==7 | BILD[_n]==9

/*+++++++++++++++
  section 1
  acceptance of spells
  and corrections based on self correction of employers
  +++++++++++++++*/


*dummy for non employment spells and spells with age below 18
*these spells are not used to construct the IP variable
generate byte DLED= TYP==6 | (TYP~=6 & BTYP~=1) | ALTER<18

*dummy for missing education information
generate byte DBMIS= BILD==7 | BILD==9

/*preliminary accept every first non missing education information an employer
reports for an employee*/
#delimit ;
by DLED DBMIS PNR BNN (SPELL), sort: replace IP3=BILD[_n] if
                    DLED[_n]==0 & DBMIS[_n]==0
                    & 1<=BILD[_n] & BILD[_n]<=6
                    & ALTER[_n]>=18
                    & _n==1 ;

/* preliminary accept every non missing education information which differs
from the previous non missing employment information the same
employer made for this employee*/
by DLED DBMIS PNR BNN (SPELL), sort: replace IP3=BILD[_n] if
                    DLED[_n]==0 & DBMIS[_n]==0
                    & 1<=BILD[_n] & BILD[_n]<=6
                    & BILD[_n-1]~=BILD[_n]
                    & ALTER[_n]>=18
                    & _n>1 ;

*drop DBMIS ;

/* dummy which indicates a non missing education information
for an employment spell*/
by DLED PNR BNN (SPELL), sort: generate byte DBLDH1= 1<=IP3[_n]
                                        & IP3[_n]<=6 & DLED==0 ;

/* dummy which indicates the change of education information reported by
an employer about an employee */
by DBLDH1 PNR BNN (SPELL), sort: generate DBLDH2= _n>1
                                                & IP3[_n]~=IP3[_n-1]
                                                & DBLDH1[_n]==1 ;
#delimit cr

/* the variable BLDN1 counts how often an employer reports each of the six different education
information (1,2,...,6) for an employee */
by DLED PNR BNN BILD (SPELL), sort: generate BLDN1=_n if 1<=BILD[_n] & BILD[_n]<=6 & DLED[_n]==0
by DLED PNR BNN BILD (SPELL), sort: replace BLDN1=BLDN1[_N] if BLDN1[_N]~=. & DLED[_n]==0
by PNR BNN BILD (SPELL), sort: replace BLDN1=0 if BLDN1[_n]==. /* missing: non employment spell or BILD==7,9 */

*tabulate BLDN1 , missing
gen IP3old=IP3

/* One-time deviations in the education information an employer reports about
his employee are immediately corrected. One time deviations are defined by the employer
 reporting the deviating education information only once (BLDN=1) and reporting the
 same education information immediately after the deviation as reported before the deviation.*/
#delimit ;
by DBLDH1 PNR BNN (SPELL), sort: replace IP3=IP3[_n-1] if _N>=3 & _n<_N
                                    & BLDN1[_n-1]>=2 & BLDN1[_n]==1
                                    & IP3[_n-1]==IP3[_n+1]
                                    & DBLDH2[_n]==1 /* ensures _n>1 & DBLDH1==1 */ ;
# delimit cr

tab IP3old IP3
drop IP3old

/* The variable BLDN2 counts how often an employer changes the reported education for
an employee. The first report also counts as a change. Hence the number of true changes
is BLDN2-1. Missings and already corrected one time exceptions do not count.
Example: assume the following sequence in the IAB data "1-missing-1-5-1-2-2-2-1-1" reported
by an employer for an employee. It would give BLDN2=3 since the missing (DBLDH1=0) and the
one time exception 5 (already corrected above) do not count, only the first report of 1, the
change to 2 and the change back to 1 count. */
#delimit ;
by DBLDH1 PNR BNN (SPELL), sort: generate byte DBLDH3= (DBLDH1[_n]==1
                        & _n==1) | (DBLDH1[_n-1]==1 & IP3[_n-1]~=IP3[_n]  & _n>1) ;
#delimit cr
by DLED PNR BNN (SPELL), sort: egen byte BLDN2= sum(DBLDH3) if DLED[_n]==0
by PNR (SPELL), sort: replace BLDN2=-1 if DLED[_n]==1

tabulate BLDN2 , missing

drop DBLDH3

#delimit ;
/* Correct the inconsistent reports of reliable employers (only one change, BLDN2=2).
If reliable employers  first report a degree and later report a lower degree for this
employee this is inconsistent. We assume they correct themselve. Impute the wrongly
reported first spells  with the later reported correct information.*/

/* Replace inconsistent sequence 2,3 with 3,3 since 2,3 is inconsistent and hence
this is a correction.*/
capture drop IP3old;
gen IP3old=IP3;
by DBLDH1 PNR BNN (SPELL2), sort: replace IP3=3 if
                        _n>1
                        & IP3[_N]==2 & IP3[_n-1]==3
                        & BLDN2[_n]==2
                        & DBLDH1==1 ;

tab IP3old IP3;
drop IP3old;

gen IP3old=IP3;
/*Replace decreasing sequence (example 2,1) with flat sequence (example 1,1)
since every decreasing sequence is inconsistent.*/
by DBLDH1 PNR BNN (SPELL2), sort: replace IP3=IP3[_n-1] if
                        _n>1
                        & IP3[_n]>IP3[_n-1]
                        & BLDN2[_n]==2
                        & IP3[_n-1]>=1 & IP3[_n-1]<=5
                        & DBLDH1==1 ;
tab IP3old IP3;
drop IP3old;
# delimit;

/*Do not accept education information from unreliable employers, who
 more than once changed the reported education information for an employee
(without ont-time exceptions), indicated by BLDN2>2.*/
by PNR BNN (SPELL), sort: replace IP3=-9 if BLDN2[_n]>2;


/* step
special rules for young persons*/
*if age below 18 any formal education (2 to 6) is implausible
by PNR (SPELL), sort: replace IP3= 1 if ALTER[_n]<18;

/*
For persons with education information missing at all spells:
Impute "vocational tranining degree" if the employment status (STIB=2,3)
 indicates a qualified job at the respective employment spells.*/

egen NMIS=sum(BILD==7 | BILD==9), by(PNR);
generate DMIS= NMIS==NSPELL;

by PNR (SPELL), sort: replace IP3=2 if
                        2<=STIB[_n] & STIB[_n]<=3
                        & 1<=TYP[_n] & TYP[_n]<=5
                        & BTYP==1
                        & DMIS[_n]==1 ;

/* +++++++++++++++++++++
   section 2
   extrapolation
   +++++++++++++++++++++

Part 1: extrapolation to following spells

Extrapolate education information to following spells with lower
education information or missing information. Note that persons who have
vocational training (2) and high school (3) have both, which
has to be reported as (4). Hence the extrapolation is done stepwise.
1) Extrapolate "no degree" to following missings
2) Extrapolate "vocational training degree, 2" to following missings and "no degree, 1"
3) Extrapolate "high school, 3" to following missings and "no degree, 1". Do
not extrapolate 3 to 2. 3 is not higher than 2. 2 and 3 mean having both, 4.
4) Impute "high school & vocational training degree, 4" if "high school, 3" is reported
and "vocational training degree, 2" has been reported before or vice versa. Extrapolate
"high school & vocational training degree, 4" to following missings and spells with lower
education.
5) Extrapolate "technical college, 5" to following missings and lower degrees
6) Extrapolate "university, 6" to following missings and lower degrees*/

sort PNR SPELL;

by PNR: replace IP3=1 if _n>1
                             & IP3[_n-1]==1
                             & IP3[_n]<1; /*only to missing values*/

by PNR: replace IP3=2 if _n>1
                             & IP3[_n-1]==2
                             & IP3[_n]<2; /*only to missing values and "1"*/

by PNR: replace IP3=3 if _n>1
                             & IP3[_n-1]==3
                             & IP3[_n]<2; /*only to missing values and "1"*/

by PNR: replace IP3=4 if _n>1
                             & (IP3[_n-1]==4 | (IP3[_n-1]==2 & IP3[_n]==3) | (IP3[_n-1]==3 & IP3[_n]==2))
                             & IP3[_n]<4;

by PNR: replace IP3=5 if _n>1
                             & IP3[_n-1]==5
                             & IP3[_n]<5;

by PNR: replace IP3=6 if _n>1
                             & IP3[_n-1]==6
                             & IP3[_n]<6;


/*

part 2: backwards extrapolation to previous spells

The extrapolation rule above has eliminated all missing values for a person after
the first spell with education information. But possibliy the
first spell(s) still have missing values. Backwards extrapolate information
from the first spell with non missing education information for a person to
previous spells which have no information yet. Do not extrapolate below education
specific age limits:
university: 29 years
technical college: 27 years
both vocational training and high school: 23 years
only high school: 21 years
only vocational training: 20 years
no education degree: no age limit*/

by PNR (SPELL2), sort: replace IP3=6 if
                         _n>1
                        & (IP3[_n]==-9 | IP3[_n]==0)
                        & IP3[_n-1]==6
                        & ALTER[_n]>=29 ;

by PNR (SPELL2), sort: replace IP3=5 if
                         _n>1
                        & (IP3[_n]==-9 | IP3[_n]==0)
                        & IP3[_n-1]==5
                        & ALTER[_n]>=27 ;

by PNR (SPELL2), sort: replace IP3=4 if
                        _n>1
                        & (IP3[_n]==-9 | IP3[_n]==0)
                        & IP3[_n-1]==4
                        & ALTER[_n]>=23 ;

by PNR (SPELL2), sort: replace IP3=3 if
                        _n>1
                        & (IP3[_n]==-9 | IP3[_n]==0)
                        & IP3[_n-1]==3
                        & ALTER[_n]>=21 ;

by PNR (SPELL2), sort: replace IP3=2 if
                        _n>1
                        & (IP3[_n]==-9 | IP3[_n]==0)
                        & IP3[_n-1]==2
                        & ALTER[_n]>=20;

by PNR (SPELL2), sort: replace IP3=1 if
                        _n>1
                        & (IP3[_n]==-9 | IP3[_n]==0)
                        & IP3[_n-1]==1;


#delimit cr

/* +++++++++++
   section 3
   +++++++++++
*/

/*
Require multiple spells (parallel spells at the same time, STYP=2) to have the
same education information.
Identification through the same beginning date.
Impute highest of the parallel education information.*/

sort PNR ADAT SPELL
by PNR ADAT (SPELL): egen maxbild=max(IP3)
by PNR ADAT (SPELL): replace IP3=maxbild if IP3~=maxbild & maxbild>0.5


sort PNR SPELL

*single missing value
replace IP3=-9 if IP3==0
tab IP3
keep PNR SPELL IP3
compress
save IP3.dta, replace
log close
exit
