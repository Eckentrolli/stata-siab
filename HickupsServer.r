options("scipen"=30, "digits"=2)

library(foreign)
library(Hmisc)
library(parallel)
library(car)


setwd("/u/home/jd1033/DFG-descriptives")

load(file =  "ResidualsFT.RData")

rm(years)

years <- c(1998, 1999, 2000, 2001, 2002, 2003)



quants <-list()

for (i in years) {
	sample <- subset(trim, year==i & frau==0)
	quants[[i-1997]]<- quantile(sample$discount, probs=seq(0,1,by=0.01),na.rm=FALSE)
	rm(sample)
	}
	
names(quants) <-years

sink("Quants_for_Hickup.txt", split=TRUE) 
quants
sink()

#create graphs for quantiles per year

for (i in years) {

	png(paste("Results/FT/Graphs/Quants_for_hickup", i,".png", sep=""), width=1200, height=1050)
	par(mar=c(5.1, 5.5, 4.1, 2.1))
	plot(quants[[i-1997]], type="h")

	dev.off()
}

perc.one <- numeric()
perc.99 <- numeric()
for (i in 1:6) {
	temp <-  quants[[i]][2]
	perc.one <- c(perc.one, temp)
	
	temp2 <- quants[[i]][100]
	perc.99 <- c(perc.99, temp2)
	}
names(perc.one)<-names(quants)
names(perc.99) <- names(quants)

#plot first percentile over the years

png("Results/FT/Graphs/FirstPerc.png", width=1200, height=1050)
plot(perc.one, type="s", xaxt = "n")
axis(side=1, at=c(1:6), labels=names(perc.one), cex.axis=0.9)
dev.off()

#plot highest percentile over the years

png("Results/FT/Graphs/LastPerc.png", width=1200, height=1050)
plot(perc.99, type="s", xaxt = "n")
axis(side=1, at=c(1:6), labels=names(perc.99), cex.axis=0.9)
dev.off()

vars <- list()
for (i in years) {
	sample <- subset(trim, trim$year==i & trim$frau==0 & trim$workft==1)
	vars[[i-1997]]<- wtd.var(sample$discount, weights=sample$sweight)
	rm(sample)
	}
	
names(vars) <- years
options("digits"=4)
sink("Vars_for_Hickup.txt", split=TRUE) 
vars
sink()

options("digits"=2)

#FEMALES

quantsf <-list()

for (i in years) {
	sample <- subset(trim, year==i & frau==1)
	quantsf[[i-1997]]<- quantile(sample$discount, probs=seq(0,1,by=0.01),na.rm=FALSE)
	rm(sample)
	}
	
names(quantsf) <-years

sink("Quants_for_HickupF.txt", split=TRUE) 
quantsf
sink()

#create graphs for quantiles per year

for (i in years) {

	png(paste("Results/FT/Graphs/Quants_for_hickup", i,"F.png", sep=""), width=1200, height=1050)
	par(mar=c(5.1, 5.5, 4.1, 2.1))
	plot(quantsf[[i-1997]], type="h")

	dev.off()
}

perc.onef <- numeric()
perc.99f <- numeric()
for (i in 1:6) {
	temp <-  quantsf[[i]][2]
	perc.onef <- c(perc.onef, temp)
	
	temp2 <- quantsf[[i]][100]
	perc.99f <- c(perc.99f, temp2)
	}
names(perc.onef)<-names(quantsf)
names(perc.99f) <- names(quantsf)

#plot first percentile over the years

png("Results/FT/Graphs/FirstPercF.png", width=1200, height=1050)
plot(perc.onef, type="s", xaxt = "n")
axis(side=1, at=c(1:6), labels=names(perc.onef), cex.axis=0.9)
dev.off()

#plot highest percentile over the years

png("Results/FT/Graphs/LastPercF.png", width=1200, height=1050)
plot(perc.99f, type="s", xaxt = "n")
axis(side=1, at=c(1:6), labels=names(perc.99f), cex.axis=0.9)
dev.off()

varsf <- list()
for (i in years) {
	sample <- subset(trim, trim$year==i & trim$frau==1 & trim$workft==1)
	varsf[[i-1997]]<- wtd.var(sample$discount, weights=sample$sweight)
	rm(sample)
	}
	
names(varsf) <- years
options("digits"=4)
sink("Vars_for_HickupF.txt", split=TRUE) 
varsf
sink()


