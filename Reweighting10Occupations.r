#Reweighting scheme for comparison of 

options(scipen=30)

library(np)
library(Hmisc)
library(parallel)
library(plyr)
library(speedglm)
library(boot)
#library(psych)



#setwd("E:/DATA")
setwd("/u/home/jd1033/DFG-descriptives/Results/FT")

#load("E:/DATA/Residuals.RData")
load("/u/home/jd1033/DFG-descriptives/ResidualsSmallFT.RData")

#trim <- trim[sample(nrow(trim), 10000), ] 

#results.bak <- results
rm(results, resultsall)

trim$beruf_gr <- as.numeric(trim$beruf_gr)

berufe <- as.data.frame(read.csv("/u/home/jd1033/DFG-descriptives/Occupations_SIAB_Neu.csv", header=TRUE,sep = ";"))

occlist <- sapply(trim$beruf_gr, function(x) berufe[match(x, berufe[,2]),3])

trim$beruf.ab <- occlist
trim$beruf.ab <- as.factor(trim$beruf.ab)

trim <-trim[complete.cases(trim[,50]),]

#save(trim, file="/u/home/jd1033/DFG-descriptives/ResidSmallFTplusOcc.RData")
#load("/u/home/jd1033/DFG-descriptives/ResidSmallFTplusOcc.RData")

years <- unique(trim$year)

#male and female subsamples 
trim.m <- subset(trim, frau==0 & age >= 20 & age <=60)
trim.f <- subset(trim, frau==1 & age >= 20 & age <=60)


rm(trim)


years2 <- years[years >=1985 & years < 2010]


reweight10occs <- function(i) {

	#calculation for base year 1985 to 2010

	pooled <- subset(trim.m, year==2010 | year==i )

	n <- nrow(pooled)
	nbase <- nrow(pooled[pooled$year==2010,])
	ndest <- nrow(pooled[pooled$year==i,])
	
	comp.weight <- nbase/ndest
	
	pooled$comp.weight <- 1
	pooled[pooled$year==i,]$comp.weight <- comp.weight
	
		
	#Create year dummies
	pooled$dyear <-  pooled$year
	pooled$dbase <-  pooled$year

	pooled$dyear[pooled$dyear==2010] <- 0
	pooled$dyear[pooled$dyear==i] <- 1
	pooled$dbase[pooled$dbase==2010] <- 1
	pooled$dbase[pooled$dbase==i] <- 0
	
	pooled$dyear <- as.factor(pooled$dyear)
	pooled$dbase <- as.factor(pooled$dbase)

	#Probit of likelihood of being in 2010, given charactersistics
	
	#results0 <- glm(dyear ~ beruf.ab+beruf.ab:potexp+potexp+potexp2+potexp3+bild+potexp:bild+potexp2:bild+potexp3:bild, data=pooled, family="binomial"(link=probit))
	
	data <- model.matrix( ~ beruf.ab+beruf.ab:potexp+potexp+potexp2+potexp3+bild+potexp:bild+potexp2:bild+potexp3:bild+deutsch+deutsch:bild, data=pooled)
	
	#results1 <- glm.fit(data, pooled$dyear, family="binomial"(link=probit))
	results2 <- speedglm.wfit(pooled$dyear,data, family=binomial(link=probit))
	
	
	#head(inv.logit(data %*% results1$coefficients))
	#head(pnorm(data %*% results1$coefficients))
	fitted.values <- pnorm(data %*% results2$coefficients)
	
		
	#combine values & fits into dataframe
	rw.data <- cbind(pooled, fitted.values=fitted.values)

	#Calculate reweighting factor. (1-P)/P small if P large (e.g. highly educated)
	rw.data$base10occ <- (((1-rw.data$fitted.values)/rw.data$fitted.values)*(1/rw.data$comp.weight))

	#remove superfluous count columns
	rw.data <- rw.data[!names(rw.data) %in% c("dyear", "dbase","sweight.2","count.1", "count.2" ) ]
	save(rw.data, file=paste("reweighting/rw",i,".occ.RData" , sep=""))
	#res <- rw.data[!names(rw.data) %in% "fitted.values"]
	
	if (i==years2[length(years2)]) { 
		res <- rw.data 
		} 	else {
		res <- subset(rw.data, year!=2010) 
		}
	
	rm(rw.data)
	
	return(res)

}



results.10.occ  <- mclapply(years2,function(x) reweight10occs(x), mc.cores=3)

names(results.10.occ) <- years2
trim.m.rw.occ <- results.10.occ[[1]] 

for(i in 2:length(years2)) {
	temp.2 <- results.10.occ[[i]]
	trim.m.rw.occ <- rbind(trim.m.rw.occ, temp.2)
	}
	
	
#set CF weights for baseyear to 1
trim.m.rw.occ[trim.m.rw.occ$year==2010,]$base10occ <- 1


save(trim.m.rw.occ, file="reweighting/trim.m.rw10.occ.RData")


rm(results.10.occ,trim.m.rw.occ)


# library(plyr)
# ddply(trim.f, .(beruf.ab), function(z){
  # data.frame(obsnum = seq_along(z$beruf.ab))
# })

# l <- length(unique(trim.f$beruf.ab))	

# for (j in years) {
	# nr.of.berufs <- apply(trim.f, 2, 
	# row.has.zero <- apply(trim.f, 1, function(x){any(is.na(x))})
		
		
		# }
#FEMALES

 data2010 <- subset(trim.f, year==2010)

 occ.counts <-  cbind(1:63,summary(data2010$beruf.ab))
 data2010$occ.count.base <- sapply(data2010$beruf.ab, function(x) occ.counts[[match(x, occ.counts[,1]),2]])
 data2010$occ.count.dest <- 1000
 # data2010 <- subset(data2010, data2010$occ.count >=30)

#rm(occ.counts)

reweight10occ.f <- function(i) {
	
	data.dest <- subset(trim.f,  year==i)
	data.dest$occ.count.base <- 1000
	occ.counts <-  cbind(1:63,summary(data.dest$beruf.ab))
	data.dest$occ.count.dest <- sapply(data.dest$beruf.ab, function(x) occ.counts[[match(x, occ.counts[,1]),2]])
	
	#data.dest <- subset(data.dest, data.dest$occ.count >=30)
	
	pooled <- rbind(data.dest,data2010)
	pooled <- subset(pooled, pooled$occ.count.dest >=60 & pooled$occ.count.base >= 60)
	
	n <- nrow(pooled)
	nbase <- nrow(pooled[pooled$year==2010,])
	ndest <- nrow(pooled[pooled$year==i,])

	
	comp.weight <- nbase/ndest
	
	pooled$comp.weight <- 1
	pooled[pooled$year==i,]$comp.weight <- comp.weight
	
		
	#Create year dummies
	pooled$dyear <-  pooled$year
	pooled$dbase <-  pooled$year

	pooled$dyear[pooled$dyear==2010] <- 0
	pooled$dyear[pooled$dyear==i] <- 1
	pooled$dbase[pooled$dbase==2010] <- 1
	pooled$dbase[pooled$dbase==i] <- 0
	
	pooled$dyear <- as.factor(pooled$dyear)
	pooled$dbase <- as.factor(pooled$dbase)

	#Probit of likelihood of being in 2010, given charactersistics
	
	
	#	data <- model.matrix( ~ beruf.ab+beruf.ab:potexp+potexp+potexp2+potexp3+bild+potexp:bild+potexp2:bild+potexp3:bild+deutsch+deutsch:bild, data=pooled)
	data <- model.matrix( ~ potexp+potexp2+potexp3+bild+potexp:bild+potexp2:bild+potexp3:bild+deutsch+deutsch:bild, data=pooled)
	
	#results1 <- glm.fit(data, pooled$dyear, family="binomial"(link=probit))
	results2 <- speedglm.wfit(pooled$dyear,data, family=binomial(link=probit))
	
	
	#head(inv.logit(data %*% results1$coefficients))
	#head(pnorm(data %*% results1$coefficients))
	fitted.values <- pnorm(data %*% results2$coefficients)
	
		
	#combine values & fits into dataframe
	rw.data <- cbind(pooled, fitted.values=fitted.values)

	#Calculate reweighting factor. (1-P)/P small if P large (e.g. highly educated)
	rw.data$base10occ <- (((1-rw.data$fitted.values)/rw.data$fitted.values)*(1/rw.data$comp.weight))


	

	#remove superfluous count columns
	rw.data <- rw.data[!names(rw.data) %in% c("dyear", "dbase","sweight.2","count.1", "count.2" ) ]
	save(rw.data, file=paste("reweighting/rw",i,".occF.RData" , sep=""))
	#res <- rw.data[!names(rw.data) %in% "fitted.values"]
	
	if (i==years2[length(years2)]) { 
		res <- rw.data 
		} 	else {
		res <- subset(rw.data, year!=2010) 
		}
	
	rm(rw.data)
	
	return(res)

}


results.10.occ  <- mclapply(years2,function(x) reweight10occ.f(x), mc.cores=3)

names(results.10.occ) <- years2
trim.f.rw.occ <- results.10.occ[[1]] 

for(i in 2:length(years2)) {
	temp.2 <- results.10.occ[[i]]
	trim.f.rw.occ <- rbind(trim.f.rw.occ, temp.2)
	}
	
	
#set CF weights for baseyear to 1
trim.f.rw.occ[trim.f.rw.occ$year==2010,]$base10occ <- 1


save(trim.f.rw.occ, file="reweighting/trim.f.rw10.occ.RData")
