options(scipen=30)

library(foreign)
library(Hmisc)
library(parallel)
library(car)
library(quantreg)



setwd("/u/home/jd1033/DFG-descriptives/Results/FT")

load("reweighting/Resid85.RData")

setwd("/u/home/jd1033/DFG-descriptives")



trim.m.rw$bland <- as.factor(trim.m.rw$bland)
trim.m.rw$bland <- C(trim.m.rw$bland, contr.treatment, base=10)
trim.m.rw$bild <- C(trim.m.rw$bild, contr.treatment, base=2)

occs <- sort(unique(trim.m.rw$beruf_gr))

years <- unique(trim.m.rw$year)
years <- sort(years, decreasing=TRUE)
justfrau <- unique(trim.m.rw$frau)

data.1985.m <- subset(trim.m.rw, trim.m.rw$year==1985 )
data.2010.m <- subset(trim.m.rw, trim.m.rw$year==2010)

fit85 <- rq(discount ~ potexp+potexp2+potexp3+bild+potexp:bild+potexp2:bild+potexp3:bild+deutsch+bland, data=data.1985.m, weights=sweight ,tau=c(0.15, 0.25, 0.5, 0.75, 0.85) , method="pfn")

#fit.test <- rq(discount ~ potexp, data=data.1985.m ,tau=c(0.5, 0.75) )
#sumi.test <- summary(fit.test, se="boot", R=10, bsmethod="mcmb")
#latex.summary.rqs(sumi.test,caption="Norw quantreg", file="Results/FT/Tables/QuantregTEST01")

#bootstrap
sumi.85 <- summary(fit85, se="boot", R=50, bsmethod="mcmb")
sumi.85
latex(sumi.85, file="Results/FT/Tables/qrTabM85" )

###Without reweighting 2010##########

fit2010 <- rq(discount ~ potexp+potexp2+potexp3+bild+potexp:bild+potexp2:bild+potexp3:bild+deutsch+bland, data=data.2010.m, weights=sweight ,tau=c(0.15, 0.25, 0.5, 0.75, 0.85) , method="pfn")

#fit2010 <- lm(discount ~ potexp+potexp2+bild+bild:potexp+potexp2:bild, data=data.2010.m, weights=sweight)

sumi.10 <- summary(fit2010, se="boot", R=50, bsmethod="mcmb")
sumi.10[[1]]
sumi.10[[5]]
latex(sumi.10, file="Results/FT/Tables/qrTabM10" )

###########With reweighting 2010###########

fit2010rw <- rq(discount ~ potexp+potexp2+potexp3+bild+potexp:bild+potexp2:bild+potexp3:bild+deutsch+bland, data=data.2010.m, weights=rweight ,tau=c(0.15, 0.25, 0.5, 0.75, 0.85) , method="pfn")
sumi.10rw <- summary(fit2010, se="boot", R=50, bsmethod="mcmb")
sumi.10rw[[1]]
sumi.10rw[[5]]

latex(sumi.10rw, file="Results/FT/Tables/qrTabM10rw" )