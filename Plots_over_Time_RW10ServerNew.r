options(scipen=30)

library(foreign)
library(Hmisc)
library(WriteXLS)
library(xtable)


setwd("/u/home/jd1033/DFG-descriptives/Results/FT")
#setwd("T:/")


load("reweighting/Resid10.RData")

rm(bindresf, bindresm)

#trim.m.rw <- trim.m.rw[!names(trim.m.rw) %in% c("values.base85", "sweight.1 ","countmf", "rweight.1") ]

setwd("/u/home/jd1033/DFG-descriptives/Results/FT")


###############################################
#########Variance Tables#####################
##############################################

#males
x.table <- xtable(variances10[1:26,], caption ="Variance and Residuals for male workers baseyear 2010")
print(x.table, file="Tables/VarMalesFT10.tex")
rm(x.table)

xltable <- variances10[1:26,]
WriteXLS("xltable", ExcelFileName="Tables/VarMalesFT10.xls", row.names=TRUE)

#females
x.table <- xtable(variances10[27:52,], caption ="Variance and Residuals for female workers baseyear 2010")
print(x.table, file="Tables/VarFemFT10.tex")
rm(x.table)

xltable <- variances10[27:52,]
WriteXLS("xltable", ExcelFileName="Tables/VarFemFT10.xls", row.names=TRUE)


################################################
######### Variance Plots #######################
################################################


#Plot variance for males only 

setEPS()
postscript("Graphs/MalesVar&Res10.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(variances10[1:26,1]~years, xlim=c(1985,2011),ylim=c(0.00,0.25), xaxt = "n",  main="Residual variance males, baseyear 2010", xlab="Time", ylab="Variance / Residual variance", type="o", lwd=2, col="red")
axis(side=1, at=years, labels=years, cex.axis=0.9)
lines(variances10[1:26,3]~years, type="o", col="blue", lwd=2)
legend(1985,0.25, c("Total Variance", "Residual Variance"), lty=c(1,1), lwd=c(2.5,2.5),col=c("blue","red")) # gives the legend lines the correct color and widt
dev.off()


#Plot variance for females only
setEPS()
postscript("Graphs/FemVar&Res10.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(variances10[27:52,1]~years, xlim=c(1985,2011),ylim=c(0.0,0.3), xaxt = "n",  main="Residual variance females, baseyear 2010", xlab="Time", ylab="Variance / Residual variance", type="o", lwd=2, col="red")
axis(side=1, at=years, labels=years, cex.axis=0.9)
lines(variances10[27:52,3]~years, type="o", col="blue", lwd=2)
legend(1985,0.3, c("Total Variance", "Residual Variance"), lty=c(1,1), lwd=c(2.5,2.5),col=c("blue","red")) # gives the legend lines the correct color and widt

dev.off()


################################################
######### Gap-Plots ##########################
################################################

#Calculate Gaps for M/F separate

#MALES

gapsnorw <- data.frame()
gaps <- data.frame()

for (i in years) {

	tmp <- subset(trim.m.rw10, trim.m.rw10$year==i)
	quants<- wtd.quantile(tmp$values, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$rweight)
	resQuants <- wtd.quantile(tmp$res.base10, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$rweight)

	quants.norw<- wtd.quantile(tmp$values, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$sweight)
	resQuants.norw <- wtd.quantile(tmp$res.base10, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$sweight)
	
	eight50 <- (quants["85%"] - quants["50%"])
	eight15 <- (quants["85%"] - quants["15%"])
	five15 <- (quants["50%"] - quants["15%"])
	
	eight50.norw <- (quants.norw["85%"] - quants.norw["50%"])
	eight15.norw <- (quants.norw["85%"] - quants.norw["15%"])
	five15.norw <- (quants.norw["50%"] - quants.norw["15%"])
		
	reseight50 <- (resQuants["85%"] - resQuants["50%"])
	reseight15 <- (resQuants["85%"] - resQuants["15%"])
	resfive15 <- (resQuants["50%"] - resQuants["15%"])
	
	reseight50.norw <- (resQuants.norw["85%"] - resQuants.norw["50%"])
	reseight15.norw <- (resQuants.norw["85%"] - resQuants.norw["15%"])
	resfive15.norw <- (resQuants.norw["50%"] - resQuants.norw["15%"])
	
	gaps[2011-i,1] <- eight50
	gaps[2011-i,2] <- eight15
	gaps[2011-i,3] <- five15
	gaps[2011-i,4] <- reseight50
	gaps[2011-i,5] <- reseight15
	gaps[2011-i,6] <- resfive15
	gaps[2011-i,7] <- quants["85%"]
	gaps[2011-i,8] <- quants["50%"]
	gaps[2011-i,9] <- quants["15%"]
	
	gapsnorw[2011-i,1] <- eight50.norw
	gapsnorw[2011-i,2] <- eight15.norw
	gapsnorw[2011-i,3] <- five15.norw
	gapsnorw[2011-i,4] <- reseight50.norw
	gapsnorw[2011-i,5] <- reseight15.norw
	gapsnorw[2011-i,6] <- resfive15.norw
	gapsnorw[2011-i,7] <- quants.norw["85%"]
	gapsnorw[2011-i,8] <- quants.norw["50%"]
	gapsnorw[2011-i,9] <- quants.norw["15%"]
}

colnames(gaps) <- c("eight50", "eight15", "five15", "reseight50", "reseight15", "resfive15" ,"85% quantile", "Median", "15% quantile")
gaps <- cbind(years, gaps)
colnames(gaps)[1]<-"years"
rownames(gaps)<-gaps$years

colnames(gapsnorw) <- c("eight50", "eight15", "five15", "reseight50", "reseight15", "resfive15" ,"85% quantile", "Median", "15% quantile")
gapsnorw <- cbind(years, gapsnorw)
colnames(gapsnorw)[1]<-"years"
rownames(gapsnorw)<-gapsnorw$years

sink("Q-gapsRW10.txt", split=TRUE)
gaps
sink()

save(gapsnorw, gaps, file="Gaps_and_NoRW.10.M.RData")


#percentage developments in composition adjusted wage gaps
perc.gaps <- data.frame(years)

base850 <- gaps$eight50[1]/100
base815 <- gaps$eight15[1]/100
base515 <- gaps$five15[1]/100

for (i in 1:26) {
	perc.gaps[i,2] <- gaps[i,2]/base850
	perc.gaps[i,3] <- gaps[i,3]/base815
	perc.gaps[i,4] <- gaps[i,4]/base515
}
colnames(perc.gaps) <- c("years","eight50gap.perc", "eight15gap.perc", "five15gap.perc")

sink("Q-gapsRW10-perc.txt", split=TRUE)
perc.gaps
sink()


#percentage developments in non-adjusted wage gaps
perc.gapsnorw <- data.frame(years)

base850 <- gapsnorw$eight50[1]/100
base815 <- gapsnorw$eight15[1]/100
base515 <- gapsnorw$five15[1]/100

for (i in 1:26) {
	perc.gapsnorw[i,2] <- gapsnorw[i,2]/base850
	perc.gapsnorw[i,3] <- gapsnorw[i,3]/base815
	perc.gapsnorw[i,4] <- gapsnorw[i,4]/base515
}
colnames(perc.gapsnorw) <- c("years","eight50gap.perc", "eight15gap.perc", "five15gap.perc")

sink("Q-gaps-perc.txt", split=TRUE)
perc.gapsnorw
sink()

#Calculate explained share
gapsnorw$eight50[1] - gapsnorw$eight50[26]
gapsnorw$eight15[1] - gapsnorw$eight15[26]
gapsnorw$five15[1] - gapsnorw$five15[26]

gaps$eight50[1] - gaps$eight50[26]
gaps$eight15[1] - gaps$eight15[26]
gaps$five15[1] - gaps$five15[26]

1-((gaps$eight50[1] - gaps$eight50[26])/(gapsnorw$eight50[1] - gapsnorw$eight50[26]))
1-((gaps$eight15[1] - gaps$eight15[26])/(gapsnorw$eight15[1] - gapsnorw$eight15[26]))
1-((gaps$five15[1] - gaps$five15[26])/(gapsnorw$five15[1] - gapsnorw$five15[26]))

#FEMALES

gapsnorw.f <- data.frame()
gaps.f <- data.frame()

for (i in years) {

	tmp <- subset(trim.f.rw10, trim.f.rw10$year==i)
	quants<- wtd.quantile(tmp$values, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$rweight)
	resQuants <- wtd.quantile(tmp$res.base10, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$rweight)

	quants.norw<- wtd.quantile(tmp$values, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$sweight)
	resQuants.norw <- wtd.quantile(tmp$res.base10, probs=c(0.85,0.5,0.15), na.rm=FALSE, weights=tmp$sweight)
	
	eight50 <- (quants["85%"] - quants["50%"])
	eight15 <- (quants["85%"] - quants["15%"])
	five15 <- (quants["50%"] - quants["15%"])
	
	eight50.norw <- (quants.norw["85%"] - quants.norw["50%"])
	eight15.norw <- (quants.norw["85%"] - quants.norw["15%"])
	five15.norw <- (quants.norw["50%"] - quants.norw["15%"])
		
	reseight50 <- (resQuants["85%"] - resQuants["50%"])
	reseight15 <- (resQuants["85%"] - resQuants["15%"])
	resfive15 <- (resQuants["50%"] - resQuants["15%"])
	
	reseight50.norw <- (resQuants.norw["85%"] - resQuants.norw["50%"])
	reseight15.norw <- (resQuants.norw["85%"] - resQuants.norw["15%"])
	resfive15.norw <- (resQuants.norw["50%"] - resQuants.norw["15%"])
	
	gaps.f[2011-i,1] <- eight50
	gaps.f[2011-i,2] <- eight15
	gaps.f[2011-i,3] <- five15
	gaps.f[2011-i,4] <- reseight50
	gaps.f[2011-i,5] <- reseight15
	gaps.f[2011-i,6] <- resfive15
	gaps.f[2011-i,7] <- quants["85%"]
	gaps.f[2011-i,8] <- quants["50%"]
	gaps.f[2011-i,9] <- quants["15%"]
	
	gapsnorw.f[2011-i,1] <- eight50.norw
	gapsnorw.f[2011-i,2] <- eight15.norw
	gapsnorw.f[2011-i,3] <- five15.norw
	gapsnorw.f[2011-i,4] <- reseight50.norw
	gapsnorw.f[2011-i,5] <- reseight15.norw
	gapsnorw.f[2011-i,6] <- resfive15.norw
	gapsnorw.f[2011-i,7] <- quants.norw["85%"]
	gapsnorw.f[2011-i,8] <- quants.norw["50%"]
	gapsnorw.f[2011-i,9] <- quants.norw["15%"]
}

colnames(gaps.f) <- c("eight50", "eight15", "five15", "reseight50", "reseight15", "resfive15" ,"85% quantile", "Median", "15% quantile")
gaps.f <- cbind(years, gaps.f)
colnames(gaps.f)[1]<-"years"
rownames(gaps.f)<-gaps.f$years

colnames(gapsnorw.f) <- c("eight50", "eight15", "five15", "reseight50", "reseight15", "resfive15" ,"85% quantile", "Median", "15% quantile")
gapsnorw.f <- cbind(years, gapsnorw.f)
colnames(gapsnorw.f)[1]<-"years"
rownames(gapsnorw.f)<-gapsnorw.f$years


sink("Q-gapsRW10.F.txt", split=TRUE)
gaps.f
sink()

save(gapsnorw.f, gaps.f, file="Gaps_and_NoRW.10.F.RData")


#percentage developments in composition adjusted wage gaps
perc.gaps.f <- data.frame(years)

base850 <- gaps.f$eight50[1]/100
base815 <- gaps.f$eight15[1]/100
base515 <- gaps.f$five15[1]/100

for (i in 1:26) {
	perc.gaps.f[i,2] <- gaps.f[i,2]/base850
	perc.gaps.f[i,3] <- gaps.f[i,3]/base815
	perc.gaps.f[i,4] <- gaps.f[i,4]/base515
}
colnames(perc.gaps.f) <- c("years","eight50gap.perc", "eight15gap.perc", "five15gap.perc")

sink("Q-gapsRW10-percF.txt", split=TRUE)
perc.gaps.f
sink()


#percentage developments in non-adjusted wage gaps
perc.gapsnorw.f <- data.frame(years)

base850 <- gapsnorw.f$eight50[1]/100
base815 <- gapsnorw.f$eight15[1]/100
base515 <- gapsnorw.f$five15[1]/100

for (i in 1:26) {
	perc.gapsnorw.f[i,2] <- gapsnorw.f[i,2]/base850
	perc.gapsnorw.f[i,3] <- gapsnorw.f[i,3]/base815
	perc.gapsnorw.f[i,4] <- gapsnorw.f[i,4]/base515
}
colnames(perc.gapsnorw.f) <- c("years","eight50gap.perc", "eight15gap.perc", "five15gap.perc")

sink("Q-gaps-percF.txt", split=TRUE)
perc.gapsnorw.f
sink()

#Calculate explained share
gapsnorw.f$eight50[1] - gapsnorw.f$eight50[26]
gapsnorw.f$eight15[1] - gapsnorw.f$eight15[26]
gapsnorw.f$five15[1] - gapsnorw.f$five15[26]

gaps.f$eight50[1] - gaps.f$eight50[26]
gaps.f$eight15[1] - gaps.f$eight15[26]
gaps.f$five15[1] - gaps.f$five15[26]

1-((gaps.f$eight50[1] - gaps.f$eight50[26])/(gapsnorw.f$eight50[1] - gapsnorw.f$eight50[26]))
1-((gaps.f$eight15[1] - gaps.f$eight15[26])/(gapsnorw.f$eight15[1] - gapsnorw.f$eight15[26]))
1-((gaps.f$five15[1] - gaps.f$five15[26])/(gapsnorw.f$five15[1] - gapsnorw.f$five15[26]))



##################################################################
#PLOTS
################################################################

load("/u/home/jd1033/DFG-descriptives/Results/FT/Gaps_and_NoRW.10.M.RData")
load("/u/home/jd1033/DFG-descriptives/Results/FT/Gaps_and_NoRW.10.F.RData")



#Plot 85/15, 85/50, 50/15 gaps for males only 

setEPS()
postscript("Graphs/Gapsmales10.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gaps[,"eight50"]~gaps$years, xlim=c(1985,2011),ylim=c(0.00,1.10), xaxt = "n",  main="Quantile gaps males only, baseyear 2010", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="red")
axis(side=1, at=gaps$years, labels=gaps$years, cex.axis=0.9)
lines(gaps[,"eight15"]~gaps$years, type="o", col="blue", lwd=2)
lines(gaps[,"five15"]~gaps$years, type="o", col="green", lwd=2)

legend(1985,1.1, c("85/15 gap","85/50 gap", "50/15 gap"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green")) # gives the legend lines the correct color and widt

dev.off()

#Table
temptab <- xtable(gaps, caption="Quantiles Males baseyear 2010")
print(temptab, include.rownames=FALSE, file="Tables/QuantMales10.tex")

xltable <- gaps
WriteXLS("xltable", ExcelFileName="Tables/QuantMales10.xls", row.names=TRUE)


#Combined Gaps Males 2010

setEPS()
postscript("Graphs/CombinedMales2010.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gaps[,"eight50"]~gaps$years, xlim=c(1985,2011),ylim=c(0.00,1.25), xaxt = "n",  main="Quantile gaps males, overlaid with baseyear 2010", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="hotpink2")
axis(side=1, at=gaps$years, labels=gaps$years, cex.axis=0.9)
lines(gaps[,"eight15"]~gaps$years, type="o", col="cornflowerblue", lwd=2)
lines(gaps[,"five15"]~gaps$years, type="o", col="greenyellow", lwd=2)

lines(gapsnorw[,"eight50"]~gapsnorw$years, type="o", col="red", lwd=2)
lines(gapsnorw[,"eight15"]~gapsnorw$years, type="o", col="blue", lwd=2)
lines(gapsnorw[,"five15"]~gapsnorw$years, type="o", col="green", lwd=2)


legend(1985,1.25, c("85/15 gap","85/50 gap", "50/15 gap","85/15 gap baseyear 2010","85/50 gap baseyear 2010", "50/15 gap baseyear 2010"), 
lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green","cornflowerblue","hotpink2","greenyellow" )) # gives the legend lines the correct color and widt

dev.off()



#Residual Gaps
setEPS()
postscript("Graphs/ResGapsmales10.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gaps[,"reseight50"]~gaps$years, xlim=c(1985,2011),ylim=c(0.00,1.10), xaxt = "n",  main="Residual quantile gaps males only, baseyear 2010", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="red")
axis(side=1, at=years, labels=gaps$years, cex.axis=0.9)
lines(gaps[,"reseight15"]~gaps$years, type="o", col="blue", lwd=2)
lines(gaps[,"resfive15"]~gaps$years, type="o", col="green", lwd=2)

legend(1985,1.1, c("85/15 gap","85/50 gap", "50/15 gap"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green")) # gives the legend lines the correct color and widt

dev.off()


#FEMALES

#Plot 85/15, 85/50, 50/15 gaps for females only 

setEPS()
postscript("Graphs/Gapsfemales10.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gaps.f[,"eight50"]~gaps.f$years, xlim=c(1985,2011),ylim=c(0.1,1.0), xaxt = "n",  main="Quantile gaps females only, baseyear 2010", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="red")
axis(side=1, at=years, labels=years, cex.axis=0.9)
lines(gaps.f[,"eight15"]~gaps.f$years, type="o", col="blue", lwd=2)
lines(gaps.f[,"five15"]~gaps.f$years, type="o", col="green", lwd=2)

legend(1986,1.0, c("85/15 gap","85/50 gap", "50/15 gap"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green")) # gives the legend lines the correct color and widt
dev.off()

temptab <- xtable(gaps.f, caption="Quantiles Females baseyear 2010")
print(temptab, include.rownames=FALSE, file="Tables/QuantFemales10.tex")

xltable <- gaps.f
WriteXLS("xltable", ExcelFileName="Tables/QuantFemales10.xls", row.names=TRUE)

#Residual Gaps

setEPS()
postscript("Graphs/Res.Gapsfemales10.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gaps.f[,"reseight50"]~gaps.f$years, xlim=c(1985,2011),ylim=c(0.1,1.0), xaxt = "n",  main="Residual quantile gaps females only, baseyear 2010", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="red")
axis(side=1, at=years, labels=years, cex.axis=0.9)
lines(gaps.f[,"reseight15"]~gaps.f$years, type="o", col="blue", lwd=2)
lines(gaps.f[,"resfive15"]~gaps.f$years, type="o", col="green", lwd=2)

legend(1985,1.0, c("85/15 gap","85/50 gap", "50/15 gap"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green")) # gives the legend lines the correct color and widt

dev.off()


#Combined Gaps Females 2010

setEPS()
postscript("Graphs/CombinedFemales2010.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gaps.f[,"eight50"]~gaps.f$years, xlim=c(1985,2011),ylim=c(0.2,1.35), xaxt = "n",  main="Quantile gaps females, overlaid with baseyear 2010", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="hotpink2")
axis(side=1, at=gaps.f$years, labels=gaps.f$years, cex.axis=0.9)
lines(gaps.f[,"eight15"]~gaps.f$years, type="o", col="cornflowerblue", lwd=2)
lines(gaps.f[,"five15"]~gaps.f$years, type="o", col="greenyellow", lwd=2)

lines(gapsnorw.f[,"eight50"]~gapsnorw.f$years, type="o", col="red", lwd=2)
lines(gapsnorw.f[,"eight15"]~gapsnorw.f$years, type="o", col="blue", lwd=2)
lines(gapsnorw.f[,"five15"]~gapsnorw.f$years, type="o", col="green", lwd=2)


legend(1985,1.35, c("85/15 gap","85/50 gap", "50/15 gap","85/15 gap baseyear 2010","85/50 gap baseyear 2010", "50/15 gap baseyear 2010"), 
lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green","cornflowerblue","hotpink2","greenyellow" )) # gives the legend lines the correct color and widt

dev.off()

