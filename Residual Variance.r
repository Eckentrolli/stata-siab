options(scipen=30)

library(foreign)
library(Hmisc)
library(parallel)
library(car)

setwd("E:/Data")
#setwd("I:/Data")

data <- read.dta("E:/Data/siab_r_7510_v1_testdata.dta", convert.factors=FALSE)
#data <- read.dta("I:/DATA/siab_r_7510_v1_testdata.dta", convert.factors=FALSE)
#preparation
attach(data)


data$bild <- as.factor(IP2A)
data$deutsch <- as.factor(deutsch)

data$potexp2 <- potexp*potexp
data$potexp3 <- potexp*potexp*potexp

detach(data)

#Create subsample for west german full time workers with non-missing education

trim <- subset(data,  workft==1  )
rm(data)

trim$bild <- C(trim$bild, contr.treatment, base=3) 

trim$count <- c(1:length(trim$discount))
#reg-preparation

years <- unique(trim$year)
years <- sort(years, decreasing=TRUE)
justfrau <- unique(trim$frau)
grid <- as.matrix(expand.grid(years, justfrau))

grid2 <- grid
colnames(grid)<-c("year","frau")


#define functions

cellreg <- function(i, j) {
	sample1 <- subset(trim, frau==j & year==i)		
	temp <-lm(discount ~ potexp+potexp2+potexp3+bild+potexp:bild+potexp2:bild+potexp3:bild, data=sample1,weights=sweight)
	res<-data.frame(count=sample1$count,residualsmf=temp$residuals, fittedmf=temp$fitted.values, values=sample1$discount, sweight=sample1$sweight)
	rm(sample1)
	
	return(res)
}

cellregall <- function(i) {
	sample2 <- subset(trim, year==i)
	#test<- summary(sample1)	
	temp <-lm(discount ~ frau+potexp+potexp2+potexp3+bild+potexp:bild+potexp2:bild+potexp3:bild, data=sample2,weights=sweight)
	resall <- data.frame(count=sample2$count, residualsall=temp$residuals, fittedall=temp$fitted.values, valuesall= sample2$values, sweight=sample2$sweight)
	rm(sample2)
	return(resall)
}

grid3 <- numeric()
for (i in 1:nrow(grid)) {
	grid3 <- c(grid3, paste(as.character(grid[i,1]), as.character(grid[i,2]),sep="@"))
}

wrapper <- function(i) {
	x <- as.numeric(unlist(strsplit(i, split="@")))
	res2<-cellreg(x[1],x[2])
	return(res2)
}

#Mincer regression

results <- mclapply(grid3,function(x) wrapper(x))

bindres <- results[[1]]
for (i in 2:length(grid3)) {
bindres<- rbind(bindres, results[[i]])
}

trim <- trim[order(trim$count),]
bindres <- bindres[order(bindres$count),]

trim <- cbind(trim, residualsmf=bindres$residualsmf, fittedmf=bindres$fittedmf)

resultsall <- mclapply(years, function(x) cellregall(x))

bindresall <- resultsall[[1]]

for (i in 2:length(years)) {
bindresall<- rbind(bindresall, resultsall[[i]])
}

bindresall <- bindresall[order(bindresall$count),]
bindresall <- subset(bindresall, select= -valuesall)

trim <- cbind(trim, residualsall=bindresall$residualsall, fittedall=bindresall$fittedall)

#naming

grid2[grid2[,2]==0,][,2] <-"m"
grid2[grid2[,2]==1,][,2] <-"f"

resnames <- numeric()
for (i in 1:dim(grid2)[1]) {
	resnames <- c(resnames, paste(as.character(grid2[i,1]), as.character(grid2[i,2]),sep=""))
}

names(results) <- resnames 
names(resultsall) <- c(years[1:31])

variances <- data.frame()

for (i in 1:nrow(grid)) {

variances[i,1] <- wtd.var(results[[i]]$residualsmf,weights=results[[i]]$sweight, normwt=TRUE, na.rm=FALSE) 
variances[i,2] <- sqrt(wtd.var(results[[i]]$residualsmf,weights=results[[i]]$sweight, normwt=TRUE, na.rm=FALSE) )
variances[i,3] <- wtd.var(results[[i]]$values, weights=results[[i]]$sweight, normwt=TRUE, na.rm=FALSE)
variances[i,4] <- sqrt( wtd.var(results[[i]]$values, weights=results[[i]]$sweight, normwt=TRUE, na.rm=FALSE))
}


rownames(variances) <- resnames
colnames(variances) <- c("ResidualV", "ResidualSD", "Variance", "SD")

varsall <- data.frame()
for (i in 1:length(years)) {

varsall[i,1] <- wtd.var(resultsall[[i]]$residualsall,weights=resultsall[[i]]$sweight, normwt=TRUE, na.rm=FALSE)
varsall[i,2] <- sqrt(wtd.var(resultsall[[i]]$residualsall,weights=resultsall[[i]]$sweight, normwt=TRUE, na.rm=FALSE))

varsall[i,3] <- wtd.var(resultsall[[i]]$valuesall, weights=resultsall[[i]]$sweight, normwt=TRUE, na.rm=FALSE)
varsall[i,4] <- sqrt(wtd.var(resultsall[[i]]$valuesall, weights=resultsall[[i]]$sweight, normwt=TRUE, na.rm=FALSE))

}

rownames(varsall) <- c(years[1:31])
colnames(varsall) <- c("ResidualV", "ResidualSD", "Variance", "SD")

# variances <- lapply(results, function(i) var(unlist(i)))
# varsall <- lapply(resultsall, function(i) var(unlist(i)))

save.image(file = "E:/Data/Residuals.RData", compress=TRUE)
#save.image(file = "I:/DATA/Residuals.RData", compress=TRUE)


#Res SD m/F
plot(varsall[,2]~years)
#Variance M/F
plot(varsall[,3]~years)
#SD m+f
plot(varsall[,4]~years)

#Res variance females
plot(variances[1:31,1]~years)
#Res SD fem
plot(variances[1:31,2]~years)

#Variance fem
plot(variances[1:31,3]~years)
#SD fem
plot(variances[1:31,4]~years)

#Res variance male
plot(variances[32:62,1]~years)
#Res SD male
plot(variances[32:62,2]~years)
#Variance male
plot(variances[32:62,3]~years)
#SD male
plot(variances[32:62,4]~years)

#hist(trim$discount[trim$year==1996], breaks=50)
#hist(trim$discount[trim$year==1997], breaks=50)

#Create 

