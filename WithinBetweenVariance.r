#Calculate Variance & gaps for age/education cells
#West germany fulltime workers only

options(scipen=30)

library(foreign)
library(Hmisc)
library(parallel)
library(car)
library(xtable)

setwd("E:/DATA/Results")
#setwd("C:/Dokumente/Uni/Wage Inequality/Results")

load("E:/DATA/Residuals.RData")
#load("I:/DATA/Residuals.RData")

#Create  bildcat variable with only 4 values (analogous to Dustman)
#Bildcat labels: Kein = 0, Schulabschluss = 1, Ausbildung = 2, Hochschule = 3

trim$bildcat <- recode(trim$IP2A, "c(-9, 1)= 0; 2=1; c(3,4)=2; c(5,6)= 3")
trim$agecat <- recode(trim$age, "17:30=18; 31:40=31; 41:50=41; 51:62=51")
trim$cell <- paste(trim$agecat, trim$IP2A, sep="") 
trim$cell <- as.factor(trim$cell)
trim$bild <- as.factor(trim$IP2A)

#Make two tables, male and female each
#Define cells with 
#Bildung 6x
#Age groups 17-30 = 1, 31-40 = 2, 41-50 =3 , 51-62 =4 4x
#Years 1985,1990,2000,2004,2010

#Create subsamples of male and female workers
males <- subset(trim, frau==0)
females <- subset(trim, frau==1)
years <- unique(trim$year)
# results <- data.frame()

# for(i in years) {
	# tmp <- subset(males, males$year==i)
	# attach(tmp)
	# test <- aov(discount~cell, data=tmp)
	# print(i)
	# print(summary(test))
	# detach(tmp)
	
	# test$"cell"$"Mean Sq"

# }
for(i in years) {
	cellmeansby(males$discount, males$cell, mean)
}