options(scipen=30)

library(foreign)
library(Hmisc)
library(parallel)
library(car)

setwd("/u/home/jd1033/DFG-descriptives")
load("/u/home/jd1033/DFG-descriptives/siab_r_7510_v1_forResiduals.RData")

attach(data)

data$bild <- as.factor(IP2A)
data$deutsch <- as.factor(deutsch)



data$potexp2 <- potexp*potexp
data$potexp3 <- potexp*potexp*potexp
detach(data)

#Create subsample for west german full time workers with non-missing education
trim <- subset(data,  workft==1  )
rm(data)

trim$bild <- C(trim$bild, contr.treatment, base=3)
trim$count <- c(1:length(trim$discount))

occs <- sort(unique(trim$beruf_gr))

years <- unique(trim$year)
years <- sort(years, decreasing=TRUE)
justfrau <- unique(trim$frau)

input <- expand.grid(years, justfrau, occs)
colnames(input) <- c("year","Frau","Occupation")

#trim <- trim[sample(nrow(trim), 50000), ] 

occucells <- function (i) {
	sample1 <- subset(trim, year==i[[1]] & frau==i[[2]] & beruf_gr==i[[3]] )
	groupmean <- wtd.mean(sample1$discount, weights=sample1$sweight, na.rm=FALSE)
	variance <- wtd.var(sample1$discount, weights=sample1$sweight, na.rm=FALSE)
	SD <- sqrt(wtd.var(sample1$discount, weights=sample1$sweight, na.rm=FALSE))
	quants <- wtd.quantile(sample1$discount, probs=c(0.85,0.5,0.15),weights=sample1$sweight, na.rm=FALSE)
	nine50 <- (quants["85%"] - quants["50%"])
	nine10 <- (quants["85%"] - quants["15%"])
	five10 <- (quants["50%"] - quants["15%"])
	
	obs <- nrow(sample1)
	share <- obs/nrow(trim[trim$year==i[[1]] & trim$frau==i[[2]],])
	rm(sample1)
	return(c(obs, groupmean, share, variance, SD, nine50, nine10, five10))
	}
	
results <- t(apply(input, 1, occucells))
colnames(results) <- c("obs", "groupmean", "share", "variance", "SD", "eight50", "eight15", "five15")

results <- cbind(input, results)



jobs.gen <- expand.grid(occs,justfrau)
colnames(jobs.gen) <- c("Job", "Frau")

occdiff <- function (i) {
	sample1 <- results[results[,3] == i[[1]] & results[,2] == i[[2]] & results[,1]==1985,]
	sample2 <- results[results[,3] == i[[1]] & results[,2] == i[[2]] & results[,1]==2010,]
	diff.var <- sample2$variance-sample1$variance 
	vari85 <-sample1$variance
	vari10 <- sample2$variance
	eight15.85<- sample1$eight15
	eight15.10 <- sample2$eight15
	diff.var.abs <- abs(diff.var)
	diff.share <- sample2$share-sample1$share
	diff.share.abs <- abs(diff.share)
	occ <-sample1$Occupation
	frau <- sample1$Frau
	rm(sample1,sample2)
	return(c(occ,frau, diff.share, diff.share.abs, vari85,vari10,eight15.85,eight15.10,diff.var, diff.var.abs))
	}

diffs <- t(apply(jobs.gen, 1, occdiff))
colnames(diffs) <- c("occ", "frau", "diff.share", "diff.share.abs", "vari85", "vari10", "eight15.85", "eight15.10","diff.var","diff.var.abs")
diffs.m <- subset(diffs, diffs[,2]==0)
diffs.f <- subset(diffs, diffs[,2]==1)

#T10 WG-Variance 1985
top10.var85.m <- head(diffs.m[order(-diffs.m[,5]),], n=10)
top10.var85.f <- head(diffs.f[order(-diffs.f[,5]),], n=10)
#T10 WG-Variance 2010
top10.var10.m <- head(diffs.m[order(-diffs.m[,6]),], n=10)
top10.var10.f <- head(diffs.f[order(-diffs.f[,6]),], n=10)

#T10 WG-Gaps 1985
top10.gap85.m <- head(diffs.m[order(-diffs.m[,7]),], n=10)
top10.gap85.f <- head(diffs.f[order(-diffs.f[,7]),], n=10)
#T10 WG-Gaps 2010
top10.gap10.m <- head(diffs.m[order(-diffs.m[,8]),], n=10)
top10.gap10.f <- head(diffs.f[order(-diffs.f[,8]),], n=10)
#T10 sharediff
top10.share.m <- head(diffs.m[order(-diffs.m[,6]),], n=10)
top10.share.f <- head(diffs.f[order(-diffs.f[,6]),], n=10)

rcorr(diffs.m)

save.image(file="Results/FT/Results.Occupactions.RData")	


##################################################################