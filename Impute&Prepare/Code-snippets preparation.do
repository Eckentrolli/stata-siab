recode bild (50 21 = 0) (1 2 3 4 =1) (5 22 23 24 25 = 2) (6 26 27 = 3)   , gen(bildcat)

label define bildcatl 0 "Kein" 1 "Schulabschluss" 2 "Ausbildung" 3 "Hochschulabschluss"
label values bildcat bildcatl


recode IP2A (-9 1 = 10) (2 = 12.125) (3= 13) (4= 15.125) (5= 15) (6=18), gen (bildyear)

gen potexp = age-bildyear