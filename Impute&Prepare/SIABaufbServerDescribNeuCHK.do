set more off
capture program drop fill

log using "/u/home/jd1033/siab/logs/AufbereitungStep1CHK.log" , replace

use "/u/home/jd1033/siab/CHK/siab_r_7510_v1_mergedCHK.dta"
sort persnr begepi

//Restrict Sample to relevant years 

gen year = year(begepi)
drop if year < 1985

gen workft=0
replace workft=1 if inlist(stib,1,2,3,4) & inlist(erwstat_gr, 1,2) 
drop if workft==0

// Generate variables
gen age= (year(begepi)-gebjahr)
gen bildorig=bild

// Find overlapping spells

sort persnr begepi


by persnr: drop if begepi==begepi[_n+1] & endepi==endepi[_n+1] & tentgelt_gr[_n] <= tentgelt_gr[_n+1]
by persnr: drop if begepi==begepi[_n-1] & endepi==endepi[_n-1] & tentgelt_gr[_n] < tentgelt_gr[_n-1]


gen overl = 0
by persnr: replace overl=1 if begepi[_n+1] < endepi[_n] &  persnr[_n] == persnr[_n+1]

by persnr: replace overl=1 if endepi[_n-1] > begepi[_n] &  persnr[_n-1] == persnr[_n]

gen begepi2 = begepi
gen endepi2 = endepi

by persnr: replace begepi2 = (endepi[_n-1]) if overl==1 & overl[_n-1]==1 & tentgelt_gr[_n] <= tentgelt_gr[_n-1]

by persnr: replace endepi2 = (begepi[_n+1]) if overl==1 & overl[_n+1]==1 &  tentgelt_gr[_n] <= tentgelt_gr[_n+1]

by persnr: replace overl=3 if endepi2[_n-1] > begepi2[_n] &  persnr[_n-1] == persnr[_n]

tab overl 

replace begepi = begepi2
replace endepi = endepi2

format begepi %td
format endepi %td

drop begepi2 endepi2



//Define program to fill up continuous missing values

program define fill
gsort persnr begepi
replace `1'=`1'[_n-1] if `1' >=. & `1'[_n-1] <. & persnr==persnr[_n-1] 
gsort -persnr -begepi
replace `1'=`1'[_n-1] if `1' >=. & `1'[_n-1] <. & persnr==persnr[_n-1] 
gsort persnr begepi
end

//Apply fill program to variables

sort persnr begepi
fill schbild
fill frau
fill deutsch



//Generate weights by share of the year spent working

gen workdays = (endepi-begepi)
gen sweight = (1/365)*(workdays)
gen ltentgelt = ln(tentgelt_gr)

//Missing discrete

replace pendler = 3 if year >= 1999 & pendler >= . 
replace beruf_gr = 150 if beruf_gr >=.
replace deutsch = 3 if deutsch >=.

//Drop some variables to reduce file size

drop gebjahr
drop tage_alt
drop _merge

//Code zur Bereinigung der ao_region

sort persnr 
* Absolute Häufigkeit der Personennummer
by persnr: generate Ipers=_N 

sort persnr ao_region

* Duplizieren der Arbeitsortangabe vllt. für später
gen Dao_region=ao_region

* Missings in Variable Arbeitsortangabe vereinheitlichen
replace ao_region=.n if ao_region==.z 

sort persnr ao_region

* Absolute Häufigkeit der Arbeitsortangabe wenn kein Arbeitsort gegeben ist
by persnr ao_region: generate Iao=_N if ao_region==.n 

* Sollten die Absoluten Häufigkeiten identisch sein, bedeutet das über die gesamte Historie keine Angabe exitiert und Sie dann unbrauchbar sind
drop if Ipers==Iao & ao_region==.n 

* Markierung der Kreise der neuen Bundesländer
*replace IaoM=1 if ao_region>11000

sort persnr

* Für Arbeitsregion aus der Vergangenheit/Zukunft sollten bnn übereinstimmen dann ist der Arbeitgeber nicht umgezogen "Annahme Arbeitgeber ist eher immobil"

count if ao_region==.n

sort persnr begepi

*tab persnr

sum spell

*2617 max Spell Anzahl eines Individuums

forvalues i=1(1)2617{
by persnr: replace ao_region=ao_region[_n-`i'] if ao_region==.n & ao_region[_n-`i']!=.n & bnn==bnn[_n-`i'] & persnr==persnr[_n-`i']
}


forvalues i=1(1)2617{
by persnr: replace ao_region=ao_region[_n+`i'] if ao_region==.n & ao_region[_n+`i']!=.n & bnn==bnn[_n+`i'] & persnr==persnr[_n+`i']
}
 
count if ao_region==.n

mdesc ao_region

drop if ao_region>=.

gen bland=int(ao_region/1000)
gen ost=0 
replace ost=1 if ao_region>11000



//Split into east and west sample and save

save "/u/home/jd1033/siab/CHK/siab_r_7510_v1_mergedCHKaufb1.dta",replace

drop if ost==1

save "/u/home/jd1033/siab/CHK/siab_7510_CHK_West1.dta",replace
clear

use "/u/home/jd1033/siab/CHK/siab_r_7510_v1_mergedCHKaufb1.dta"

drop if ost==0
save "/u/home/jd1033/siab/CHK/siab_7510_CHK_Ost1.dta",replace


log close