set more off

sort persnr 

* Absolute H�ufigkeit der Personennummer
by persnr: generate Ipers=_N 

sort persnr ao_region

* Duplizieren der Arbeitsortangabe vllt. f�r sp�ter
gen Dao_region=ao_region

* Missings in Variable Arbeitsortangabe vereinheitlichen
replace ao_region=.n if ao_region==.z 

sort persnr ao_region

* Absolute H�ufigkeit der Arbeitsortangabe wenn kein Arbeitsort gegeben ist
by persnr ao_region: generate Iao=_N if ao_region==.n 

* Sollten die Absoluten H�ufigkeiten identisch sein, bedeutet das �ber die gesamte Historie keine Angabe exitiert und Sie dann unbrauchbar sind
drop if Ipers==Iao & ao_region==.n 

* Markierung der Kreise der alten Bundesl�nder an der Grenze zu den neuen Bundesl�nder
gen IaoM=1 if (ao_region==13058 | ao_region==13054  | ao_region==12068 | ao_region==15090| ao_region==15083 | ao_region==15085 | ao_region==16062 | ao_region==16061 | ao_region==16064 | ao_region==16063 | ao_region==16066 | ao_region==16072 | ao_region==16073 | ao_region==16075 | ao_region==14523 | ao_region==9475 | ao_region==9674 | ao_region==9473 | ao_region==9478 | ao_region==6631 |ao_region==6632 | ao_region==6636  | ao_region==3152 | ao_region==3155| ao_region==3153 | ao_region==3158 | ao_region==3154 | ao_region==3151 | ao_region==3354 | ao_region==3355 | ao_region==1053 | ao_region==1003 )

* Markierung der Kreise der neuen Bundesl�nder
*replace IaoM=1 if ao_region>11000

sort persnr

* Missings wurden mitgez�hlt als Markierung bei den Kreisen, dieses wird r�ckg�ngig gemacht
replace IaoM=. if ao_region==.n

* Markierung der gesamten Historie sollten ein Individuum nur einmal in Grenzkreise oder neuen Bundesl�ndern gearbeitet haben
by persnr: egen IaoM1=max(IaoM)

* Absolute H�ufigkeit der Nichtmarkierung
by persnr: gen IaoM2=_N if IaoM1==.

* Sollten die Absoluten H�ufigkeiten identisch sein, bedeutet das �ber die gesamte Historie keine Angabe f�r den Arbeitsort gab und bzw oder nur in nicht Grenznahen und neuen Bundesl�ndern gerarbeitet wurde
drop if IaoM2==Ipers


* F�r Arbeitsregion aus der Vergangenheit/Zukunft sollten bnn �bereinstimmen dann ist der Arbeitgeber nicht umgezogen "Annahme Arbeitgeber ist eher immobil"

count if ao_region==.n

sort persnr begepi

*tab persnr

*3749 max Spell Anzahl eines Individuums

forvalues i=1(1)3749{
by persnr: replace ao_region=ao_region[_n-`i'] if ao_region==.n & ao_region[_n-`i']!=.n & bnn==bnn[_n-`i'] & persnr==persnr[_n-`i']
}


forvalues i=1(1)3749{
by persnr: replace ao_region=ao_region[_n+`i'] if ao_region==.n & ao_region[_n+`i']!=.n & bnn==bnn[_n+`i'] & persnr==persnr[_n+`i']
}
 
count if ao_region==.n


