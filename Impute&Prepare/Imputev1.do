forvalues i=1980/2010 {
	cnreg ltentgelt age age2 bildyear bildyear2 if year==`i', cens(censwest) 
	predict xb, xb
	gen alpha=(ln(dailywest)-xb)/_b[_se] if censwest==1 & year==`i'
	gen imputtest=ltentgelt if year==`i'
	replace imputtest = invnorm(uniform()*(1-norm(alpha))+norm(alpha)) * _b[_se] if year==`i'
	xb if cen==1 & year==`i'
	drop xb alpha 
	
	}


gen bland=int(ao_region /1000)
	
	
	
	
	
	
	
	
	
gen maxwest=dailywest *0.98
gen censwest=1
replace censwest =1 if tentgelt_gr>=maxwest & ost==0

gen maxost=dailyost *0.98
gen censost=0
replace censost=1 if tentgelt_gr>=maxost & ost==1

tab IP2A, gen(bdum)
gen dumage1 = age*bdum1
gen dumage2 = age*bdum2
gen dumage3 = age*bdum3
gen dumage4 = age*bdum4
gen dumage5 = age*bdum5
gen dumage6 = age*bdum6


forvalues i=1980/2010 {
	imputw ltentgelt age age2 bdum1 bdum3 bdum4 bdum5 bdum6 dumage1 dumage3 dumage4 dumage5 dumage6 frau tempdeutsch if year==`i' & tentgelt_gr <. , cens(censwest) grenze(maxwest) outvar(imputwest2)
}

gen lnVPI=ln(VPI)
gen discount = lnVPI+imputwest2



imputw ltentgelt age age2 bdum1 bdum3 bdum4 bdum5 bdum6  frau tempdeutsch if year==1990 & tentgelt_gr <. , cens(censwest) grenze(maxwest) outvar(imputwest3)



replace imputwest = ltentgelt if imputwest >=. & censwest ==0

forvalues i=1980/2010 {
	imputw ltentgelt age age2 bildyear bildyear2 frau deutsch if year==`i' & tentgelt_gr <. & ost==1, cens(censost) grenze(maxost) outvar(imputost)
}
replace imputost = ltentgelt if imputost >=. & censost ==0 & ost==1

gen imput=imputwest if ost==0
replace imput=imputost if ost==1

replace imput=0 if imput>=.


