set more off
capture program drop fill

use "/u/home/jd1033/siab/siab_r_7510_v1.dta"
sort persnr begepi



// Generate additional variables
gen age= (year(begepi)-gebjahr)
gen spellyear = year(begepi)
replace tentgelt_gr=0 if erwstat==31 & tentgelt_gr >=.

// Find overlapping spells

gen workft=0
replace workft=1 if inlist(stib, 1,2,3,4,7)
gen overl = 0
by persnr: replace overl=1 if begepi[_n+1] <= endepi[_n] & work==0 & persnr[_n] == persnr[_n+1]

by persnr: replace overl=1 if endepi[_n-1] >= begepi[_n] & work==0 & persnr[_n-1] == persnr[_n]

//Drop overlapping spells, keep those with higher tentgelt

drop if overl==1 & stib >=.
//drop if overl==1 & overl[_n+1] ==1 & tentgelt[_n] <= tentgelt[_n+1]
drop if overl==1 & overl[_n+1] ==1 & tentgelt_gr[_n] <= tentgelt_gr[_n+1]
//drop if overl==1 & overl[_n-1] ==1 & tentgelt[_n] <= tentgelt[_n-1]
drop if overl==1 & overl[_n-1] ==1 & tentgelt_gr[_n] <= tentgelt_gr[_n-1]


//Define program to fill up continuous missing values

program define fill
gsort persnr begepi
replace `1'=`1'[_n-1] if `1' >=. & `1'[_n-1] <. & persnr==persnr[_n-1] 
gsort -persnr -begepi
replace `1'=`1'[_n-1] if `1' >=. & `1'[_n-1] <. & persnr==persnr[_n-1] 
gsort persnr begepi
end

//Apply fill program to variables

sort persnr begepi
fill ao_region
fill schbild
fill frau
fill deutsch
fill bild

// replace ao_region=ao_region[_n-1] if ao_region >=. & ao_region[_n-1] <. & persnr==persnr[_n-1] 
// gsort -persnr -begepi
// replace ao_region=ao_region[_n-1] if ao_region >=. & ao_region[_n-1] <. & persnr==persnr[_n-1] 
// gsort persnr begepi

//Drop uneccessary variables

//drop grund_gr
//drop estatvor
//drop lart
// drop kundengr
// drop restanspruch
// drop traeger

//Mark labor force participants 

gen LF=0
replace LF=1 if inlist(erwstat_gr, 1, 2, 3, 4, 5, 6, 7, 23)

//Generate weights by share of the year spent working

gen workdays = (endepi-begepi)
gen sweight = (1/365)*(workdays)
gen ltentgelt = ln(tentgelt_gr)
gen yearwage = workdays*tentgelt_gr

//Missing discrete

replace pendler = 3 if spellyear >= 1999 & pendler >= . 
replace bild = 50 if bild >=.
replace beruf_gr = 150 if beruf_gr >=.
replace stib = 20 if stib >=.
replace ao_region = 1 if ao_region >=.
replace deutsch = 3 if deutsch >=.
replace grund_gr = 101 if grund_gr >=.



// log using SIABaufbServerDesc.log

// bysort frau: tab workft if year(begepi) == 1992
// bysort frau: tab workft if year(begepi) == 2000
// bysort frau: tab workft if year(begepi) == 2008

// bysort frau: tab LF if year(begepi) == 1992
// bysort frau: tab LF if year(begepi) == 2000
// bysort frau: tab LF if year(begepi) == 2008

mdesc

close log