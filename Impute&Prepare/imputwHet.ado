	version 8
program 	define imputw, byable(recall)
	syntax varlist [if] , Cens(varlist) Grenze(varlist) [Outvar(string asis)]

      marksample touse
      if "`outvar'" == "" {
      local outvar "lnw_i" 
      }

	cnreg `varlist' if `touse'  , censored(`cens')

	quietly {
	predict xb00 if `touse'  , xb

	gen alpha00=(ln(`grenze')-xb00)/_b[_se] if `touse'  
       }

	cap gen  `outvar'=.
	replace `outvar'=`1' if `touse'  

	replace `outvar'=xb00+_b[_se] * invnorm(uniform()*(1-norm(alpha00))+norm(alpha00)) if `touse'   & `cens'
 	
	drop xb00 alpha00
  end

