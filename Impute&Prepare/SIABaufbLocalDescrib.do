set more off
capture program drop fill

use "C:\Documents\Computing\DATA\SIAB7510_v1_Testdaten_Stata\SIAB_7510_v1.dta"


sort persnr begepi



// Generate additional variables
gen age= (year(begepi)-gebjahr)
gen year = year(begepi)
gen bland = wo_bula
gen ost=0
replace ost=1 if bland>=11
gen age2 = age*age
gen bild2 = bild*bild

gen deutsch=0
replace deutsch=1 if nation==0

replace tentgelt=0 if erwstat==31 | erwstat==33 & tentgelt >=.

// Find overlapping spells

gen workft=0
replace workft=1 if inlist(stib, 1,2,3,4,7)
gen overl = 0
by persnr: replace overl=1 if begepi[_n+1] <= endepi[_n] & work==0 & persnr[_n] == persnr[_n+1]

by persnr: replace overl=1 if endepi[_n-1] >= begepi[_n] & work==0 & persnr[_n-1] == persnr[_n]

//Drop overlapping spells, keep those with higher tentgelt

drop if overl==1 & stib >=.
//drop if overl==1 & overl[_n+1] ==1 & tentgelt[_n] <= tentgelt[_n+1]
drop if overl==1 & overl[_n+1] ==1 & tentgelt[_n] <= tentgelt[_n+1]
//drop if overl==1 & overl[_n-1] ==1 & tentgelt[_n] <= tentgelt[_n-1]
drop if overl==1 & overl[_n-1] ==1 & tentgelt[_n] <= tentgelt[_n-1]




//Define program to fill up continuous missing values

program define fill
replace `1'=`1'[_n-1] if `1' >=. & `1'[_n-1] <. & persnr==persnr[_n-1] 
gsort -persnr -begepi
replace `1'=`1'[_n-1] if `1' >=. & `1'[_n-1] <. & persnr==persnr[_n-1] 
gsort persnr begepi
end

//Apply fill program to variables

sort persnr begepi
fill frau
fill deutsch
fill bild
fill bland


//Mark labor force participants 

gen LF=0
replace LF=1 if inlist(erwstat, 1, 2, 3, 4, 5, 6, 7, 23)

//Generate weights by share of the year spent working

gen workdays = (endepi-begepi)
gen sweight = (1/365)*(workdays)
gen ltentgelt = ln(tentgelt)
gen yearwage = workdays*tentgelt
//Missing discrete

//replace pendler = 3 if spellyear >= 1999 & pendler >= . 
//label define pendlerlabel 3 "missing" 1 "Pendler" 0 "kein Pendler"
//label values pendler pendlerlabel
replace bild = 50 if bild >=.
replace beruf = 150 if beruf >=.
replace stib = 20 if stib >=.
replace deutsch = 3 if deutsch >=.
//replace grund = 101 if grund >=.

// merge m:1 year using "C:\Documents\Computing\DATA\SIAB7510_v1_Testdaten_Stata\Bemessungsgrenzen.dta"

// gen dailywest=YearlyWest/365
// gen dailyost=YearlyOst/365
recode bild (1=9) (2=12) (3=13) (4=16) (5=17.5) (6=18) (21=9) (22=12) (23=12) (24=13) (25=13) (26=17.5) (27=18), gen(bildyear)
gen bildyear2=bildyear*bildyear

// gen maxwest=dailywest *0.98
// gen censwest=1
// replace censwest =1 if tentgelt>=maxwest & ost==0

// gen maxost=dailyost *0.98
// gen censost=0
// replace censost=1 if tentgelt>=maxost & ost==1

// forvalues i=1980/2010 {
	// imputw ltentgelt age age2 bildyear bildyear2 frau deutsch if year==`i' & tentgelt <. & ost==0 , cens(censwest) grenze(maxwest) outvar(imputwest)
// }

// replace imputwest = ltentgelt if imputwest >=. & censwest ==0

// forvalues i=1980/2010 {
	// imputw ltentgelt age age2 bildyear bildyear2 frau deutsch if year==`i' & tentgelt <. & ost==1, cens(censost) grenze(maxost) outvar(imputost)
// }
// replace imputost = ltentgelt if imputost >=. & censost ==0 & ost==1

// gen imput=imputwest if ost==0
// replace imput=imputost if ost==1

// replace imput=0 if imput>=.

mdesc

close log