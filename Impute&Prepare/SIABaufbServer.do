set more off
capture program drop fill
sort persnr begepi

gen workft=0
replace workft=1 if inlist(stib, 1,2,3,4,7)
gen overl = 0
by persnr: replace overl=1 if begepi[_n+1] <= endepi[_n] & work==0 & persnr[_n] == persnr[_n+1]

by persnr: replace overl=1 if endepi[_n-1] >= begepi[_n] & work==0 & persnr[_n-1] == persnr[_n]

*testing
*gen test2=0
drop if overl==1 & stib >=.
*drop if overl==1 & overl[_n+1] ==1 & tentgelt[_n] <= tentgelt[_n+1]
drop if overl==1 & overl[_n+1] ==1 & tentgelt_gr[_n] <= tentgelt_gr[_n+1]
*drop if overl==1 & overl[_n-1] ==1 & tentgelt[_n] <= tentgelt[_n-1]
drop if overl==1 & overl[_n-1] ==1 & tentgelt_gr[_n] <= tentgelt_gr[_n-1]


program define fill
replace `1'=`1'[_n-1] if `1' >=. & `1'[_n-1] <. & persnr==persnr[_n-1] 
gsort -persnr -begepi
replace `1'=`1'[_n-1] if `1' >=. & `1'[_n-1] <. & persnr==persnr[_n-1] 
gsort persnr begepi
end

sort persnr begepi
fill ao_region
fill schbild


* replace ao_region=ao_region[_n-1] if ao_region >=. & ao_region[_n-1] <. & persnr==persnr[_n-1] 
* gsort -persnr -begepi
* replace ao_region=ao_region[_n-1] if ao_region >=. & ao_region[_n-1] <. & persnr==persnr[_n-1] 
* gsort persnr begepi

drop grund_gr
drop estatvor
drop lart
drop kundengr
drop restanspruch
drop traeger

keep if inlist(stib, 1, 2, 3, 4, 7, 8, 9) | (stib >=. & tentgelt_gr > 0)
drop if tentgelt_gr >=.
*drop if tentgelt >=.
drop if inlist(erwstat_gr, 1, 2)

gen age= (year(begepi)-gebjahr)

mdesc