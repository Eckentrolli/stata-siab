set more off
sort persnr begepi
*Absolute Häufigkeit der Personnr.
by persnr: generate Ipers=_N 
*Absolute Häufigkeit der jeweiligen Region (Berlin gehört zu alt)
gen markOFS=1 if ao_region<=11000
gen markNFS=1 if ao_region>11000
sort persnr markOFS
by persnr markOFS: generate aoOFS=_N if markOFS==1
sort persnr markNFS
by persnr markNFS: generate aoNFS=_N if markNFS==1
*Sollten die oben genannten Häufigkeiten übereinstimmen haben diese nur in den beiden Regionen gearbeitet
gen NOFS=1 if aoOFS==Ipers
replace NOFS=0 if NOFS==.
gen NNFS=1 if aoNFS==Ipers
replace NNFS=0 if NNFS==.
*Sollten von n-1 zu n eine Region verändert wurden sein hat Sie die Region gewechselt (bezieht sich erstmal nur auf Vollbeschäftigte) direkter Wechsel
by persnr: generate POFS=1 if ao_region[_n-1]>11001 & ao_region[_n]<11001 & ao_region[_n-1]!=.n & ao_region[_n]!=.n & stib<=4 & erwstat==1 & persnr[_n-1]==persnr[_n]
replace POFS=0 if POFS==.
by persnr: generate PNFS=1 if ao_region[_n-1]<11001 & ao_region[_n]>11001 & ao_region[_n-1]!=.n & ao_region[_n]!=.n & stib<=4 & erwstat==1 & persnr[_n-1]==persnr[_n]
replace PNFS=0 if PNFS==.
*Mit Arbeitslosigkeit etc zwischen drin indirekter Wechsel
set more off
sort persnr begepi
forvalue i=2(1)3749{
by persnr: gen POFS`i'=1 if ao_region[_n-`i']>11001 & ao_region[_n]<11001 & persnr[_n-`i']==persnr[_n] & ao_region[_n-`i']!=.n & ao_region[_n]!=.n & POFS[_n]!=1 & ao_region[_n-`i']!=ao_region[_n-`i'+1] 
replace POFS`i'=0 if POFS`i'==.
}

forvalue i=3(1)3749{
by persnr: replace POFS2=1 if POFS`i'==1
}

forvalue i=3(1)3749{
drop POFS`i'
}

set more off

sort persnr begepi
forvalue i=2(1)3749{
by persnr: gen PNFS`i'=1 if ao_region[_n-`i']<11001 & ao_region[_n]>11001 & persnr[_n-`i']==persnr[_n] & ao_region[_n-`i']!=.n & ao_region[_n]!=.n & PNFS[_n]!=1 & ao_region[_n-`i']!=ao_region[_n-`i'+1] 
replace PNFS`i'=0 if PNFS`i'==.
}

forvalue i=3(1)3749{
by persnr: replace PNFS2=1 if PNFS`i'==1
}

forvalue i=3(1)3749{
drop PNFS`i'
}

gen Wechsel=.
replace Wechsel=1 if ao_region>11001
replace Wechsel=2 if ao_region<11001
replace Wechsel=. if ao_region==.n
by persnr: replace Wechsel=Wechsel[_n-1] if Wechsel[_n]==.
by persnr: gen POFSa=1 if Wechsel[_n-1]==1 & Wechsel[_n]==2
replace POFSa=0 if POFSa==.
by persnr: gen PNFSa=1 if Wechsel[_n]==2 & Wechsel[_n+1]==1
replace PNFSa=0 if PNFSa==.

**Übergange anschuan
*edit persnr begepi ao_region Wechsel POFS POFS2 POFSa PNFS PNFS2 PNFSa stib erwstat if persnr==1414722

gen Start=Wechsel
* quietly sum Start
* local i1=r(N)
* local i3=1
* while `i1'>`i3'{
* quietly sum Start
* local i1=r(N)
* foreach X of varlist Start{
* by persnr: replace `X'=Start[_n-1] if Start[_n-1]!=. & persnr[_n]==persnr[_n-1]
* quietly sum Start
* local i2=r(N)
* }
* quietly sum Start
* local i2=r(N)
* local i3=`i2'
* } 

*wdh von Hand läuft nicht
* quietly sum Start
* local i1=r(N)
* local i3=1
* while `i1'>`i3'{
* quietly sum Start
* local i1=r(N)
* foreach X of varlist Start{
* by persnr: replace `X'=Start[_n+1] if Start[_n+1]!=. & persnr[_n]==persnr[_n+1]
* quietly sum Start
* local i2=r(N)
* }
* local i3=`i2'
* } 
* }


replace ao_region=ao_region[_n-1] if ao_region >=. & ao_region[_n-1] <. & persnr==persnr[_n-1] 
gsort -persnr -begepi
replace ao_region=ao_region[_n-1] if ao_region >=. & ao_region[_n-1] <. & persnr==persnr[_n-1] 
gsort persnr begepi

sort persnr ao_region
by persnr: gen Iao=_N if ao_region==.n
drop if Ipers==Iao & ao_region==.n

*sort persnr ao_region
*by persnr ao_region: generate Iao=_N if ao_region==.n 
*drop if Ipers==Iao & ao_region==.n 
