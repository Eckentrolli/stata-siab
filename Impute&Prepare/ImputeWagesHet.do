set more off
clear
cap log close
log using "/u/home/jd1033/siab/logs/ImputeWagesHet.log" , replace

use "/u/home/jd1033/siab/CHK/siab_7510_CHK_WestIP2A.dta"

*top-censoring
merge m:1 year using "/u/home/jd1033/siab/BemessungsgrenzenNeu.dta"

drop if _merge == 2
drop _merge
replace censwestNEW = (censwestNEW*0.51129) if year <= 2001


*marginal employment

merge m:1 year using "/u/home/jd1033/siab/geringf.dta"
drop if _merge==2

*safety margin because of rounding
gen censored = 0
replace censored = 1 if tentgelt_gr >= (censwestNEW*0.98)

gen lwage1 = ltentgelt
replace lwage1 = ln(censwestNEW*0.98) if censored

gen lncensvar = ltentgelt
replace lncensvar = . if censored


gen lncenspoint = ln(censwestNEW*0.98)

tab censored 
mdesc ltentgelt

hist ltentgelt if frau==0 , name(originaldata_males)
graph save originaldata_males "/u/home/jd1033/siab/Impute&prepare/graphs/originaldata_males.gph", replace

hist ltentgelt if frau==1 , name(originaldata_females)
graph save originaldata_females "/u/home/jd1033/siab/Impute&prepare/graphs/originaldata_females.gph" , replace


*restrict to prime working age 20-60
*keep if age >=20 & age <= 60

*small sample for testing:
*sample 10


set emptycells drop
set matsize 1000

*define ML program

capture program drop mycens_lf
program mycens_lf

	args lnfj xb lnsigma
	
    quietly replace `lnfj' = ln(1-normal(($ML_y2 -`xb')/exp(`lnsigma')))   if  $ML_y1 >= $ML_y2
    quietly replace `lnfj' = ln(normalden($ML_y1,`xb', exp(`lnsigma')))    if  $ML_y1 <  $ML_y2

end


cap drop imput
cap gen imput=.
replace imput=ltentgelt

****males first****

forvalues i=1985/2010 {
	cap graph drop _all
	*intreg lwage1 lncensvar c.age c.age#c.age i.bildcat i.bildcat#c.age i.bildcat#c.age#c.age i.deutsch i.deutsch#i.bildcat i.deutsch#c.age if frau==0 & year==`i', iterate(200)   het(c.age c.age#c.age i.bildcat i.bildcat#c.age i.bildcat#c.age#c.age i.deutsch i.deutsch#i.bildcat i.deutsch#c.age)  

	*outreg using "/u/home/jd1033/siab/Impute&prepare/output/intreg_het_`i'.doc", se replace
	*cap drop yhatintreg
	*predict yhatintreg  

	*hist yhatintreg if frau==0 & year==`i', name(intreggraph_`i')
	*graph save intreggraph_`i' "/u/home/jd1033/siab/Impute&prepare/graphs/intreggraph_`i'.gph" ,replace
	
	
	cap drop yhat
	
	ml model lf mycens_lf (xb: lwage1 lncenspoint= c.age c.age#c.age i.bildcat i.bildcat#c.age i.bildcat#c.age#c.age i.deutsch i.deutsch#i.bildcat i.deutsch#c.age) (lnsigma: c.age c.age#c.age i.bildcat i.bildcat#c.age i.bildcat#c.age#c.age i.deutsch i.deutsch#i.bildcat i.deutsch#c.age) if frau==0 & year==`i'
	
	ml search
	ml maximize
		
	outreg using "/u/home/jd1033/siab/Impute&prepare/output/out_ML_het_`i'.doc", se replace
	
	_predict yhat if frau==0 & year==`i', equation(xb)

	cap drop lnsigmahat
	_predict lnsigmahat if frau==0 & year==`i', equation(lnsigma)

	cap drop sigmahat
	gen sigmahat = exp(lnsigmahat) if frau==0 & year==`i'
	
	gen alpha00=(ln(censwestNEW*0.98)-yhat)/sigmahat 
		
	replace imput=yhat+ sigmahat * invnormal(runiform()*(1-normal(alpha00))+normal(alpha00)) if censored & frau==0 & year==`i'
 	
	drop alpha00
	
	hist imput if frau==0 & year==`i', name(imputegraph_`i')

	graph save imputegraph_`i' "/u/home/jd1033/siab/Impute&prepare/graphs/imputegraph_`i'.gph" , replace

	
}


***females***

forvalues i=1985/2010 {
	cap graph drop _all
		
	cap drop yhat
	
	ml model lf mycens_lf (xb: lwage1 lncenspoint= c.age c.age#c.age i.bildcat i.bildcat#c.age i.bildcat#c.age#c.age i.deutsch i.deutsch#i.bildcat i.deutsch#c.age) (lnsigma: c.age c.age#c.age i.bildcat i.bildcat#c.age i.bildcat#c.age#c.age i.deutsch i.deutsch#i.bildcat i.deutsch#c.age) if frau==1 & year==`i'
	
	ml search
	ml maximize
		
	outreg using "/u/home/jd1033/siab/Impute&prepare/output/out_ML_het_F_`i'.doc", se replace
	
	_predict yhat if frau==1 & year==`i', equation(xb)

	cap drop lnsigmahat
	_predict lnsigmahat if frau==1 & year==`i', equation(lnsigma)

	cap drop sigmahat
	gen sigmahat = exp(lnsigmahat) if frau==1 & year==`i'
	
	gen alpha00=(ln(censwestNEW*0.98)-yhat)/sigmahat 
		
	replace imput=yhat+ sigmahat * invnormal(runiform()*(1-normal(alpha00))+normal(alpha00)) if censored & frau==1 & year==`i'
 	
	drop alpha00
	
	hist imput if frau==1 & year==`i', name(imputegraph_`i')

	graph save imputegraph_`i' "/u/home/jd1033/siab/Impute&prepare/graphs/imputegraph_F_`i'.gph" , replace

	
}
log close