set more off
clear

log using "/u/home/jd1033/siab/logs/ImputeHSKtesting" , replace

use "/u/home/jd1033/siab/CHK/siab_7510_CHK_WestIP2A.dta"

*top-censoring
merge m:1 year using "/u/home/jd1033/siab/BemessungsgrenzenNeu.dta"

drop if _merge == 2
drop _merge
replace censwestNEW = (censwestNEW*0.51129) if year <= 2001


*safety margin because of rounding:
gen censored = 0
replace censored = 1 if ltentgelt >= ln(censwestNEW*0.98)

gen lwage1 = ltentgelt
replace lwage1 = ln(censwestNEW*0.98) if censored

gen lncensvar = ltentgelt
replace lncensvar = . if censored

gen lncenspoint = ln(censwestNEW*0.98)

*sample already restricted to west-german fulltime workers, 
*restrict to prime working age 20-60
keep if age >=20 & age <= 60

*split into male and female samples


*take subsample for testing

preserve

keep if year==2005 & ltentgelt <. & frau==0

sample 20

hist ltentgelt, name(originaldata_males)
graph save originaldata_males "/u/home/jd1033/siab/Impute&prepare/graphs/originaldata_males.gph"


*intreg specification without heteroskedasticity

*lwage1 : daily wage in logs
*lncensvar : if below censoring point, this is daily wage, if above, missing

*intreg lwage1 lncensvar age age##age i.bildcat i.bildcat#c.age i.deutsch#c.age i.deutsch#i.bildcat i.deutsch , iterate(200)           

*predict yhatintreg

*cap drop imput alpha00
*gen alpha00=(ln(censwestNEW*0.98)-yhatintreg)/e(sigma)
*cap gen  imput=.
*	replace imput=ltentgelt  

*	replace imput=yhatintreg+ e(sigma) * invnormal(runiform()*(1-normal(alpha00))+normal(alpha00)) if censored
 	
*	drop alpha00

*hist imput, name(intreggraph)

*intreg specification with heteroskedasticity
set emptycells drop
set matsize 1000

intreg lwage1 lncensvar c.age c.age#c.age i.bildcat i.bildcat#c.age i.bildcat#c.age#c.age i.deutsch i.deutsch#i.bildcat i.deutsch#c.age , iterate(200)   het(c.age c.age#c.age i.bildcat i.bildcat#c.age i.bildcat#c.age#c.age i.deutsch i.deutsch#i.bildcat i.deutsch#c.age)  

outreg using "/u/home/jd1033/siab/Impute&prepare/output/out_intreg_het_test.doc", se replace

predict yhatintreg

hist yhatintreg, name(intreggraph_test1)
graph save intreggraph_test1 "/u/home/jd1033/siab/Impute&prepare/graphs/intreggraph_test1.gph"
graph drop_all

* Likelihood evaluator
* in $ML_y1 wird abhängige Variable übergeben
* in $ML_y2 wird Zensierungspoint übergeben

capture program drop mycens_lf
program mycens_lf

	args lnfj xb lnsigma
	
    quietly replace `lnfj' = ln(1-normal(($ML_y2 -`xb')/exp(`lnsigma')))   if  $ML_y1 >= $ML_y2
    quietly replace `lnfj' = ln(normalden($ML_y1,`xb', exp(`lnsigma')))    if  $ML_y1 <  $ML_y2

end


* Estimate model

ml model lf mycens_lf (xb: lwage1 lncenspoint= c.age c.age#c.age i.bildcat i.bildcat#c.age i.bildcat#c.age#c.age i.deutsch i.deutsch#i.bildcat i.deutsch#c.age) (lnsigma: c.age c.age#c.age i.bildcat i.bildcat#c.age i.bildcat#c.age#c.age i.deutsch i.deutsch#i.bildcat i.deutsch#c.age)

* wo wagecensored steht: die zensierte Lohnvariable
* wo censpoint steht: die Variable die die Zensierungsgrenze enthält
* wo educ exper steht: die Liste der Regressoren für die Regression
* wo educ steht: die Liste der Regressoren für die Varianz/Heteroskedastie

ml search
ml maximize

outreg using "/u/home/jd1033/siab/Impute&prepare/output/out_ML_het_test.doc", se replace


* Predictions

cap drop yhat
_predict yhat, equation(xb)

cap drop lnsigmahat
_predict lnsigmahat, equation(lnsigma)

cap drop sigmahat
gen sigmahat = exp(lnsigmahat)


cap drop imput alpha00
gen alpha00=(ln(censwestNEW*0.98)-yhat)/sigmahat
cap gen  imput=.
	replace imput=ltentgelt  

	replace imput=yhat+ sigmahat * invnormal(runiform()*(1-normal(alpha00))+normal(alpha00)) if censored
 	
	drop alpha00

hist imput, name(imputegraph_test)

graph save imputegraph_test "/u/home/jd1033/siab/Impute&prepare/graphs/imputegraph_test.gph" , replace



*cnreg testing
cnreg lwage1 age bildyear, censored(censored)            

predict yhatcnreg
hist yhatcnreg, name(yhatcnreg)

















