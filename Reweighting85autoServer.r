#Reweighting scheme to create education/experience weights similar to baseyear 1985

options(scipen=30)

library(np)
library(Hmisc)
library(parallel)
library(plyr)
library(car)

setwd("/u/home/jd1033/DFG-descriptives/Results/FT")

#Load data on west german workers without marginal workers and with imputed wages above the social security censoring threshold

load("/u/home/jd1033/DFG-descriptives/siab_r_7510_v1_forResiduals.RData")

#trim <- trim[sample(nrow(trim), 10000), ] 

#Remove superflous variables to reduce memory footprint
data <- data[!names(data) %in% c("quelle_gr","beruf_backup","gebjahr","VPI", "overl", "LF", "imputwest2", "cens", "workdays") ]


attach(data)
data$bild <- as.factor(IP2A)	#convert cetegorial variables into factors
data$deutsch <- as.factor(deutsch)
data$beruf_gr <- as.factor(beruf_gr)

data$potexp2 <- potexp*potexp	
data$potexp3 <- potexp*potexp*potexp
detach(data)

#Create subsample for west german full time workers age 18-62
trim <- subset(data,  workft==1 & age>=18 )

rm(data)

trim$bild <- C(trim$bild, contr.treatment, base=3)
trim$count <- c(1:length(trim$discount))

years <- unique(trim$year)
years <- sort(years, decreasing=TRUE)

#male and female subsamples 
trim.m <- subset(trim, frau==0 & age >= 18 & age <=62)
trim.f <- subset(trim, frau==1 & age >= 18 & age <=62)

rm(trim)

years2 <- years[years >1985]

#Define function for calculating composition-weights according to baseyear 1985:

reweight85 <- function(i) {

	pooled <- subset(trim.m, year==1985 | year==i ) #take pooled sample of baseyear and "target-year" data

	n <- nrow(pooled)
	nbase <- nrow(pooled[pooled$year==1985,])
	ndest <- nrow(pooled[pooled$year==i,])
	
	comp.weight <- nbase/ndest
	
	pooled$comp.weight <- 1
	pooled[pooled$year==i,]$comp.weight <- comp.weight
	
		
	#Create year dummies
	pooled$dyear <-  pooled$year
	pooled$dbase <-  pooled$year

	pooled$dyear[pooled$dyear==1985] <- 0
	pooled$dyear[pooled$dyear==i] <- 1
	pooled$dbase[pooled$dbase==1985] <- 1
	pooled$dbase[pooled$dbase==i] <- 0

	attach(pooled)
	#Probit of likelihood of being in 2010, given charactersistics
	results <- glm(dyear ~ potexp+potexp2+potexp3+bild+potexp:bild+potexp2:bild+potexp3:bild, data=pooled, weights=comp.weight ,family="binomial")
	detach(pooled)
	#summary(results)
	
	rm(pooled)
	#combine values & fits ainto dataframe
	rw.data <- cbind(results$data, fitted.values=results$fitted.values )

	rm(results)

	#Calculate reweighting factor. (1-P)/P small if P large (e.g. highly educated)
	rw.data$base85 <- ((1-rw.data$fitted.values)/rw.data$fitted.values)


	#remove superfluous count columns
	rw.data <- rw.data[!names(rw.data) %in% c("dyear", "dbase","sweight.2","count.1", "count.2" ) ]
	save(rw.data, file=paste("reweighting/rw",i,".RData" , sep=""))
	#res <- rw.data[!names(rw.data) %in% "fitted.values"]
	
	if (i==years2[length(years2)]) { 
		res <- rw.data 
		} 	else {
		res <- subset(rw.data, year!=1985) 
		}
	
	rm(rw.data)
	
	return(res)

}



results.85  <- mclapply(years2,function(x) reweight85(x), mc.cores=3)

trim.m.rw <- results.85[[1]] 

for(i in 2:length(years2)) {
	temp.2 <- results.85[[i]]
	trim.m.rw <- rbind(trim.m.rw, temp.2)
	}
	
	
#set CF weights for baseyear to 1
trim.m.rw[trim.m.rw$year==1985,]$base85 <- 1


save(trim.m.rw, file="reweighting/trim.m.rw85.RData")


rm(results.85,trim.m.rw)

#FEMALES

reweight85.f <- function(i) {

	#calculation for base year 1985 to 2010

	pooled <- subset(trim.f, year==1985 | year==i )

	n <- nrow(pooled)
	nbase <- nrow(pooled[pooled$year==1985,])
	ndest <- nrow(pooled[pooled$year==i,])
	
	comp.weight <- nbase/ndest
	
	pooled$comp.weight <- 1
	pooled[pooled$year==i,]$comp.weight <- comp.weight
	
		
	#Create year dummies
	pooled$dyear <-  pooled$year
	pooled$dbase <-  pooled$year

	pooled$dyear[pooled$dyear==1985] <- 0
	pooled$dyear[pooled$dyear==i] <- 1
	pooled$dbase[pooled$dbase==1985] <- 1
	pooled$dbase[pooled$dbase==i] <- 0

	attach(pooled)
	#Probit of likelihood of being in 2010, given charactersistics
	results <- glm(dyear ~ potexp+potexp2+potexp3+bild+potexp:bild+potexp2:bild+potexp3:bild, data=pooled, weights=comp.weight ,family="binomial")
	detach(pooled)
	#summary(results)
	
	rm(pooled)
	#combine values & fits ainto dataframe
	rw.data <- cbind(results$data, fitted.values=results$fitted.values )

	rm(results)

	#Calculate reweighting factor. (1-P)/P small if P large (e.g. highly educated)
	rw.data$base85 <- ((1-rw.data$fitted.values)/rw.data$fitted.values)


	#cbind(rw.data$fitted.values[1:20],rw.data$base85[1:20])

	#remove superfluous count columns
	rw.data <- rw.data[!names(rw.data) %in% c("dyear", "dbase","sweight.2","count.1", "count.2" ) ]
	save(rw.data, file=paste("reweighting/rw",i,".RData" , sep=""))
	#res <- rw.data[!names(rw.data) %in% "fitted.values"]
	
	if (i==years2[length(years2)]) { 
		res <- rw.data 
		} 	else {
		res <- subset(rw.data, year!=1985) 
		}
	
	rm(rw.data)
	
	return(res)

}


results.85  <- mclapply(years2,function(x) reweight85.f(x), mc.cores=3)

names(results.85) <- years2
trim.f.rw <- results.85[[1]] 

for(i in 2:length(years2)) {
	temp.2 <- results.85[[i]]
	trim.f.rw <- rbind(trim.f.rw, temp.2)
	}
	
	
#set CF weights for baseyear to 1
trim.f.rw[trim.f.rw$year==1985,]$base85 <- 1


save(trim.f.rw, file="reweighting/trim.f.rw85.RData")
