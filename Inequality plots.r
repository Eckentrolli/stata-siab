options(scipen=30)

library(foreign)
library(xtable)
library(Hmisc)
library(parallel)
library(car)

setwd("C:/Documents/Computing/Stata-siab")
data <- read.dta("E:/Data/siab_r_7510_v1_testdata.dta", convert.factors=FALSE)
#preparation
attach(data)

data$bildcat <- as.factor(bildcat)
data$deutsch <- as.factor(deutsch)

data$age3 <- age*age*age

detach(data)

trim <- subset(data,  discount> 1 & workft==1 & ost==0 & geringf == 0 )

rm(data)

#Remove marginal jobs!!!!!
describe(trim$year)

years <- unique(trim$year)
years <-sort(years,decreasing=TRUE)

variances <- unlist(mclapply(years, function(x) var(trim[ which(trim$year==x),]$discount)))

quantiles <- mclapply(years, function(x) quantile(trim[ which(trim$year==x),]$discount, prob=c(0.1, 0.5, 0.9)))

quant2 <- matrix(nrow=length(years), ncol=3)
colnames(quant2) <- c("ten","fifty","ninety")
for(i in 1:length(years)) {
quant2[i,] <- unlist(quantiles[i])
}
 
descript1 <- cbind(years,variances, quant2)
descript1 <- as.data.frame(descript1)
attach(descript1)
descript1$fifty10 <- fifty/ten
descript1$ninety50 <- ninety/fifty
descript1$ninety10 <- ninety/ten
head(descript1)
detach(descript1)

plot(descript1$variances~years)
plot(descript1$fifty~years)

#reg-preparation

justyears <- unique(trim$year)
justfrau <- unique(trim$frau)
grid <- as.matrix(expand.grid(justyears, justfrau))

grid2 <- grid
colnames(grid)<-c("year","frau")


#define functions

cellreg <- function(i, j) {
	sample1 <- subset(trim, frau==j & year==i)
	#test<- summary(sample1)	
	res <- residuals(lm(discount ~ age+age2+age3+bildyear+age:bildcat+age2:bildcat+age3:bildcat, data=sample1,weights=sweight))
	rm(sample1)
	return(res)
}

grid3 <- numeric()
for (i in 1:dim(grid)[1]) {
	grid3 <- c(grid3, paste(as.character(grid[i,1]), as.character(grid[i,2]),sep="@"))
}

wrapper <- function(i) {
	x <- as.numeric(unlist(strsplit(i, split="@")))
	res2<-cellreg(x[1],x[2])
	return(res2)
}

#Mincer regression

results <- mclapply(grid3,function(x) wrapper(x))

#naming

grid2[grid2[,2]==0,][,2] <-"m"
grid2[grid2[,2]==1,][,2] <-"f"

resnames <- numeric()
for (i in 1:dim(grid2)[1]) {
	resnames <- c(resnames, paste(as.character(grid2[i,1]), as.character(grid2[i,2]),sep=""))
}

names(results) <- resnames 

variances <- mclapply(results, function(i) var(unlist(i)))

save.image(file = "Residuals.RData", compress=TRUE)


