options(scipen=30)

library(foreign)
library(Hmisc)
library(parallel)
library(car)

setwd("/u/home/jd1033/DFG-descriptives")
load("/u/home/jd1033/DFG-descriptives/siab_r_7510_v1_forResiduals.RData")

attach(data)

data$bild <- as.factor(IP2A)
data$deutsch <- as.factor(deutsch)



data$potexp2 <- potexp*potexp
data$potexp3 <- potexp*potexp*potexp
detach(data)

#Create subsample for west german full time workers with non-missing education
trim <- subset(data,  workft==1  )
rm(data)

trim$bild <- C(trim$bild, contr.treatment, base=3)
trim$count <- c(1:length(trim$discount))
trim$beruf_gr <- as.factor(trim$beruf_gr)

occs <- sort(unique(trim$beruf_gr))

years <- unique(trim$year)
years <- sort(years, decreasing=TRUE)

###################################################
###Define Functions################################
#################################################

calcWithinGroupsVariance <- function(variable,groupvariable,sweight)
  {
     # find out how many values the group variable can take
     levels <- levels(groupvariable)
     numlevels <- length(levels)
     # get the mean and standard deviation for each group:
     numtotal <- 0
     denomtotal <- 0
     for (i in 1:numlevels)
     {
        leveli <- levels[i]
        levelidata <- variable[groupvariable==leveli]
		leveliweights <- sweight[groupvariable==leveli]
        levelilength <- length(levelidata)
        # get the standard deviation for group i:
        sdi <- sqrt(wtd.var(levelidata, weights=leveliweights))
        numi <- (levelilength - 1)*(sdi * sdi)
        denomi <- levelilength
        numtotal <- numtotal + numi
        denomtotal <- denomtotal + denomi
     }
     # calculate the within-groups variance
     Vw <- numtotal / (denomtotal - numlevels)
     return(Vw)
  }
  
  calcBetweenGroupsVariance <- function(variable,groupvariable,sweight)
  {
     # find out how many values the group variable can take
     #groupvariable2 <- as.factor(groupvariable[[1]])
     levels <- levels(groupvariable)
     numlevels <- length(levels)
     # calculate the overall grand mean:
     grandmean <- wtd.mean(variable, weights=sweight)
     # get the mean and standard deviation for each group:
     numtotal <- 0
     denomtotal <- 0
     for (i in 1:numlevels)
     {
        leveli <- levels[i]
        levelidata <- variable[groupvariable==leveli]
		leveliweights <- sweight[groupvariable==leveli]
        levelilength <- length(levelidata)
        # get the mean and standard deviation for group i:
        meani <- wtd.mean(levelidata, weights=leveliweights)
        sdi <- sqrt(wtd.var(levelidata, weights=leveliweights))
        numi <- levelilength * ((meani - grandmean)^2)
        denomi <- levelilength
        numtotal <- numtotal + numi
        denomtotal <- denomtotal + denomi
     }
     # calculate the between-groups variance
     Vb <- numtotal / (numlevels - 1)
     Vb <- Vb[[1]]
     return(Vb)
  }
  
sample85.m <- subset(trim, trim$year==1985 & trim$frau==0)
sample10.m <- subset(trim, trim$year==2010 & trim$frau==0)

WGvar <- calcWithinGroupsVariance(sample85.m$discount, sample85.m$beruf_gr, sample85.m$sweight)
BGvar <- calcBetweenGroupsVariance(sample85.m$discount, sample85.m$beruf_gr, sample85.m$sweight)

