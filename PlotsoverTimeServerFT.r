options(scipen=30)

library(foreign)
library(Hmisc)
library(xtable)

#setwd("E:/Data")
#setwd("T:/")




load("/u/home/jd1033/DFG-descriptives/ResidualsFT.RData")
#load("/u/home/jd1033/DFG-descriptives/ResidualsSmallFT.RData")


setwd("/u/home/jd1033/DFG-descriptives/Results/FT")

###############################################
#########Variance Tables#####################
##############################################

x.table <- xtable(varsall, caption ="Variance and Residuals for all workers")
print(x.table, file="Tables/VarTotals.tex")
rm(x.table)

#males
x.table <- xtable(variances[1:31,], caption ="Variance and Residuals for male workers")
print(x.table, file="Tables/VarMalesFT.tex")
rm(x.table)

#females
x.table <- xtable(variances[32:62,], caption ="Variance and Residuals for female workers")
print(x.table, file="Tables/VarFem.tex")
rm(x.table)


################################################
######### Variance Plots #######################
################################################


#Plot variance for total sample
setEPS()
postscript("Graphs/TotalVar&Res.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(varsall[,1]~years, xlim=c(1980,2011),ylim=c(0.0,0.4), xaxt = "n",  main="Residual variance entire sample", xlab="Time", ylab="Variance / Residual variance", type="o", lwd=2, col="red")
axis(side=1, at=years, labels=years, cex.axis=0.9)
lines(varsall[,3]~years, type="o", col="blue", lwd=2)

legend(1980,0.4, c("Total Variance", "Residual Variance"), lty=c(1,1), lwd=c(2.5,2.5),col=c("blue","red")) # gives the legend lines the correct color and widt

dev.off()

#Plot variance for males only 

setEPS()
postscript("Graphs/MalesVar&Res.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(variances[1:31,1]~years, xlim=c(1985,2011),ylim=c(0.05,0.3), xaxt = "n",  main="Residual variance males", xlab="Time", ylab="Variance / Residual variance", type="o", lwd=2, col="red")
axis(side=1, at=years, labels=years, cex.axis=0.9)
lines(variances[1:31,3]~years, type="o", col="blue", lwd=2)

legend(1985,0.3, c("Total Variance", "Residual Variance"), lty=c(1,1), lwd=c(2.5,2.5),col=c("blue","red")) # gives the legend lines the correct color and widt


dev.off()

#PNG
png("Graphs/MalesVar&Res.png", width=900, height=700)
par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(variances[1:31,1]~years, xlim=c(1985,2011),ylim=c(0.05,0.3), xaxt = "n",  main="Residual variance males", xlab="Time", ylab="Variance / Residual variance", type="o", lwd=2, col="red")
axis(side=1, at=years, labels=years, cex.axis=0.9)
lines(variances[1:31,3]~years, type="o", col="blue", lwd=2)
legend(1985,0.3, c("Total Variance", "Residual Variance"), lty=c(1,1), lwd=c(2.5,2.5),col=c("blue","red")) # gives the legend lines the correct color and widt


dev.off()


#Plot variance for females only
setEPS()
postscript("Graphs/FemVar&Res.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(variances[32:62,1]~years, xlim=c(1985,2011),ylim=c(0.1,0.3), xaxt = "n",  main="Residual variance females", xlab="Time", ylab="Variance / Residual variance", type="o", lwd=2, col="red")
axis(side=1, at=years, labels=years, cex.axis=0.9)
lines(variances[32:62,3]~years, type="o", col="blue", lwd=2)

legend(1985,0.3, c("Total Variance", "Residual Variance"), lty=c(1,1), lwd=c(2.5,2.5),col=c("blue","red")) # gives the legend lines the correct color and widt


dev.off()

#PNG
png("Graphs/FemVar&Res.png", width=900, height=700)
par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(variances[32:62,1]~years, xlim=c(1985,2011),ylim=c(0.1,0.3), xaxt = "n",  main="Residual variance females", xlab="Time", ylab="Variance / Residual variance", type="o", lwd=2, col="red")
axis(side=1, at=years, labels=years, cex.axis=0.9)
lines(variances[32:62,3]~years, type="o", col="blue", lwd=2)
legend(1985,0.3, c("Total Variance", "Residual Variance"), lty=c(1,1), lwd=c(2.5,2.5),col=c("blue","red")) # gives the legend lines the correct color and widt


dev.off()


################################################
######### Gap-Plots ##########################
################################################

#Calculate Gaps for M/F separate

gaps <- data.frame()

for (i in 1:nrow(grid)) {

	quants<- wtd.quantile(results[[i]]$values, probs=c(0.85,0.5,0.15),na.rm=FALSE, weights=results[[i]]$sweight)
	resQuants <- wtd.quantile(results[[i]]$residualsmf, probs=c(0.85,0.5,0.15),na.rm=FALSE, weights=results[[i]]$sweight)

	eight50 <- (quants["85%"] - quants["50%"])
	eight15 <- (quants["85%"] - quants["15%"])
	five15 <- (quants["50%"] - quants["15%"])
	
	reseight50 <- (resQuants["85%"] - resQuants["50%"])
	reseight15 <- (resQuants["85%"] - resQuants["15%"])
	resfive15 <- (resQuants["50%"] - resQuants["15%"])
	
	gaps[i,1] <- eight50
	gaps[i,2] <- eight15
	gaps[i,3] <- five15
	gaps[i,4] <- reseight50
	gaps[i,5] <- reseight15
	gaps[i,6] <- resfive15
	gaps[i,7] <- quants["85%"]
	gaps[i,8] <- quants["50%"]
	gaps[i,9] <- quants["15%"]
	
}


colnames(gaps) <- c("eight50", "eight15", "five15", "reseight50", "reseight15", "resfive15","85% quantile", "Median", "15% quantile")
gaps <- cbind(grid, gaps)


#Calculate gaps for total population
gaps.total <- data.frame()

for (i in 1:length(years)) {

	quants<- wtd.quantile(resultsall[[i]]$valuesall, probs=c(0.85,0.5,0.15),na.rm=FALSE, weights=resultsall[[i]]$sweight)
	resQuants <- wtd.quantile(resultsall[[i]]$residualsall, probs=c(0.85,0.5,0.15),na.rm=FALSE, weights=resultsall[[i]]$sweight)

	eight50 <- (quants["85%"] - quants["50%"])
	eight15 <- (quants["85%"] - quants["15%"])
	five15 <- (quants["50%"] - quants["15%"])
	
	reseight50 <- (resQuants["85%"] - resQuants["50%"])
	reseight15 <- (resQuants["85%"] - resQuants["15%"])
	resfive15 <- (resQuants["50%"] - resQuants["15%"])
	
	gaps.total[i,1] <- eight50
	gaps.total[i,2] <- eight15
	gaps.total[i,3] <- five15
	gaps.total[i,4] <- reseight50
	gaps.total[i,5] <- reseight15
	gaps.total[i,6] <- resfive15
	gaps.total[i,7] <- quants["85%"]
	gaps.total[i,8] <- quants["50%"]
	gaps.total[i,9] <- quants["15%"]
	
}


colnames(gaps.total) <- c("eight50", "eight15", "five15", "reseight50", "reseight15", "resfive15","85% quantile", "Median", "15% quantile")
gaps.total <- cbind(years, gaps.total)


#Plot 85/15, 85/50, 50/15 gaps for total sample

setEPS()
postscript("Graphs/Gapstotal.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gaps.total[,"eight50"]~years, xlim=c(1980,2011),ylim=c(0.3,1.5), xaxt = "n",  main="Quantile gaps entire sample", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="red")
axis(side=1, at=years, labels=years, cex.axis=0.9)
lines(gaps.total[,"eight15"]~years, type="o", col="blue", lwd=2)
lines(gaps.total[,"five15"]~years, type="o", col="green", lwd=2)

legend(1980,1.5, c("85/15 gap","85/50 gap", "50/15 gap"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green")) # gives the legend lines the correct color and widt


#Table
	temptab <- xtable(gaps.total, caption="Quantiles total")
	print(temptab, include.rownames=FALSE, file="Tables/QuantTotals.tex")
dev.off()

#Plot 85/15, 85/50, 50/15 + resid gaps for males only 


#MALES

setEPS()
postscript("Graphs/Gapsmales.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gaps[1:31,"eight50"]~years, xlim=c(1980,2011),ylim=c(0.25,1.3), xaxt = "n",  main="Quantile gaps males only", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="red")
axis(side=1, at=years, labels=years, cex.axis=0.9)
lines(gaps[1:31,"eight15"]~years, type="o", col="blue", lwd=2)
lines(gaps[1:31,"five15"]~years, type="o", col="green", lwd=2)

legend(1980,1.3, c("85/15 gap","85/50 gap", "50/15 gap"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green"))

dev.off()


setEPS()
postscript("Graphs/ResGapsmales.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gaps[1:31,"reseight50"]~years, xlim=c(1980,2011),ylim=c(0.25,1.3), xaxt = "n",  main="Residual quantile gaps males only", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="red")
axis(side=1, at=years, labels=years, cex.axis=0.9)
lines(gaps[1:31,"reseight15"]~years, type="o", col="blue", lwd=2)
lines(gaps[1:31,"resfive15"]~years, type="o", col="green", lwd=2)

legend(1980,1.3, c("85/15 gap","85/50 gap", "50/15 gap"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green"))

dev.off()

#Table
temptab <- xtable(gaps[1:31,], caption="Residual quantiles Males")
print(temptab, include.rownames=FALSE, file="Tables/ResQuantMales.tex")
#Plot 85/15, 85/50, 50/15 gaps for females only 


#FEMALES

setEPS()
postscript("Graphs/ResGapsfemales.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gaps[32:62,"reseight50"]~years, xlim=c(1980,2011),ylim=c(0.1,1.1), xaxt = "n",  main="Residual quantile gaps females only", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="red")
axis(side=1, at=years, labels=years, cex.axis=0.9)
lines(gaps[32:62,"reseight15"]~years, type="o", col="blue", lwd=2)
lines(gaps[32:62,"resfive15"]~years, type="o", col="green", lwd=2)

legend(1980,1.1, c("85/15 gap","85/50 gap", "50/15 gap"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green"))

dev.off()


temptab <- xtable(gaps[32:62,], caption="Residual quantiles Females")
print(temptab, include.rownames=FALSE, file="Tables/ResQuantFemales.tex")


setEPS()
postscript("Graphs/Gapsfemales.eps", width=8, heigh=7)

par(mar=c(5.1, 5.5, 4.1, 2.1))
plot(gaps[32:62,"eight50"]~years, xlim=c(1980,2011),ylim=c(0.1,1.1), xaxt = "n",  main="Quantile gaps females only", xlab="Time", ylab="85/15, 85/50, 50/15 quantile gaps", type="o", lwd=2, col="red")
axis(side=1, at=years, labels=years, cex.axis=0.9)
lines(gaps[32:62,"eight15"]~years, type="o", col="blue", lwd=2)
lines(gaps[32:62,"five15"]~years, type="o", col="green", lwd=2)

legend(1980,1.1, c("85/15 gap","85/50 gap", "50/15 gap"), lty=c(1,1,1), lwd=c(2.5,2.5,2.5),col=c("blue","red","green"))

dev.off()


temptab <- xtable(gaps[32:62,], caption="Quantiles Females")
print(temptab, include.rownames=FALSE, file="Tables/QuantFemales.tex")

