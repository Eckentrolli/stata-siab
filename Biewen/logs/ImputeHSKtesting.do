set more off
clear

log using "/u/home/jd1033/siab/logs/ImputeHSKtesting" , replace

use "/u/home/jd1033/siab/CHK/siab_7510_CHK_WestIP2A.dta"

*top-censoring
merge m:1 year using "/u/home/jd1033/siab/BemessungsgrenzenNeu.dta"

drop if _merge == 2
drop _merge
replace censwestNEW = (censwestNEW*0.51129) if year <= 2001

gen age2 = age*age

*safety margin because of rounding:
gen censored = 0
replace censored = 1 if ltentgelt >= ln(censwestNEW*0.98)

gen lwage1 = ltentgelt
replace lwage1 = ln(censwestNEW*0.98) if censored

gen lncensvar = ltentgelt
replace lncensvar = . if censored


*sample already restricted to west-german fulltime workers, 
*restrict further and take subsample for testing

keep if year==2005 & ltentgelt <. & frau==0
sample 20

hist ltentgelt, name(originaldata)

*intreg specification without heteroskedasticity

*lwage1 : daily wage in logs
*lncensvar : if below censoring point, this is daily wage, if above, missing

intreg lwage1 lncensvar age age2 i.bildcat i.bildcat#c.age i.deutsch#c.age i.deutsch#i.bildcat i.deutsch , iterate(200)           

predict yhatintreg

cap drop imput alpha00
gen alpha00=(ln(censwestNEW*0.98)-yhatintreg)/e(sigma)
cap gen  imput=.
	replace imput=ltentgelt  

	replace imput=yhatintreg+ e(sigma) * invnormal(runiform()*(1-normal(alpha00))+normal(alpha00)) if censored
 	
	drop alpha00

hist imput, name(intreggraph)

*intreg specification with heteroskedasticity

intreg lwage1 lncensvar age age2 i.bildcat i.bildcat#c.age i.deutsch#c.age i.deutsch#i.bildcat i.deutsch , iterate(200)   het(age i.bildcat  i.deutsch)  

predict yhatintreg2









*cnreg testing
cnreg lwage1 age bildyear, censored(censored)            

predict yhatcnreg
hist yhatcnreg, name(yhatcnreg)




*ML approach
* Likelihood evaluator
* in $ML_y1 wird abhängige Variable übergeben
* in $ML_y2 wird Zensierungspoint übergeben

*censwestNEW : censoring point (not in logs)

capture program drop mycens_lf
program mycens_lf

	args lnfj xb lnsigma
	
    quietly replace `lnfj' = ln(1-normal(($ML_y2 -`xb')/exp(`lnsigma')))   if  $ML_y1 >= $ML_y2
    quietly replace `lnfj' = ln(normalden($ML_y1,`xb', exp(`lnsigma')))    if  $ML_y1 <  $ML_y2

end

* Estimate model

ml model lf mycens_lf (xb: ltentgelt censwestNEW= age age2 i.bildcat i.bildcat#c.age i.deutsch#c.age i.deutsch i.bildcat#c.age2 i.deutsch#c.age2  ) (lnsigma: age age2 i.bildcat i.deutsch ) 

ml search
ml maximize

cap drop yhat
_predict yhat, equation(xb)

sum yhat
hist yhat
log close




















