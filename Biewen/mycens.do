* use "WAGE2.DTA", clear
hist wage

cap drop censpoint
gen censpoint = 1000

cap drop wagecensored
gen wagecensored = wage
replace wagecensored = censpoint if wage>=censpoint


* Stata intreg

cap drop wage1
cap drop wage2

gen wage1=wage if wage<=censpoint
gen wage2=wage if wage<=censpoint

replace wage1=censpoint if wage>=censpoint
replace wage2=. if wage>=censpoint

intreg wage1 wage2 educ exp, het(educ)
reg wagecensored educ exp


* Likelihood evaluator
* in $ML_y1 wird abh�ngige Variable �bergeben
* in $ML_y2 wird Zensierungspoint �bergeben

capture program drop mycens_lf
program mycens_lf

	args lnfj xb lnsigma
	
    quietly replace `lnfj' = ln(1-normal(($ML_y2 -`xb')/exp(`lnsigma')))   if  $ML_y1 >= $ML_y2
    quietly replace `lnfj' = ln(normalden($ML_y1,`xb', exp(`lnsigma')))    if  $ML_y1 <  $ML_y2

end


* Estimate model

ml model lf mycens_lf (xb: wagecensored censpoint= educ exper) (lnsigma: educ)
* wo wagecensored steht: die zensierte Lohnvariable
* wo censpoint steht: die Variable die die Zensierungsgrenze enth�lt
* wo educ exper steht: die Liste der Regressoren f�r die Regression
* wo educ steht: die Liste der Regressoren f�r die Varianz/Heteroskedastie

ml search
ml maximize


* Predictions

cap drop yhat
_predict yhat, equation(xb)

cap drop lnsigmahat
_predict lnsigmahat, equation(lnsigma)

cap drop sigmahat
gen sigmahat = exp(lnsigmahat)



