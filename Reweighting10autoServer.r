#Reweighting scheme for comparison of 

options(scipen=30)

library(foreign)
library(Hmisc)
library(parallel)
library(plyr)

#setwd("E:/DATA")
setwd("/u/home/jd1033/DFG-descriptives/Results/FT")

#load("E:/DATA/Residuals.RData")
load("/u/home/jd1033/DFG-descriptives/ResidualsSmallFT.RData")

#trim <- trim[sample(nrow(trim), 10000), ] 

#results.bak <- results
rm(results, resultsall)

#male and female subsamples 
trim.m <- subset(trim, frau==0 & age >= 18 & age <=62)
trim.f <- subset(trim, frau==1 & age >= 18 & age <=62)

rm(trim)

years2 <- years[years <2010 & years >= 1985]


reweight10 <- function(i) {

	#calculation for base year 2010

	pooled <- subset(trim.m, year==2010 | year==i )

	n <- nrow(pooled)
	nbase <- nrow(pooled[pooled$year==2010,])
	ndest <- nrow(pooled[pooled$year==i,])
	
	comp.weight <- nbase/ndest
	
	pooled$comp.weight <- 1
	pooled[pooled$year==i,]$comp.weight <- comp.weight
	
		
	#Create year dummies
	pooled$dyear <-  pooled$year
	pooled$dbase <-  pooled$year

	pooled$dyear[pooled$year==2010] <- 0
	pooled$dyear[pooled$year==i] <- 1
	pooled$dbase[pooled$dbase==2010] <- 1
	pooled$dbase[pooled$dbase==i] <- 0

	attach(pooled)
	#Probit of likelihood of being in destination-year, given charactersistics
	results <- glm(dyear ~ potexp+potexp2+potexp3+bild+potexp:bild+potexp2:bild+potexp3:bild, data=pooled, weights=comp.weight ,family="binomial")
	detach(pooled)
	#summary(results)
	
	rm(pooled)
	#combine values & fits ainto dataframe
	rw.data <- cbind(results$data, fitted.values=results$fitted.values )

	rm(results)

	#Calculate reweighting factor. (1-P)/P small if P large (e.g. highly educated)
	rw.data$base10 <- ((1-rw.data$fitted.values)/rw.data$fitted.values)


	#cbind(rw.data$fitted.values[1:20],rw.data$base85[1:20])

	#remove superfluous count columns
	rw.data <- rw.data[!names(rw.data) %in% c("dyear", "dbase","sweight.2","count.1", "count.2" ) ]
	save(rw.data, file=paste("reweighting/rwBJ10_",i,".RData" , sep=""))
	#res <- rw.data[!names(rw.data) %in% "fitted.values"]
	
	if (i==years2[1]) { 
		res <- rw.data 
		} 	else {
		res <- subset(rw.data, year!=2010) 
		}
	
	rm(rw.data)
	
	return(res)

}



results.10  <- mclapply(years2,function(x) reweight10(x), mc.cores=2)

trim.m.rw10 <- results.10[[1]] 

for(i in 2:length(years2)) {
	temp.2 <- results.10[[i]]
	trim.m.rw10 <- rbind(trim.m.rw10, temp.2)
	}
	
	
#set CF weights for baseyear to 1
trim.m.rw10[trim.m.rw10$year==2010,]$base10 <- 1


save(trim.m.rw10, file="reweighting/trim.m.rw10.RData")


rm(results.10,trim.m.rw10)

#FEMALES


reweight10.f <- function(i) {

	#calculation for base year 2010

	pooled <- subset(trim.f, year==2010 | year==i )

	n <- nrow(pooled)
	nbase <- nrow(pooled[pooled$year==2010,])
	ndest <- nrow(pooled[pooled$year==i,])
	
	comp.weight <- nbase/ndest
	
	pooled$comp.weight <- 1
	pooled[pooled$year==i,]$comp.weight <- comp.weight
	
		
	#Create year dummies
	pooled$dyear <-  pooled$year
	pooled$dbase <-  pooled$year

	pooled$dyear[pooled$year==2010] <- 0
	pooled$dyear[pooled$year==i] <- 1
	pooled$dbase[pooled$dbase==2010] <- 1
	pooled$dbase[pooled$dbase==i] <- 0

	attach(pooled)
	#Probit of likelihood of being in destination-year, given charactersistics
	results <- glm(dyear ~ potexp+potexp2+potexp3+bild+potexp:bild+potexp2:bild+potexp3:bild, data=pooled, weights=comp.weight ,family="binomial")
	detach(pooled)
	#summary(results)
	
	rm(pooled)
	#combine values & fits ainto dataframe
	rw.data <- cbind(results$data, fitted.values=results$fitted.values )

	rm(results)

	#Calculate reweighting factor. (1-P)/P small if P large (e.g. highly educated)
	rw.data$base10 <- ((1-rw.data$fitted.values)/rw.data$fitted.values)


	#cbind(rw.data$fitted.values[1:20],rw.data$base85[1:20])

	#remove superfluous count columns
	rw.data <- rw.data[!names(rw.data) %in% c("dyear", "dbase","sweight.2","count.1", "count.2" ) ]
	save(rw.data, file=paste("reweighting/rwBJ10_",i,".RData" , sep=""))
	#res <- rw.data[!names(rw.data) %in% "fitted.values"]
	
	if (i==years2[1]) { 
		res <- rw.data 
		} 	else {
		res <- subset(rw.data, year!=2010) 
		}
	
	rm(rw.data)
	
	return(res)

}



results.10.f  <- mclapply(years2,function(x) reweight10.f(x), mc.cores=2)

trim.f.rw10 <- results.10.f[[1]] 

for(i in 2:length(years2)) {
	temp.2 <- results.10.f[[i]]
	trim.f.rw10 <- rbind(trim.f.rw10, temp.2)
	}
	
	
#set CF weights for baseyear to 1
trim.f.rw10[trim.f.rw10$year==2010,]$base10 <- 1


save(trim.f.rw10, file="reweighting/trim.f.rw10.RData")


