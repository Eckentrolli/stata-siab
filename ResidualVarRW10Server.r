options(scipen=30)

library(Hmisc)
library(parallel)
library(car)


setwd("/u/home/jd1033/DFG-descriptives/Results/FT")
load("reweighting/trim.m.rw10.RData")
load("reweighting/trim.f.rw10.RData")


#calculate reweighting

trim.m.rw10$rweight <- trim.m.rw10$sweight*trim.m.rw10$base10
trim.f.rw10$rweight <- trim.f.rw10$sweight*trim.f.rw10$base10

trim.m.rw10$countmf <- c(1:length(trim.m.rw10$discount))
trim.f.rw10$countmf <- c(1:length(trim.f.rw10$discount))

#reg-preparation
years <- unique(trim.m.rw10$year)

years <- sort(years, decreasing=TRUE)
justfrau <- c(0,1)
grid <- as.matrix(expand.grid(years, justfrau))
grid2 <- grid
colnames(grid)<-c("year","frau")


#define functions

cellreg <- function(i, j) {
	if(j==1) {
		sample1 <- subset(trim.f.rw10 , year==i)	
	}
	else {
		sample1 <- subset(trim.m.rw10 , year==i)
	}
	temp <-lm(discount ~ potexp+potexp2+potexp3+bild+potexp:bild+potexp2:bild+potexp3:bild, data=sample1,weights=rweight)
	res<-data.frame(countmf=sample1$countmf,res.base10=temp$residuals, fitted.base10=temp$fitted.values, values.base10=sample1$discount, rweight=sample1$rweight) 
	rm(sample1)
	return(res)
}

grid3 <- numeric()

for (i in 1:nrow(grid)) {
	grid3 <- c(grid3, paste(as.character(grid[i,1]), as.character(grid[i,2]),sep="@"))
}

wrapper <- function(i) {
	x <- as.numeric(unlist(strsplit(i, split="@")))
	res2<-cellreg(x[1],x[2])
	return(res2)
}
#Mincer regression

results <- mclapply(grid3,function(x) wrapper(x), mc.cores=3)

#naming

grid[grid[,2]==0,][,2] <-"m"
grid[grid[,2]==1,][,2] <-"f"

resnames <- numeric()
for (i in 1:dim(grid)[1]) {
	resnames <- c(resnames, paste(as.character(grid[i,1]), as.character(grid[i,2]),sep=""))
}

names(results) <- resnames 

bindresm <- results[[1]]

for (i in 2:26) {
bindresm<- rbind(bindresm, results[[i]])
}

bindresf <- results[[27]]
for (i in 28:52) {
bindresf<- rbind(bindresf, results[[i]])
}


trim.m.rw10 <- trim.m.rw10[order(trim.m.rw10$countmf),]
trim.f.rw10 <- trim.f.rw10[order(trim.f.rw10$countmf),]

bindresm <- bindresm[order(bindresm$countmf),]
bindresf <- bindresf[order(bindresf$countmf),]

trim.m.rw10 <- cbind(trim.m.rw10, bindresm)
trim.f.rw10 <- cbind(trim.f.rw10, bindresf)


variances10 <- data.frame()

for (i in 1:nrow(grid)) {

variances10[i,1] <- wtd.var(results[[i]]$res.base10,weights=results[[i]]$rweight, normwt=TRUE, na.rm=FALSE)
variances10[i,2] <- sqrt(variances10[i,1])
variances10[i,3] <- wtd.var(results[[i]]$values.base10, weights=results[[i]]$rweight, normwt=TRUE, na.rm=FALSE)
variances10[i,4] <- sqrt(variances10[i,3])
}


rownames(variances10) <- resnames
colnames(variances10) <- c("ResidualV.10", "ResidualSD.10", "Variance.10", "SD.10")

trim.m.rw10 <- trim.m.rw10[!names(trim.m.rw10) %in% c("values.base10", "sweight.1 ","countmf","rweight.1","valuesall") ]
trim.f.rw10 <- trim.f.rw10[!names(trim.f.rw10) %in% c("values.base10", "sweight.1 ","countmf","rweight.1","valuesall") ]
save.image( file = "reweighting/Resid10.RData")



